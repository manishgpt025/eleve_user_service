const request = require('request-promise');
//use user model
const user = require('../models/userModel.js');
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('CG0lEmHWZK4req6olYnGOQ');

var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
var randomstring = require("randomstring");

module.exports = {
    //Check post key validation
    checkRequest: (array, obj) => {
        console.log("arry is", array, obj);
        for (i of array) {

            if (obj[i] == undefined || obj[i] == "")
                return i;

        }
        return true;
    },

    // check email from DB
    checkEmail: (obj) => {
        user.findOne({ email: obj }, (error, result) => {
            console.log(result);
            if (error) {
                return 0;
            } else {
                return result;
            }
        })
    },
    // check mobile from DB
    checkMobile: (obj) => {
        user.findOne({ mobile: obj }, (error, result) => {
            console.log(result);
            if (error) {
                return 0;
            } else {
                return result;
            }
        })
    },
    // sending email function
    sendEmail: (obj) => {
        var message = obj || {};
        var async = false;
        var ip_pool = "";
        var send_at = "";
        let email_format = "";
        if(Object.keys(message).length){
            ({to:[{email:email_format,type}]} = message);
            let extention = email_format.split('@')[1];
            if(
                !extention.includes('eleve')
                   &&
                !extention.includes('engagelyee')
            ){
                message = {...message, to:[{email:'mohan@eleve.co.in',type}]}
            }
        }
        mandrill_client.messages.send({ "message": message, "async": async, "ip_pool": ip_pool, "send_at": send_at }, function(result) {
            return result;
        }, function(e) {
            // Mandrill returns the error as an object with name and message keys
            return e.message;
        });
    },

    // Send SMS
    sendSMS: (obj) => {
        let sns = new AWS.SNS({
            accessKeyId: 'AKIAJHJ2HVZPRSQ2ONWA',
            secretAccessKey: '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',
            region: 'ap-southeast-1'
        });

        let params = {
            PhoneNumber: obj.mobile,
            Message: 'Your OTP is ' + obj.otp,
            MessageStructure: 'string',
        };

        sns.publish(params, (err, data) => {
            if (err) {
                console.log('in sns error');
                throw err;
            }
            console.log(data)
        });
    },
    // sending OTP
    sendNewOTP: async(obj) => {
        let key = 'Hj1CnqXrlq4ZIGSDdtqneepNvKcSgI';
        const options = {
            method: 'GET',
            uri: 'https://push.sanketik.net//api/push?accesskey=' + key + '&to=' + obj.mobile + '&text=' + 'Hi' + '&from=ELEVEM'
        };
        await request(options).then((response) => {
            // console.log(response);
            // res.send(response.body);

        }).catch((err) => {
            console.log(err);
        })
    },

    // uploading file to AWS
    uploadFile: async(userId, file, callback) => {
        let number = Math.random() * (999999 - 10000) + 10000
        let resData = {
            Key: userId + number,
            Body: file,
            // ContentEncoding: 'base64',
            ContentType: 'image/jpeg',
            ACL: 'public-read'
        };
        await s3Bucket.upload(resData, (error, result) => {
            if (error) {
                return 0;
            } else {
                callback(null, result.Location);
            }
        });
    },

    //generate Random String
    generateRandomString: () => {
        let newId = randomstring.generate({
            length: 12,
            charset: 'hex'
        });
        return newId = newId + new Date().getTime().toString();
    }
}