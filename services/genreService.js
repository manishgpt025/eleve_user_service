const genreModel = require('../models/genreModel.js');

// get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    genreModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    genreModel.find(bodyData,{_id:0,genre:0,status : 0},{ sort:{ subgenre: 1 }}, (err, result) => {
        callback(err, result);
    });
}

//
const getOnlyData = (data, options, callback) => {
    genreModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

// Export
module.exports = {
    "getData": getData,
    "findData": findData,
    "getOnlyData": getOnlyData,
}