const currencyModel = require('../models/currencyModel.js');

const getData = (bodyData, callback) => {
    currencyModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}

const findData = (bodyData, callback) => {
    currencyModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

module.exports = {
    "getData": getData,
    "findData": findData
}