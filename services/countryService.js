const countryModel = require('../models/countryModel.js');

const createData = (bodyData, callback) => {
    countryModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

const getData = (bodyData, callback) => {
    countryModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findData = (bodyData, callback) => {
    countryModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}
const updateOne = (query, bodyData, options, callback) => {
    countryModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

const getDetails = (bodydata, options, callback) => {
    countryModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const getDetails_pagination = (bodydata, options, page, callback) => {
    countryModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const updateDetails = (query, bodydata, options, callback) => {
    countryModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const deleteDetails = (query, bodydata, options, callback) => {
    countryModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const getOnlyData = (data, options, callback) => {
    countryModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}
module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "getOnlyData": getOnlyData

}