const adminModel = require('../models/adminModel.js');

const createData = (bodyData, callback) => {
    adminModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

const getData = (bodyData, options, callback) => {
    // console.log(options);
    adminModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const findData = (bodyData, callback) => {
    adminModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}
const updateOne = (query, bodyData, options, callback) => {
    adminModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

const getDetails = (bodydata, options, callback) => {
    adminModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
});
}
const getDetails_pagination = (bodydata, options, page, callback) => {
    adminModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const updateDetails = (query, bodydata, options, callback) => {
    adminModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const deleteDetails = (query, bodydata, options, callback) => {
    adminModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const getOnlyData = (data, options, callback) => {
    adminModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}
const arrayUpdate = (query, data, options, callback) => {
        adminModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
            callback(err, result);
        });
    }
    // Get record by pagination
const getPaginateData = (data, options, sort, callback) => {
    adminModel.paginate(data, options, sort, (err, result) => {
        callback(err, result);
    });
}
const findstatusData = (bodyData, options, callback) => {
        adminModel.findOne(bodyData, options, (err, result) => {
            callback(err, result);
        });
    }

const findDataById = (bodydata, callback) => {
    adminModel.findOne(bodydata, { password: 0, logins:0, }, (err, result) => {
        callback(err, result);
    });
}
    // Get all admin with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = adminModel.aggregate([
        bodyData.admin_credential,
        {
            $match: {
                $or: [{
                        name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        team: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }
                ]
            },
        },
        bodyData.status,
        bodyData.role,
        bodyData.location,

        {
            $project: {
                _id: 1,
                "admin_id": 1,
                "dob": 1,
                "email": 1,
                "extension": 1,
                "first_login": 1,
                "isd": 1,
                "last_name": 1,
                "location": 1,
                "mobile": 1,
                "name": 1,
                "name": { "$toLower": "$name" },
                "profile_image": 1,
                "role": 1,
                "role_permission": 1,
                "status": 1,
                "team": 1
            }
        },
        sort

    ])
    adminModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })


}

module.exports = {
        createData,
        getData,
        updateOne,
        getDetails,
        updateDetails,
        deleteDetails,
        getDetails_pagination,
        findData,
        getOnlyData,
        arrayUpdate,
        getPaginateData,
        findstatusData,
        getPaginateDataWithAggregate,
        findDataById
}