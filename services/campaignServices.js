const campaignModel = require('../models/campaign.js');

// Add data to campaignModel
const createData = (bodyData, callback) => {
        campaignModel.create(bodyData, (err, result) => {
            callback(err, result);
        });
    }
    // get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    campaignModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    campaignModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// update only one record

const updateOne = (query, bodyData, options, callback) => {
    campaignModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const getDetails = (bodydata, options, callback) => {
    campaignModel.findOne(bodydata, options, (err, result) => {
        callback(err, result);
    });
}


const getDetails_pagination = (bodydata, options, page, callback) => {
    campaignModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

const updateDetails = (query, bodydata, options, callback) => {
    campaignModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const deleteDetails = (query, bodydata, options, callback) => {
    campaignModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, options, callback) => {
    campaignModel.findByIdAndUpdate(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, options, callback) => {
    campaignModel.updateOne(query, data, options, (err, result) => {
        callback(err, result);
    });
}

const getOnlyData = (data, options, callback) => {
    campaignModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

const getAllData = (bodydata, options, callback) => {
    campaignModel.find(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

// Get all admin with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = campaignModel.aggregate([{
            $match: {
                $or: [{
                        title: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        brand_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        organisation_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        hashtag: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        created_by: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }
                ]
            },
        },
        {
            $match: { status: { $in: bodyData.status } },
        },
        // {
        //     $match: { 'platforms.name': { $in: bodyData.platform } }
        // },
        { $sort: sort }

    ])
    campaignModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })


}

const getAggregate = (bodyData, callback) => {
    campaignModel.aggregate([{
            $match: bodyData,
        },
        {
            $lookup: {
                from: "campaign_influencers",
                localField: "_id",
                foreignField: "campaign_id",
                as: "influencer_details"
            }
        },
        {
            $project: {
                "title": 1,
                "hashtahg_share": 1,
                "url_share": 1,
                "secondary_hashtag": 1,
                "track_secondary_hashtag": 1,
                "keywords": 1,
                "influencer_details": 1,
                "influencers_count": { $size: "$influencer_details" },

            }
        }
    ]).exec((err, result) => {
        console.log(err);
        callback(err, result);
    });
}
module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "arrayUpdateRemove": arrayUpdateRemove,
    "getOnlyData": getOnlyData,
    "arrayUpdate": arrayUpdate,
    "getAllData": getAllData,
    "getPaginateDataWithAggregate": getPaginateDataWithAggregate,
    "getAggregate": getAggregate
}