const advertiserModel = require('../models/advertiserModel.js');

const createData = (bodyData, callback) => {
    advertiserModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

const getData = (bodyData, callback) => {
    advertiserModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findData = (bodyData, callback) => {
    advertiserModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}
const updateOne = (query, bodyData, options, callback) => {
    advertiserModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

const getDetails = (bodydata, options, callback) => {
    advertiserModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const getDetails_pagination = (bodydata, options, page, callback) => {
    advertiserModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const updateDetails = (query, bodydata, options, callback) => {
    advertiserModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const deleteDetails = (query, bodydata, options, callback) => {
    advertiserModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const getOnlyData = (data, options, callback) => {
    advertiserModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get all
const getAllData = (data, options, callback) => {
    advertiserModel.find(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get record by pagination
const getPaginateData = (data, options, callback) => {
    advertiserModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}

const arrayUpdate = (query, data, options, callback) => {
    advertiserModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// Get all advertisers with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = advertiserModel.aggregate([{
            $match: {
                $or: [{
                    name: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    email: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }]
            },
        },
        {
            $match: { status: { $in: bodyData.status } },
        },
        { "$addFields": { "organisation_id": { "$toObjectId": "$organisation_id" } } },
        {
            $lookup: {
                from: "organisations",
                localField: "organisation_id",
                foreignField: "_id",
                as: "organisation_details"
            }
        },
        //{ $unwind: "$organisation_details" },
        {
            "$project": {
                "_id": 1,
                "name": 1,
                "last_name": 1,
                "email": 1,
                "isd": 1,
                "phone": 1,
                "status": 1,
                "address": 1,
                "country": 1,
                "state": 1,
                "city": 1,
                "profile_image": 1,
                "organisation_details.organisation_name": 1,
                "organisation_details.type": 1
            }
        },
        { $sort: sort }

    ])
    advertiserModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })


}

const getAdvertiserById = (bodyData, callback) => {
    advertiserModel.aggregate([{
            $match: bodyData,
        },
        { "$addFields": { "organisation_id": { "$toObjectId": "$organisation_id" } } },
        {
            $lookup: {
                from: "organisations",
                localField: "organisation_id",
                foreignField: "_id",
                as: "organisation_details"
            }
        },
        // { $unwind: "$organisation_details" },
        {
            "$project": {
                "associated_brands": 1,
                "_id": 1,
                "name": 1,
                "last_name": 1,
                "email": 1,
                "isd": 1,
                "phone": 1,
                "status": 1,
                "organisation_id": 1,
                "manager": 1,
                "address": 1,
                "country": 1,
                "state": 1,
                "city": 1,
                "designation": 1,
                "notes": 1,
                "password": 1,
                "created_at": 1,
                "profile_image": 1,
                "updated_at": 1,
                "organisation_details.organisation_name": 1,
                "organisation_details.type": 1,
                "organisation_details.addresses": 1
                    // "filtered_users": {
                    //     "$filter": {
                    //         "input": "$organisation_details.addresses.advertisers",
                    //         "as": "organisation_details_id",
                    //         // cond: {
                    //         //     $setIsSubset: [
                    //         //         [userId], '$$organisation_details_id.addresses.advertisers'
                    //         //     ]
                    //         // }
                    //         "cond": { "$in": [userId, "$$organisation_details_id.addresses.advertisers"] }
                    //     }
                    // }
            }
        },
        {
            $lookup: {
                from: "organisations",
                "let": { "associated_brands": "$associated_brands" },
                "pipeline": [{ "$match": { "$expr": { "$in": [{ "$toString": "$_id" }, "$$associated_brands"] } } }],
                // localField: "associated_brands",
                // foreignField: "_id",
                as: "brand_details"
            }
        },

    ]).exec((err, result) => {
        console.log(err);
        callback(err, result);
    });


}

module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "getOnlyData": getOnlyData,
    "getAllData": getAllData,
    "arrayUpdate": arrayUpdate,
    "getPaginateData": getPaginateData,
    "getPaginateDataWithAggregate": getPaginateDataWithAggregate,
    "getAdvertiserById": getAdvertiserById

}