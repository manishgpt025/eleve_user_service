const userModel = require('../models/userModel.js');
// Add data to userModel
const createData = (bodyData, callback) => {
    userModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

// get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    userModel.findOne(bodyData, {
        password: 0
    }, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    console.log(bodyData)
    userModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const updateOne = (query, bodyData, options, callback) => {
    userModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const getDetails = (bodydata, options, callback) => {
    userModel.findOne(bodydata, options, {
        password: 0
    }, (err, result) => {
        callback(err, result);
    });
}

//
const getDetails_pagination = (bodydata, options, page, callback) => {
    userModel.findOne(bodydata, options, page, {
        password: 0
    }, (err, result) => {
        callback(err, result);
    });
}

//
const updateDetails = (query, bodydata, options, callback) => {
    userModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

//
const deleteDetails = (query, bodydata, options, callback) => {
    userModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, callback) => {
    userModel.findByIdAndUpdate(query, data, {
        safe: true,
        upsert: true
    }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, callback) => {
    userModel.updateOne(query, data, {
        safe: true,
        upsert: true
    }, (err, result) => {
        callback(err, result);
    });
}

//
const getOnlyData = (data, options, callback) => {
    userModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get all data with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    let mainQueryParmas = [];
    mainQueryParmas.push({$match: {"registered": '1'}});

    if(bodyData.outdateProfiles && bodyData.outdateProfiles == 1){
        mainQueryParmas.push({ "$match" :
             { $or: [
                    { "twitter.followers_updated" : { "$lte" : new Date(new Date().getTime() - 1000 * 3600 * 24 * 5)}, "twitter.followers_updated": { $exists: true  } },
                    { "facebook.followers_updated" : { "$lte" : new Date(new Date().getTime() - 1000 * 3600 * 24 * 5)}, "facebook.followers_updated": { $exists: true  } },
                    { "instagram.followers_updated" : { "$lte" : new Date(new Date().getTime() - 1000 * 3600 * 24 * 5)}, "instagram.followers_updated": { $exists: true  } },
                    { "blog.followers_updated" : { "$lte" : new Date(new Date().getTime() - 1000 * 3600 * 24 * 5)}, "blog.followers_updated": { $exists: true  } }
                ]
            }
        });
    }

    if(bodyData.status && bodyData.status.length>0){
        mainQueryParmas.push({$match: {status: { $in: bodyData.status}}});
    }

    if(bodyData.gender){
        mainQueryParmas.push(bodyData.gender);
    }

    if(bodyData.search){
        mainQueryParmas.push( {
            $match: {
                $or: [{
                        first_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        last_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        country: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        state: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        city: {
                            $regex: bodyData.search,
                            $options: 'i'
                        },

                    }, {
                    alt_email: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        mobile: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        alt_mobile: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'facebook.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'twitter.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'youtube.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'snapchat.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'instagram.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'intagram_business.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'tiktok.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'linkedin.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'blog.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'pinterest.genres': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'facebook.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'twitter.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'snapchat.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'instagram.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'instagram_business.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'tiktok.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'youtube.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'blog.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'linkedin.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'pinterest.Elv_social': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }
                ],
            },
        });
    }

    if(bodyData.socialPlatform){
        mainQueryParmas.push(bodyData.socialPlatform);
    }

    mainQueryParmas.push({
            $unwind: {
                path: "$facebook",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$twitter",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$snapchat",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$instagram",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$tiktok",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$youtube",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$pinterest",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$linkedin",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$blog",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$instagram_business",
                preserveNullAndEmptyArrays: true
            }
        });

        mainQueryParmas.push({
            "$addFields": {
                "facebook_count": {
                    $cond: {
                        if: {
                            $eq: ["$facebook.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$facebook.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "twitter_count": {
                    $cond: {
                        if: {
                            $eq: ["$twitter.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$twitter.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "snapchat_count": {
                    $cond: {
                        if: {
                            $eq: ["$snapchat.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$snapchat.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "instagram_count": {
                    $cond: {
                        if: {
                            $eq: ["$instagram.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$instagram.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "tiktok_count": {
                    $cond: {
                        if: {
                            $eq: ["$tiktok.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$tiktok.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "youtube_count": {
                    $cond: {
                        if: {
                            $eq: ["$youtube.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$youtube.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "pinterest_count": {
                    $cond: {
                        if: {
                            $eq: ["$pinterest.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$pinterest.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "linkedin_count": {
                    $cond: {
                        if: {
                            $eq: ["$linkedin.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$linkedin.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "blog_count": {
                    $cond: {
                        if: {
                            $eq: ["$blog.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$blog.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "instagram_business_count": {
                    $cond: {
                        if: {
                            $eq: ["$instagram_business.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$instagram_business.followers_count"
                        }
                    }
                }
            }
    });

    if( bodyData.reachData){
        mainQueryParmas.push( bodyData.reachData);
    }

    if(bodyData.genresData){
        mainQueryParmas.push( bodyData.genresData);
    }

    if(bodyData.categoryData){
        mainQueryParmas.push( bodyData.categoryData);
    }

    mainQueryParmas.push({
            $group: {
                _id: "$_id",
                "first_name": {
                    "$first": "$first_name"
                },
                "influencer_id": {
                    "$first": "$influencer_id"
                },
                "last_name": {
                    "$first": "$last_name"
                },
                "status": {
                    "$first": "$status"
                },
                "gender": {
                    "$first": "$gender"
                },
                "dob": {
                    "$first": "$dob"
                },
                "country": {
                    "$first": "$country"
                },
                "state": {
                    "$first": "$state"
                },
                "city": {
                    "$first": "$city"
                },
                "address": {
                    "$first": "$address"
                },
                "email": {
                    "$first": "$email"
                },
                "alt_email": {
                    "$first": "$alt_email"
                },
                "isd_code": {
                    "$first": "$isd_code"
                },
                "mobile": {
                    "$first": "$mobile"
                },
                "alt_isd_code": {
                    "$first": "$alt_isd_code"
                },
                "isd_mobile": {
                    "$first": "$isd_mobile"
                },
                "note": {
                    "$first": "$note"
                },
                "registered": {
                    "$first": "$registered"
                },
                "country": {
                    "$first": "$country"
                },
                "state": {
                    "$first": "$state"
                },
                "profile_pic": {
                    "$first": "$profile_pic"
                },
                "campaign_notification": {
                    "$first": "$campaign_notification"
                },
                "payment_notification": {
                    "$first": "$payment_notification"
                },
                "account_notification": {
                    "$first": "$account_notification"
                },
                "new_feature_notification": {
                    "$first": "$new_feature_notification"
                },

                "interests": {
                    "$first": "$interests"
                },
                "language": {
                    "$first": "$language"
                },
                "lifestage": {
                    "$first": "$lifestage"
                },
                "education": {
                    "$first": "$education"
                },
                "bank_details": {
                    "$first": "$bank_details"
                },
                "payment_details": {
                    "$first": "$payment_details"
                },
                "facebook": {
                    "$addToSet": "$facebook"
                },
                "twitter": {
                    "$addToSet": "$twitter"
                },
                "youtube": {
                    "$addToSet": "$youtube"
                },
                "linkedin": {
                    "$addToSet": "$linkedin"
                },
                "instagram": {
                    "$addToSet": "$instagram"
                },
                "snapchat": {
                    "$addToSet": "$snapchat"
                },
                "pinterest": {
                    "$addToSet": "$pinterest"
                },
                "blog": {
                    "$addToSet": "$blog"
                },
                "instagram_business": {
                    "$addToSet": "$instagram_business"
                },
                "tiktok": {
                    "$addToSet": "$tiktok"
                }

            },

    });

    mainQueryParmas.push({ $sort: sort });

    if(bodyData.excelDownload && bodyData.excelDownload==1){
        userModel.aggregate(mainQueryParmas).exec((err, result) => {
            callback(err, result);
        });
    }else{
        var aggregateData = userModel.aggregate(mainQueryParmas);
        userModel.aggregatePaginate(aggregateData, options, (error, success, pages, total) => {
            callback(error, success, pages, total);
        });
    }




}


const getInfluencerData = (bodyData, options, callback) => {
    userModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}


// Get all data with pagination using aggregate
// const getDataWithAggregate = (bodyData, sort, callback) => {
//     userModel.aggregate([
//         {$match: {"registered": '1'}},
//         {$match: {status: { $in: bodyData.status}}},
//         bodyData.gender,
//         {
//             $match: {
//                 $or: [{
//                         first_name: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         last_name: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         email: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         country: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         state: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     },
//                     {
//                         city: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         },

//                     }, {
//                     alt_email: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         mobile: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         alt_mobile: {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'facebook.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'twitter.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'youtube.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'snapchat.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'instagram.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'intagram_business.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'tiktok.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'linkedin.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'blog.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'pinterest.genres': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'facebook.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'twitter.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'snapchat.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'instagram.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'instagram_business.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'tiktok.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'youtube.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'blog.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'linkedin.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }, {
//                         'pinterest.Elv_social': {
//                             $regex: bodyData.search,
//                             $options: 'i'
//                         }
//                     }
//                 ],
//             },
//         },
//         bodyData.socialPlatform,
//         {
//             $unwind: {
//                 path: "$facebook",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$twitter",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$snapchat",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$instagram",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$tiktok",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$youtube",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$pinterest",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$linkedin",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$blog",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $unwind: {
//                 path: "$instagram_business",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             "$addFields": {
//                 "facebook_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$facebook.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$facebook.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "twitter_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$twitter.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$twitter.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "snapchat_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$snapchat.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$snapchat.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "instagram_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$instagram.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$instagram.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "tiktok_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$tiktok.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$tiktok.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "youtube_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$youtube.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$youtube.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "pinterest_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$pinterest.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$pinterest.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "linkedin_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$linkedin.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$linkedin.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "blog_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$blog.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$blog.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         {
//             "$addFields": {
//                 "instagram_business_count": {
//                     $cond: {
//                         if: {
//                             $eq: ["$instagram_business.followers_count", '']
//                         },
//                         then: 0,
//                         else: {
//                             "$toDouble": "$instagram_business.followers_count"
//                         }
//                     }
//                 }
//             }
//         },
//         bodyData.reachData,
//         bodyData.genresData,
//         bodyData.categoryData,
//         //{ $sort: sort },
//         {
//             $group: {
//                 _id: "$_id",
//                 "first_name": {
//                     "$first": "$first_name"
//                 },
//                 "influencer_id": {
//                     "$first": "$influencer_id"
//                 },
//                 "last_name": {
//                     "$first": "$last_name"
//                 },
//                 "status": {
//                     "$first": "$status"
//                 },
//                 "gender": {
//                     "$first": "$gender"
//                 },
//                 "dob": {
//                     "$first": "$dob"
//                 },
//                 "country": {
//                     "$first": "$country"
//                 },
//                 "state": {
//                     "$first": "$state"
//                 },
//                 "city": {
//                     "$first": "$city"
//                 },
//                 "address": {
//                     "$first": "$address"
//                 },
//                 "email": {
//                     "$first": "$email"
//                 },
//                 "alt_email": {
//                     "$first": "$alt_email"
//                 },
//                 "isd_code": {
//                     "$first": "$isd_code"
//                 },
//                 "mobile": {
//                     "$first": "$mobile"
//                 },
//                 "alt_isd_code": {
//                     "$first": "$alt_isd_code"
//                 },
//                 "isd_mobile": {
//                     "$first": "$isd_mobile"
//                 },
//                 "note": {
//                     "$first": "$note"
//                 },
//                 "registered": {
//                     "$first": "$registered"
//                 },
//                 "country": {
//                     "$first": "$country"
//                 },
//                 "state": {
//                     "$first": "$state"
//                 },
//                 "profile_pic": {
//                     "$first": "$profile_pic"
//                 },
//                 "campaign_notification": {
//                     "$first": "$campaign_notification"
//                 },
//                 "payment_notification": {
//                     "$first": "$payment_notification"
//                 },
//                 "account_notification": {
//                     "$first": "$account_notification"
//                 },
//                 "new_feature_notification": {
//                     "$first": "$new_feature_notification"
//                 },

//                 "interests": {
//                     "$first": "$interests"
//                 },
//                 "language": {
//                     "$first": "$language"
//                 },
//                 "lifestage": {
//                     "$first": "$lifestage"
//                 },
//                 "education": {
//                     "$first": "$education"
//                 },
//                 "bank_details": {
//                     "$first": "$bank_details"
//                 },
//                 "payment_details": {
//                     "$first": "$payment_details"
//                 },
//                 "facebook": {
//                     "$addToSet": "$facebook"
//                 },
//                 "twitter": {
//                     "$addToSet": "$twitter"
//                 },
//                 "youtube": {
//                     "$addToSet": "$youtube"
//                 },
//                 "linkedin": {
//                     "$addToSet": "$linkedin"
//                 },
//                 "instagram": {
//                     "$addToSet": "$instagram"
//                 },
//                 "snapchat": {
//                     "$addToSet": "$snapchat"
//                 },
//                 "pinterest": {
//                     "$addToSet": "$pinterest"
//                 },
//                 "blog": {
//                     "$addToSet": "$blog"
//                 },
//                 "instagram_business": {
//                     "$addToSet": "$instagram_business"
//                 },
//                 "tiktok": {
//                     "$addToSet": "$tiktok"
//                 }

//             },

//         },
//         { $sort: sort }

//     ]).exec((err, result) => {
//         console.log(err, result);
//         callback(err, result);
//     });
// }

// find data / find by element - returns only one record
const findUserData = (bodyData, options, callback) => {
    userModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// Find max reach (followers_count) in influecner collection for influecner
const getMaxReachCountInfluencer = (bodyData, callback) => {
   userModel.aggregate([
        {$match: {"registered": '1'}},
        {$match: {status:bodyData.status}},
        {
            $unwind: {
                path: "$facebook",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$twitter",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$snapchat",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$instagram",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$tiktok",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$youtube",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$pinterest",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$linkedin",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$blog",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$instagram_business",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            "$addFields": {
                "facebook_count": {
                    $cond: {
                        if: {
                            $eq: ["$facebook.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$facebook.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "twitter_count": {
                    $cond: {
                        if: {
                            $eq: ["$twitter.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$twitter.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "snapchat_count": {
                    $cond: {
                        if: {
                            $eq: ["$snapchat.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$snapchat.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "instagram_count": {
                    $cond: {
                        if: {
                            $eq: ["$instagram.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$instagram.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "tiktok_count": {
                    $cond: {
                        if: {
                            $eq: ["$tiktok.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$tiktok.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "youtube_count": {
                    $cond: {
                        if: {
                            $eq: ["$youtube.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$youtube.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "pinterest_count": {
                    $cond: {
                        if: {
                            $eq: ["$pinterest.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$pinterest.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "linkedin_count": {
                    $cond: {
                        if: {
                            $eq: ["$linkedin.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$linkedin.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "blog_count": {
                    $cond: {
                        if: {
                            $eq: ["$blog.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$blog.followers_count"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "instagram_business_count": {
                    $cond: {
                        if: {
                            $eq: ["$instagram_business.followers_count", '']
                        },
                        then: 0,
                        else: {
                            "$toDouble": "$instagram_business.followers_count"
                        }
                    }
                }
            }
        },
        {
            $group: {
                _id: 'count_data',
                "facebook_count": {"$max": "$facebook_count"},
                "twitter_count":  {"$max":  "$twitter_count"},
                "youtube_count":  {"$max": "$youtube_count"},
                "instagram_count": {"$max": "$instagram_count"},
                "snapchat_count": {"$max": "$snapchat_count"},
                "linkedin_count": {"$max": "$linkedin_count"},
                "blog_count": {"$max": "$blog_count"},
                "tiktok_count": {"$max": "$tiktok_count"}
            },

        }
    ]).exec((error, result) => {
        callback(error, result);
    });
}


// Export
module.exports = {
    createData,
    getData,
    updateOne,
    getDetails,
    updateDetails,
    deleteDetails,
    getDetails_pagination,
    findData,
    arrayUpdateRemove,
    getOnlyData,
    arrayUpdate,
    getPaginateDataWithAggregate,
    getInfluencerData,
   // getDataWithAggregate,
    findUserData,
    getMaxReachCountInfluencer
}