const campaignInfluencerModel = require('../models/campaign_influencer.js');
const campaignModel = require('../models/campaign.js');

const ObjectId = require('mongodb').ObjectID;

// Add data to campaignInfluencerModel
const createData = (bodyData, callback) => {
    campaignInfluencerModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

// get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    campaignInfluencerModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    campaignInfluencerModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findDataUnique = (bodyData, callback) => {
    campaignInfluencerModel.find(bodyData).distinct('influencer_id', function(err, result) {
        callback(err, result);
    });
}

// update only one record
const updateOne = (query, bodyData, options, callback) => {
    campaignInfluencerModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// Details
const getDetails = (bodydata, options, callback) => {
    campaignInfluencerModel.findOne(bodydata, options, (err, result) => {
        callback(err, result);
    });
}

//
const getDetails_pagination = (bodydata, options, page, callback) => {
    campaignInfluencerModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

//
const updateDetails = (query, bodydata, options, callback) => {
    campaignInfluencerModel.updateMany(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// const deleteDetails = (query, bodydata, options, callback) => {
//     campaignInfluencerModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
//         callback(err, result);
//     });
// }

const deleteDetails = (query, options, callback) => {
    campaignInfluencerModel.remove(query, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, options, callback) => {
    campaignInfluencerModel.findByIdAndUpdate(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, options, callback) => {
    campaignInfluencerModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

//
const getOnlyData = (data, options, callback) => {
    campaignInfluencerModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get all data
const getAllData = (bodydata, options, callback) => {
    campaignInfluencerModel.find(bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// Populate
const getDetailsUsingPopulate = (bodydata, options, callback) => {
    campaignInfluencerModel.findOne(bodydata).populate('campaign_id').exec(function(err, result) {
        callback(err, result);
    })
}

// Remove record
const removeRecord = (bodydata, options, callback) => {
    campaignInfluencerModel.deleteMany(bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// Get invited influencers campaign with pagination using aggregate
const getDataWithAggregate = (bodyData, sort, callback) => {
    campaignInfluencerModel.aggregate([
        bodyData.influencerId,
        bodyData.action,
        {
            $lookup: {
                from: "campaigns",
                localField: "campaign_id",
                foreignField: "_id",
                as: "campaign_details"
            }
        },
        bodyData.camapign_status,
        {
            $match: {
                $or: [{
                        'campaign_details.title': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        'campaign_details.brand_name': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        'campaign_details.organisation_name': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }
                ]
            },
        },
        {$unwind: '$campaign_details'},
        {
            "$project": {
                "campaign_id":1,
                "action":1,
                "created_at":1,
                "updated_at":1,
                "platform":1,
                "campaign_details._id": 1,
                "campaign_details.title": 1,
                "campaign_details.start_date": 1,
                "campaign_details.status": 1,
                "campaign_details.platforms": 1

            }
        },
        {
            $group: {
                _id: "$campaign_id",
                "action": { "$first": "$action" },
                "created_at": { "$first": "$created_at" },
                "updated_at": { "$first": "$updated_at" },
                "campaign_id": { "$first": "$campaign_id" },
                "platform": { "$first": "$platform" },
                "campaign_details": {"$max": "$campaign_details"}
            },
        },
    sort
    ]).exec((err, result) => {
        callback(err, result);
    });
}


// Get invited influencers campaign with pagination using aggregate
const getInfluencerCampaignList = (bodyData,options,sort, callback) => {
    let mainQueryParmas = [];
    mainQueryParmas.push( bodyData.camapign_status);
    mainQueryParmas.push({
            $lookup:
               {
                 from: "campaign_influencers",
                 let: {
                     influencerId: bodyData.influencer_id,
                     campaignId: "$_id",
                     rest_action: "decline"
                },
                 pipeline: [
                    { $match:
                       { $expr:
                          { $and:
                             [
                               { $eq: [ "$influencer_id",  "$$influencerId" ] },
                               { $eq: [ "$campaign_id", "$$campaignId" ] },
                               { $ne: [ "$action", "$$rest_action" ] }
                             ]
                          }
                       }
                    },
                 ],
                 as: "profiles"
                }
    });
    mainQueryParmas.push({'$match': {'profiles': {'$ne': []}}});
    mainQueryParmas.push({
            "$project": {
                "profiles.action":1,
                "profiles.platform":1,
                "_id": 1,
                "title": 1,
                "start_date": 1,
                "advertiser_brand_name": 1,
                "end_date": 1,
                "organization_name": 1,
                "hashtag": 1,
                "secondary_hashtag": 1,

            }
    });
    mainQueryParmas.push(sort);

    if(bodyData.completeCamapign && bodyData.completeCamapign=='6'){
        var aggregateData = campaignModel.aggregate(mainQueryParmas);
        campaignModel.aggregatePaginate(aggregateData, options, (error, success, pages, total) => {
                callback(error, success, pages, total);
            });
    }else{
        campaignModel.aggregate(mainQueryParmas).exec((err, result) => {
            callback(err, result);
        });
    }
}
// Get invited influencers campaign with pagination using aggregate
const getPaginateCompletedDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = campaignInfluencerModel.aggregate([
        bodyData.influencerId,
        {
            $lookup: {
                from: "campaigns",
                localField: "campaign_id",
                foreignField: "_id",
                as: "campaign_details"
            }
        },
        bodyData.camapign_status,
        {
            $match: {
                $or: [{
                    'campaign_details.title': {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    'campaign_details.hashtag': {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    'campaign_details.organization_name': {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }]
            },
        },
        {
            $group: {
                _id: "$campaign_id",
                "action": { "$first": "$action" },
                "created_at": { "$first": "$created_at" },
                "updated_at": { "$first": "$updated_at" },
                "status": { "$first": "$status" },
                "campaign_id": { "$first": "$campaign_id" },
                "platform": { "$first": "$platform" },
                "campaign_details": {
                    "$max": "$campaign_details"
                }
            },
        },
        sort
    ])
    campaignInfluencerModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })
}

// Get invited influencers campaign with pagination using aggregate
const getCampaignNewAcceptedData = (bodyData, callback) => {
    campaignModel.aggregate([
        {$match: {_id: bodyData.campaign_id}},
        {$match: { 'status': bodyData.campaignStatus }},
        {
            $lookup:
               {
                 from: "campaign_influencers",
                 let: {
                     influencerId: bodyData.influencer_id,
                     campaignId: "$_id",
                     rest_action: "decline"
                },
                 pipeline: [
                    { $match:
                       { $expr:
                          { $and:
                             [
                               { $eq: [ "$influencer_id",  "$$influencerId" ] },
                               { $eq: [ "$campaign_id", "$$campaignId" ] },
                               { $ne: [ "$action", "$$rest_action" ] }
                             ]
                          }
                       }
                    },
                 ],
                 as: "profiles"
                }
        },
    ]).exec((err, result) => {
        callback(err, result);
    });
}

// Export
module.exports = {
    createData,
    getData,
    updateOne,
    getDetails,
    updateDetails,
    deleteDetails,
    getDetails_pagination,
    findData,
    arrayUpdateRemove,
    getOnlyData,
    arrayUpdate,
    getAllData,
    getDetailsUsingPopulate,
    removeRecord,
    findDataUnique,
    getDataWithAggregate,
    getPaginateCompletedDataWithAggregate,
    getCampaignNewAcceptedData,
    getInfluencerCampaignList
}