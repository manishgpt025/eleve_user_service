const cityModel = require('../models/cityModel.js');

const createData = (bodyData, callback) => {
    cityModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

const getData = (bodyData, callback) => {
    cityModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findData = (bodyData, callback) => {
    cityModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}
const updateOne = (query, bodyData, options, callback) => {
    cityModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

const getDetails = (bodydata, options, callback) => {
    cityModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const getDetails_pagination = (bodydata, options, page, callback) => {
    cityModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}
const updateDetails = (query, bodydata, options, callback) => {
    countryModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const deleteDetails = (query, bodydata, options, callback) => {
    cityModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}
const getOnlyData = (data, options, callback) => {
    cityModel.findOne(data, options, (err, result) => {
    callback(err, result);
    });
}
module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "getOnlyData" : getOnlyData

}