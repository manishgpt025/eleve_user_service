const dotenv = require('dotenv');
dotenv.config();
const express = require('express')
const app = express();
require('dotenv').config();
const config = require('./config.js');
bodyParser = require('body-parser');
const dbConnect = require('./mongodb');

const server = require('http').createServer(app)
cors = require('cors');
const route = require('./routes/userRoute.js');

app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.json({limit: '100mb', extended: true}));
app.use(cors());

//call route by api
app.use('/api', route);

//connectivity forn listen PORT
/*server.listen(global.gConfig.node_port, () => {
    console.log('Example app listening on port', global.gConfig.node_port)
});*/

server.listen(process.env.PORT, () => {
    console.log('Example app listening on port', global.gConfig.node_port)
});