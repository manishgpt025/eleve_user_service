// Packages

// Controllers
const userController = require("../controllers/userController");
const resetController = require("../controllers/resetController");
const socialController = require("../controllers/SocialController");
const profileController = require("../controllers/profileController");
const ChangePasswordController = require("../controllers/ChangePasswordController");
const NotificationController = require("../controllers/notificationController");
const EmailVerificationController = require("../controllers/emailVerificationController");
const paymentController = require("../controllers/paymentController");
const bankController = require("../controllers/bankController");
const facebookController = require('../controllers/facebook');
const linkedInController = require("../controllers/linkedInController");
const youtubeController = require("../controllers/youtubeController");
const twitterController = require("../controllers/twitterController");
const instagramController = require("../controllers/instagramController");
const instagramBusinessController = require("../controllers/instagramBusinessController");
const advertiserController = require("../controllers/advertiserController");
const advertisersController = require("../controllers/advertiser");
const organisationController = require("../controllers/organisationController");
const adminController = require("../controllers/adminController");
const influencerController = require("../controllers/influencerController");
// influencerCampaignController
const influencerCampaignController = require("../controllers/influencerCampaignController");
// excelController
const excelUploadController = require("../controllers/excelUploadController");


// Middleware
const middleware = require('../middleware.js');
let checkAdminuser = middleware.checkAdminUser;
// Express router
const router = require('express').Router();
// Swagger
swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('../swagger.json');
// Swagger Route
router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument));

/* Routes */
// Login
router.post('/login', userController.login);
router.post('/logout', userController.logout);
// Password reset and forget
router.post('/reset-password', resetController.resetPassword);
router.post('/forgot-password', resetController.forgotPassword);
router.post('/check-expiry', resetController.checkTokenValidity);
// Sign up
router.post('/postregister', userController.postUserdata);
// Subscribe
router.post('/postsubscribe', userController.postSubscribeData);
// Instagram login
router.get('/authorize_user', socialController.authorize_user);
router.post('/handleauth', socialController.handleauth);
// Add Social Platforms
router.post('/addconnection', socialController.addConnection);
router.post('/twitterlogin', socialController.twitterlogin);
router.post('/facebooklogin', socialController.facebooklogin);
router.post('/youtubelogin', socialController.youtubelogin);
router.post('/linkedinlogin', socialController.linkedinlogin);
router.post('/pinterestlogin', socialController.pinterestlogin);
router.post('/bloglogin', socialController.bloglogin);
router.post('/instagramlogin', socialController.instagramlogin);
// router.post('/twitterapi', socialController.twitterApi);
//delete Social Platforms
router.post('/deleteconnection', socialController.deleteConnection);
router.post('/delete/socials', socialController.deleteSocials);
// Get user details
router.post('/user', profileController.userData);
router.put('/user', profileController.userDataUpdate);
router.post('/user/image/upload', profileController.userImageUpload);

// router.get('/facebook/callback', facebookController.facebookCallback);
// Phone verification
router.post('/phone/check', profileController.phoneCheck);
router.post('/send/OTP', profileController.sendOTP);
router.post('/verify/OTP', profileController.verifyOTP);
// check & change password
router.put('/change/password', ChangePasswordController.changePassword)
router.post('/checkpassword', ChangePasswordController.checkcurrentpassword);
// notification
router.put('/notification', NotificationController.settings);
//email verify
router.post('/checkemail', EmailVerificationController.checkemail);
router.post('/emailverify', EmailVerificationController.email_verify);
router.post('/emailsendlink', EmailVerificationController.email_send_link);
// User skills
router.put('/skills', EmailVerificationController.userskills);
// Bank details
router.get('/user/bank', bankController.getBank);
router.post('/user/bank', bankController.addBank);
router.put('/user/bank', bankController.editBank);
router.delete('/user/bank', bankController.deleteBank);
router.put('/user/bank/default', bankController.defaultBank);
//Payment
router.put('/payment', paymentController.paymentAdd);
router.get('/payment_gst', paymentController.getGstData);
router.post('/payment_add', paymentController.addPaymentPostData);
//get country , state & city lists
router.get('/countrylist', paymentController.countryList);
router.post('/statelist', paymentController.stateList);
router.post('/citylist', paymentController.cityList);
router.get('/currency/list', paymentController.currencyList);

// User FB connect
router.get('/facebook/auth', facebookController.facebookCodeURI);
router.post('/facebook/callback', facebookController.facebookConnect);
// Linked in Social Login
router.get('/linkedin', linkedInController.linkedInCheck);
router.post('/linkedin-callback', linkedInController.linkiedInLogin);
// You tube Social Login
router.get('/youtube', youtubeController.youtubeLogin);
router.post('/youtube-callback', youtubeController.YoutubeCallBack);
//twitter connection api
router.get('/twitterconnection', twitterController.twitterConnection);
router.post('/twitterdata', twitterController.getData);
//instagram connection api
router.get('/instagram', instagramController.authorize_user);
router.post('/instagram-callback', instagramController.handleauth);
//instagram business connection api
router.get('/instagram/business', instagramBusinessController.instagramBusinessCodeURI);
router.post('/instagram-business/callback', instagramBusinessController.instagramBusinessConnect);


// Get Country ISD Code
router.get('/country/isd', paymentController.countryISD);
// Add Blog
router.post('/user/blog', socialController.addBlog);
// Delete Blog
router.post('/user/blog/delete', socialController.deleteBlog);
// login Instagram api
router.get('/logininstagram', instagramController.loginInstagram);
router.post('/logininstagram-callback', instagramController.loginInstagramCallback);
//Advertiser
router.post('/advertisers', advertiserController.advertiserList);
router.post('/advertiser/details', advertiserController.advertiserData);
router.post('/advertiser/create', advertiserController.createAdvertiser);
router.post('/advertiser/imageupload', advertiserController.advertiserImageUpload);
router.post('/advertiser/edit', advertiserController.editAdvertiser);
router.post('/advertiser/status', advertiserController.statusUpdate);
// For Admin
// Organisation management
router.post('/organisations', organisationController.organisationList);
router.post('/searchOrganisation', advertisersController.searchOrganisation);
router.post('/add-organisation', organisationController.postOrganisationdata);
router.post('/update-organisation', organisationController.updateOrganisation);
router.post('/location-organisation', organisationController.updateOrganisationAddresses);
router.post('/organisation/details', organisationController.details);
router.post('/get-organisation', organisationController.getOrganisationdata);
// Users management
router.post('/admin/list', adminController.usersList);
// admin Login
router.post('/admin/login', adminController.adminLogin);
// admin create
router.post('/createadmin', adminController.createAdmin);
// admin status
router.post('/admin/statusupdate', adminController.updateStatus);
//admin update
router.post('/admin/update', adminController.adminUpdate);
//admin update password
router.post('/admin/passwordupdate', adminController.adminPasswordUpdate);
router.post('/admin/getdata', adminController.getUserData);
//admin user image upload
router.post('/admin/imageupload', adminController.adminUserImageUpload);
//admin change password
router.post('/admin/changepassword', adminController.adminChangePassword);
// admin list
router.get('/users/list', adminController.adminList);
//**********Influencer section **********************/
router.post('/influencer/create', influencerController.createInfluencer);
router.post('/influencer/checkEmail', influencerController.checkEmailExist);
router.post('/influencer/editDetailInfluencer', influencerController.editDetailInfluencer);
router.post('/influencer/detail', influencerController.getInfluencerDetail);
router.post('/influencer/status', influencerController.statusUpdate);
router.post('/influencer/list', influencerController.getInfluencerList);
router.post('/influencer/getInfluencerListDataForExcel', influencerController.getInfluencerListDataForExcel);

router.post('/influencer/editSocialDetailInfluencer', influencerController.editSocialDetailInfluencer);
router.post('/influencer/deleteSocialCard', influencerController.deleteSocialCard);
router.post('/influencer/genreList', influencerController.genreList);
router.post('/influencer/checkHandleUrl', influencerController.checkHandleUrl);
/******************** Influencer Campaign Management ********************/
router.post('/influencer/campaign/new', influencerCampaignController.getCampaignNewList);
router.post('/influencer/campaign/detail', influencerCampaignController.campaignDetail);
router.post('/influencer/campaign/completed', influencerCampaignController.getCampaignCompletedList);
router.post('/influencer/actioncampaign', influencerCampaignController.actionInfluencerCampaign);
// Influencer Campaign Twitter Post
router.post('/influencer/campaign/twitter/post', influencerCampaignController.campaignTwitterPost);
/******************** Influencer Campaign Management Ends ********************/
router.post('/influencer/excelupload', excelUploadController.uploadExcel);
router.post('/influencer/checkauthentication', influencerCampaignController.checkUserAuthentication);

//Get All social platforms of influencer
router.post('/influencer/get_all_platforms', influencerController.getAllPlatforms);
router.post('/influencer/reweetcount', influencerCampaignController.retweetCountUpdate);

// Route to add super admin
// router.post('/superadmin', adminController.superAdmin)

// Test route to hash admin password
// router.get('/admin/hash', adminController.hashAdminPassword);

// //Test route to hash advertiser password
// router.get('/adver/hash', advertiserController.hashAdverPassword);

// //Test route to hash influencer password
// router.get('/influencer/hash', userController.hashInfluencerPassword);

router.post('/admin/reset-password', adminController.adminResetPassword)
router.post('/influencer/getMaxReachCountData', influencerController.getMaxReachCountData);
router.post('/skilldefaultSetting', profileController.skillSettings);

/*Export*/
module.exports = router;