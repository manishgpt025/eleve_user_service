let jwt = require('jsonwebtoken');
const config = require('./config.js');
const secret = `${global.gConfig.secret}`;
var https = require('http');
// user services
const adminService = require('./services/adminService.js');

const responseHandle = require('./globalFunctions/responseHandle.js');
const responseCode = require('./globalFunctions/httpResponseCode.js');
const responseMessage = require('./globalFunctions/httpResponseMessage.js');
const ObjectId = require('mongodb').ObjectID;

let checkToken = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }

    if (token) {
        jwt.verify(token, secret, (err, decoded) => {
            if (err) {
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Auth token is not supplied');
    }
};

let checkAdminUser = (req, res, next) => {
    // console.log(JSON.stringify(req.headers));
    // return;
    let user_id = req.headers['user_id'];
    console.log(user_id);
    adminService.findData({ '_id': new ObjectId(user_id) }, (error, result) => {
        console.log('midd====', result);
        if (error) {
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);

        } else if (result.status == "1") {
            next();
        } else {
            return responseHandle.sendResponseWithData(res, 5001, 'Admin is Inactive');
        }
    });
};

module.exports = {
    checkToken: checkToken,
    checkAdminUser: checkAdminUser
}