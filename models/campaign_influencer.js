const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let Influencer = new schema({
    campaign_influencer_id: {
        type: String //autogenerrated unique id
    },
    campaign_id: {
        type: schema.Types.ObjectId,
        ref: "campaign" // campaign id relation
    },
    influencer_id: {
        type: schema.Types.ObjectId,
        ref: "influencers" // influencer_id relation
    },

    platform: {
        type: String // platform name (like twitter,instagram...)
    },
    type: {
        type: String //platform type (micro / premium)
    },
    platform_Elv_social: {
        type: String // platform Elv social
    },
    category: {
        type: String // platform social category
    },
    cost: {
        type: String //platform social cost
    },
    package_cost: {
        type: String // for premium either "cost" or "package_cost" will be empty (premium)
    },
    currency_code: {
        type: String // currency code of platform (premium)
    },
    action: {
        type: String, // "accepted" "denied"
        default: ""
    },
    post_needed: {
        type: String, // no of post
    },
    post_done: {
        type: Number, //
        default: 0
    },
    retweet_done: [{
        url: {
            type: String //Retweet url
        },
        status: {
            type: String // status done
        },
    }],
    amount: {
        type: String //
    },
    source: {
        type: String //
    },
    clicked_at: {
        type: String // email link click
    },
    created_at: {
        type: Date,
        default: new Date() // created date
    },
    updated_at: {
        type: Date,
        default: new Date() // updated date
    },
    premium_brief: {
        type: String // (premium)
    }
});

Influencer.plugin(mongoosePaginate)
Influencer.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('campaign_influencer', Influencer);