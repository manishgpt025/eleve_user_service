const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

let InfluencerModel = new schema({
    influencer_id: {
        type: String, // randam id for influencer
        default: ""
    },
    first_name: {
        type: String // first_name of the influencer
    },
    last_name: {
        type: String // last_name of the influencer
    },
    email: {
        type: String // email of the influencer
    },
    alt_email: {
        type: String // alt_email of the influencer
    },
    mobile: {
        type: String //mobile of the influencer
    },
    alt_mobile: {
        type: String // alt_mobile of the influencer
    },
    password: {
        type: String // password of the influencer
    },
    resetPasswordToken: {
        type: String //resetpassword token of the influencer
    },
    resetPasswordExpires: {
        type: Date // reset password expires time of the influencer
    },
    campaign_notification: {
        type: Array // for campaign notification and this type array incldues sms and emal key
    },
    payment_notification: {
        type: Array // for payment notification and this type array incldues sms and emal key
    },
    account_notification: {
        type: Array // for account notification and this type array incldues sms and emal key
    },
    new_feature_notification: {
        type: Array // for new_feature notification and this type array incldues sms and emal key
    },
    email_verification: {
        type: String // for email_verification of the influencer
    },
    emailverificationToken: {
        type: String // email verification token of the influencer
    },
    emailverificationExpires: {
        type: String // email verification expries token key
    },
    interests: {
        type: Array // interests of the influencer and type is an array that includes max 5 keys which is dynamic
    },
    language: {
        type: Array // languages of the influencer and type is an array that includes max 5 keys which is dynamic
    },
    lifestage: {
        type: Array // lifestage of the influencer and type is an array that includes max 5 keys which is dynamic
    },
    education: {
        type: Array // education of the influencer and type is an array that includes max 5 keys which is dynamic
    },
    income: {
        type: Array // income of the influencer and type is an array that includes max 5 keys which is dynamic
    },
    bank_details: {
        type: Array // it is a type of array includes keys are account_number,bank_name, branch, account_number, ifsc_code, swift_code, status
    },
    payment_details: {
        type: Array // type of array includes keys are pan_number, gst_number, sac_code, gst_code, code, nature_of_service, state_Code,pan_image,gst_image
    },
    twitter: {
        type: Array // type of an array include keys are id,id_str,name,link,screen_name,profile_location,description,followers_count,friends_count,profile_image_url,Elv_name,Elv_social,connection_type
    },
    facebook: {
        type: Array //type of an array include keys are id, email, first_name, last_name, name, picture, accounts,accesstoken, connection_type
    },
    youtube: {
        type: Array //type of an array inclcudes id, picture, name, given_name, family_name
    },
    linkedin: {
        type: Array // type of an array includes id, FirstName, lastName, location, industry, pictureUrl, positions,publicProfieUrl, summary
    },
    instagram: {
        type: Array // type of an array includes username, followers_count, link, connection_type, Elv_name, Elv_social, profile_picture, full_name, bio, website, is_bussiness, counts(media, follows, followed_by)
    },
    snapchat: {
        type: Array
    },
    pinterest: {
        type: Array // type of an array
    },
    blog: {
        type: Array // type of an array includes title, link
    },
    instagram_business: {
        type: Array // type of an array includes id, email, name, profileUrl, pageid, pagename, instagram_business_id, pageName, accessToken
    },
    tiktok: {
        type: Array // type of an array
    },
    address: {
        type: String // address of influencer
    },
    dob: {
        type: String // date of birth of influencer
    },
    country: {
        type: String // country of influencer
    },
    state: {
        type: String //state of influencer
    },
    city: {
        type: String //city of influencer
    },
    isd_code: {
        type: String //isd_code of influencer
    },
    alt_isd_code: {
        type: String // alt_isd_code of influencer
    },
    isd_mobile: {
        type: String // isd_mobile of influencer
    },
    isd_alt_mobile: {
        type: String // isd_alt_mobile of influencer
    },
    gender: {
        type: String //gender of influencer
    },
    otp: {
        type: String // otp for influencer of influencer
    },
    is_verified: {
        type: String // is_verified key denotes the influencer is verified or not
    },
    profile_pic: {
        type: String // pic of the influencer
    },
    registered: {
        type: String // registered denotes eleve registeration by tool
    },
    note: {
        type: String // notes of influencer
    },
    reach_count:{
        type: Number // User reach count
    },
    status: {
        type: String,
        default: "",
        enum: ['', '1', '0','2'] //status - 0(inactive), 1(active), 2(barred)
    },

});

InfluencerModel.pre('save', function(next) {
    var influencer = this;
    if(influencer.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(influencer.password , salt, (err, hash) => {
                influencer.password = hash;
                next();
            });
        });
    }else {
        next();
    }
})

InfluencerModel.pre('findOneAndUpdate', function (next) {
    var influencer = this;

    if(!influencer.getUpdate().$set || !influencer.getUpdate().$set.password) {
        return next();
    }
    var password = influencer.getUpdate().$set.password;
    if(password) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password , salt, (err, hash) => {
                influencer.getUpdate().$set.password = hash;
                next();
            });
        });
    }else {
        next();
    }
  });

InfluencerModel.plugin(mongoosePaginate)
InfluencerModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('influencers', InfluencerModel);