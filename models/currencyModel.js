const mongoose = require('mongoose');
const schema = mongoose.Schema;

let currencyModel = new schema({
    Country: {
        type: String // country name
    },
    Currencycode: {
        type: String // currency code
    }
});

module.exports = mongoose.model('currencylists', currencyModel);