const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let genreModel = new schema({
    genre: {
        type: String //gnere name
    },
    subgenre: {
        type: String //subgenre name
    },
    status : {
    	type : Boolean,
    	defult : true
    }
});

genreModel.plugin(mongoosePaginate)
genreModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('genre', genreModel);