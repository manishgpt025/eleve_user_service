const mongoose = require('mongoose');
const schema = mongoose.Schema;
let countryModel = new schema({
    // id: {
    // 	type: String
    // },
    // name: {
    // 	type: String
    // },
    // country_id: {
    // 	type: String
    // },
    // state_id: {
    // 	type: String
    // },
    // city_id: {
    // 	type: String
    // },
    name: {
        type: String //name of the country
    },
    dial_code: {
        type: String // dial_Code
    },
    code: {
        type: String // code of the country
    },

});

module.exports = mongoose.model('country_codes', countryModel);