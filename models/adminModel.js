const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
var bcrypt = require('bcryptjs');

const schema = mongoose.Schema;
let adminModel = new schema({
    admin_id: {
        type: String // this is autognerated unique id of user
    },
    date: {
        type: String // date of creation
    },
    email: {
        type: String // email of the admin user
    },
    last_login_at: {
        type: String //last login date and time of user in the admin
    },
    logins: {
        type: Array // its an array stores logins date and time
    },
    name: {
        type: String // first_name of user
    },
    password: {
        type: String //password of the login user
    },
    permission: {
        type: String // permission given to the user
    },
    status: {
        type: String //status of user either 0(active) or 1(active)
    },
    address: {
        type: String //address of the user
    },
    last_login_from: {
        type: String // ip address of user
    },
    role: {
        type: String // role of user either admin or superadmin
    },
    mobile: {
        type: String // mobile of user
    },
    isd: {
        type: String //isd code of user
    },
    team: {
        type: String //team of the user
    },
    modules: {
        type: Array // type of an array inlcudes All, campaign_module, influencer_module
    },
    dob: {
        type: String //date of birth of user
    },
    location: {
        type: String //location of user
    },
    extension: {
        type: String //other phone number of user
    },
    first_login: {
        type: String // key has 0(not a first time login) or 1(first_time login)
    },
    last_name: {
        type: String // last_name of user
    },
    profile_image: {
        type: String //image of user
    }

});

adminModel.pre('save', function(next) {
    var admin = this;
    var test;
    if(admin.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(admin.password , salt, (err, hash) => {
                admin.password = hash;
                test = hash;
                next();
            });
        });
    }else {
        next();
    }
})

adminModel.pre('findOneAndUpdate', function (next) {
    var admin = this;
    if(!admin.getUpdate().$set || !admin.getUpdate().$set.password) {
        return next();
    }
    var password = admin.getUpdate().$set.password;
    if(password) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password , salt, (err, hash) => {
                this.getUpdate().$set.password = hash;
                next();
            });
        });
    }else {
        next();
    }
  });

adminModel.plugin(mongoosePaginate)
adminModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('admin_user', adminModel);