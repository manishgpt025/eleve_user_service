const mongoose = require('mongoose');
const schema = mongoose.Schema;
let cityModel = new schema({
    id: {
        type: String //auto genreated unique id
    },
    name: {
        type: String //name of city
    },
    country_id: {
        type: String // country_id denotes which country belongs the city
    },
    state_id: {
        type: String // denotes which state id belongs
    },
    city_id: {
        type: String //city id 
    }

});

module.exports = mongoose.model('cities', cityModel);