const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let Campaign = new schema({
    campaign_id: {
        type: String //autogenerrated unique id
    },
    title: {
        type: String // title
    },
    type: {
        type: String // Objective type
    },
    hashtag: {
        type: String // Primary hashtag
    },
    hashtag_share: {
        type: Boolean,
        default: false //status - false(no), true(yes) mandatory to share
    },
    url: {
        type: String // url
    },
    url_share: {
        type: Boolean,
        default: false //status - false(no), true(yes) mandatory to share
    },
    secondary_hashtag: {
        type: Array //secondary_hashtag
    },
    track_secondary_hashtag: {
        type: Boolean,
        default: false //status - false(no), true(yes)
    },
    keywords: {
        type: Array // keywords to be share on social media
    },
    keywords_share: {
        type: Boolean,
        default: false //status - false(no), true(yes)  mandatory to share
    },
    track_keywords: {
        type: Boolean,
        default: false //status - false(no), true(yes)
    },
    retweet_url: {
        type: Array //retweet url for to be share on social media
    },
    retweet_id: {
        type: Array //retweet url ID
    },
    images: [{
        url: {
            type: String // Social Images for campaign
        },
    }],
    video: {
        type: String // Social Video for campaign
    },
    media_share: {
        type: Boolean,
        default: false //status - false(no), true(yes)  mandatory to share
    },
    advertiser_type: {
        type: String //Advertiser type as Agency,brand or text
    },
    organization_id: {
        type: String, // organization id
        default: ""
    },
    advertiser_id: {
        type: String, // advertiser id
        default: ""
    },
    advertiser_brand_id: {
        type: String, //advertiser id
        default: ""
    },
    organization_name: {
        type: String, // organization name
        default: ""
    },
    advertiser_name: {
        type: String, // advertiser name
        default: ""
    },
    advertiser_brand_name: {
        type: String, //advertiser brand name
        default: ""
    },
    ro_number: {
        type: String // release number
    },
    eo_number: {
        type: String // number
    },
    manager_id: {
        type: String // Admin Id
    },
    colleagues: {
        type: Array // admin ids
    },
    platforms: [{
        name: {
            type: String //platform name (like twitter,instagram...)
        },
        type: {
            type: String //platform type (micro / premium)
        },
        number_influencer: {
            type: String //number of influencer
        },
        number_post: {
            type: String //number of post
        },
        number_conversation: {
            type: String //number of conversation
        }
    }],
    number_influencer: [{
        name: {
            type: String //platform name (like twitter,instagram...)
        },
        type: {
            type: String //platform type (micro / premium)
        },
        count: {
            type: String //number of influencer
        }
    }],
    number_post: [{
        name: {
            type: String //platform name (like twitter,instagram...)
        },
        type: {
            type: String //platform type (micro / premium)
        },
        count: {
            type: String //number of post
        }
    }],
    conversations: [{
        name: {
            type: String //platform name (like twitter,instagram...)
        },
        type: {
            type: String //platform type (micro / premium)
        },
        count: {
            type: String //number of conversations
        }
    }],
    start_date: {
        type: String // Start date for campaign
    },
    end_date: {
        type: String // End date for campaign
    },
    created_at: {
        type: Date,
        default: new Date() // campaign created date
    },
    created_id: {
        type: schema.Types.ObjectId,
        ref: "admin_users"     // Created id from admin
    },
    created_by: {
        type: String // Created name
    },
    updated_at: {
        type: Date,
        default: new Date() // campaign updated date
    },
    updated_by: {
        type: String // Updated by
    },
    status: {
        type: String,
        default: "0",
        enum: ["0", "1", "2", "3", "4", "5", "6", "7", "8"] // 0=Draft, 1=Active, 2=Schedule, 3=Live, 4=Ended, 5=Pending, 6=Completed, 7=Deleted , 8=Paused
    },
    approved: {
        type: String // Approve campaign
    },
    updated_flag: {
        type: Boolean,
        default: false, //status - false(no), true(yes)
    },
    active_flag: {
        type: Boolean,
        default: false, //status - false(no), true(yes)
    },
    mentions: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        name: [{
            type: Array //names of mentions handles
        }]
    }],
    platform_specific: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        is_gender: {
            type: String //description
        }
    }],

    description: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        description: {
            type: String //description
        }
    }],
    male_description: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        description: {
            type: String //male description
        }
    }],
    female_description: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        description: {
            type: String //female description
        }
    }],
    other_description: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        description: {
            type: String //other description
        }
    }],
    sample: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        sample: {
            type: String //sample
        },
    }],
    male_sample: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        sample: {
            type: String // male sample
        }
    }],
    female_sample: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        sample: {
            type: String // female sample
        },
    }],
    other_sample: [{
        platform: {
            type: String //platform name (like twitter,instagram...)
        },
        sample: {
            type: String // other sample
        },
    }],
    micro_common_brief: {
        type: String, // copy description from all platform
        default:""
    },
    is_invited: {
        type: Boolean,
        default: false, //status - false(no), true(yes) // Invitation flag
    },
    send_premium: {
        type: String, // check for premium influencer
        default: "0"
    },
    premium_common_brief: [{
        platform: {
            type: String //check to primium breif for all influencer
        },
        status: {
            type: String, // status
            default: "0"
        },
    }],
    send_email: { // check to send email
        type: String,
        default: "0"
    },
    send_micro: { // check to send email or sms to micro influencer
        type: String,
        default: "0"
    },
    send_sms: { // check to send sms
        type: String,
        default: "0"
    },
    email_sender: { // sender of email
        type: String
    },
    email_subject: { //email subject
        type: String
    },
    reply_to: { // check for reply to email
        type: String
    },
    email_body: {
        type: String //email of body
    },
    sms_body: {
        type: String //body of sms
    }
});

Campaign.plugin(mongoosePaginate)
Campaign.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('campaign', Campaign);