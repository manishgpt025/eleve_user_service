
const userService = require('../services/userServices.js');// user services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const ObjectId = require('mongodb').ObjectID;// Obj ID

/*create apis*/
/**
 * [Get bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getBank = (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.getOnlyData({ _id: req.body.user_id }, { 'bank_details': 1 }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Bank record found!', userInfo);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Bank record not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Add bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addBank = (req, res) => {
    let check = helper.checkRequest(["user_id", "account_holder", "bank_name", "account_number"], req.body);
    if (check == true) {
        let queryData = {
            _id: new ObjectId(req.body.user_id),
            'bank_details.account_number': { $exists: true }
        }
        // Check from DB Collection
        userService.findData(queryData, (error_, userInfo) => {
            if (error_) {
                responseHandle.sendResponsewithError(res, 500, error_);
            } else {
                let queryData = {
                    _id: new ObjectId(req.body.user_id),
                    'bank_details.account_number': req.body.account_number
                }
                userService.findData(queryData, (error_2, userExist) => {
                    if (error_2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_2);
                    } else if (userExist) {
                        responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This bank detail already added.');
                    } else {
                        let data = {
                            "account_holder": req.body.account_holder,
                            "bank_name": req.body.bank_name,
                            "branch": req.body.branch ? req.body.branch : '',
                            "account_number": req.body.account_number,
                            "ifsc_code": req.body.ifsc_code ? req.body.ifsc_code:'',
                            "swift_code": req.body.swift_code ? req.body.swift_code:''
                        };
                        if (userInfo) {
                            data.status = 0;
                        } else {
                            data.status = 1;
                        }
                        let myquery = { _id: new ObjectId(req.body.user_id) };
                        let newvalues = {$push: {bank_details: data}};
                        userService.arrayUpdateRemove(myquery, newvalues, (error_3, userupdate) => {
                            if (error_3) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_3);
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Bank detail added successfully.',userupdate);
                            }
                        });
                    }
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Edit bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const editBank = (req, res) => {
    let check = helper.checkRequest(["user_id", "old_account_number","account_number"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.findData({ _id: new ObjectId(req.body.user_id), 'bank_details.account_number': req.body.old_account_number }, (error_, userInfo) => {
            if (error_) {
                responseHandle.sendResponsewithError(res, 500, error_);
            } else {
                if (userInfo) {
                    let data = {
                        "bank_details.$.account_holder": req.body.account_holder,
                        "bank_details.$.bank_name": req.body.bank_name,
                        "bank_details.$.branch": req.body.branch ? req.body.branch:'',
                        "bank_details.$.account_number": req.body.account_number,
                        "bank_details.$.ifsc_code": req.body.ifsc_code ?req.body.ifsc_code : '',
                        "bank_details.$.swift_code": req.body.swift_code ? req.body.swift_code:''
                    };
                    let myquery = {
                        _id: new ObjectId(req.body.user_id),
                        'bank_details.account_number': req.body.old_account_number
                    };
                    let newvalues = {$set: data};
                    userService.arrayUpdate(myquery, newvalues, (error2, userupdate) => {
                        if (error2) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Bank Updated');
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'No record found');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Delete bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const deleteBank = (req, res) => {
    let check = helper.checkRequest(["user_id", "account_number"], req.body);
    if (check == true) {
        // Check from DB Collection
        let data = {"account_number": req.body.account_number};
        let myquery = { _id: new ObjectId(req.body.user_id) };
        let newvalues = {$pull: {bank_details: data}};
        userService.arrayUpdateRemove(myquery, newvalues, (error, userupdate) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Bank removed');
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Degault bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const defaultBank = (req, res) => {
    let check = helper.checkRequest(["user_id", "normal_account_number", "default_account_number"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.findData({ _id: new ObjectId(req.body.user_id), 'bank_details.account_number': req.body.default_account_number }, (error_, userInfo) => {
            if (error_) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
            } else {
                if (userInfo) {
                    let data = {"bank_details.$.status": 1};
                    let myquery = {
                        _id: new ObjectId(req.body.user_id),
                        'bank_details.account_number': req.body.default_account_number
                    };
                    let newvalues = {$set: data};
                    userService.arrayUpdate(myquery, newvalues, (error2, userupdate) => {
                        if (error2) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                        } else {
                            let data1 = {"bank_details.$.status": 0};
                            let myquery1 = {
                                _id: new ObjectId(req.body.user_id),
                                'bank_details.account_number': req.body.normal_account_number
                            };
                            let newvalues1 = {$set: data1};
                            userService.arrayUpdate(myquery1, newvalues1, (error2, userupdate) => {
                                if (error2) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Bank detail updated successfully.');
                                }
                            });
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'No bank detail found');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Get bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getAccountByUserId = (req, res) => {
    let check = helper.checkRequest(["id", "number"], req.params);
    if (check == true) {
        // Check from DB Collection
        userService.getOnlyData({
            _id: new ObjectId(req.params.id),
            'bank_details.account_number': req.params.number
        }, { 'bank_details': 1 }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Bank record found!', userInfo);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Bank record not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/* Export apis */
module.exports = {
    getBank,
    addBank,
    editBank,
    deleteBank,
    defaultBank,
    getAccountByUserId
}