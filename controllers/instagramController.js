const request = require('request-promise');
const qs = require('qs');
//use helper
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
// user services
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;

let jwt = require('jsonwebtoken');

// use Instagram-node
const api = require('instagram-node').instagram();
api.use({
    client_id: process.env.INSTAGRAM_CLIENT_ID,
    client_secret: process.env.INSTAGRAM_CLIENT_SECRET_KEY
});
const redirect_uri = process.env.INSTAGRAM_REDIRECT_URI;

//insta login
const authorize_user = function(req, res) {
    var api = require('instagram-node').instagram();
    api.use({
        client_id: process.env.INSTAGRAM_CLIENT_ID,
        client_secret: process.env.INSTAGRAM_CLIENT_SECRET_KEY
    });
    var redirect_uri = process.env.INSTAGRAM_REDIRECT_URI;
    var login_url = api.get_authorization_url(redirect_uri, { scope: ['likes'], state: 'a state' });
    //console.log(login_url);
    return res.status(200).send({ login_url });

    // res.redirect(api.get_authorization_url(redirect_uri, { scope: ['likes'], state: 'a state' }));
};
// insta login callback
const handleauth = function(req, res) {
    let user_id = req.body.user_id;
    var instagramData = {};
    var api = require('instagram-node').instagram();
    api.use({
        client_id: process.env.INSTAGRAM_CLIENT_ID,
        client_secret: process.env.INSTAGRAM_CLIENT_SECRET_KEY
    });
    var redirect_uri = process.env.INSTAGRAM_REDIRECT_URI;
    console.log("in insta call back ");

    api.authorize_user(req.body.code, redirect_uri, function(err, result) {
        if (err) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
        } else {
            let log_access_token = result.access_token;
            console.log('Access token is ' + log_access_token);
            api.use({
                client_id: "0c8a1c22bd774109bb8fbab3100746dc",
                client_secret: "3a5d846ad57b4159986afaf6b9f7fff0",
                access_token: log_access_token
            });
            api.user(log_access_token.split('.')[0], function(error, result, remaining, limit) {
                if (error) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    console.log(result);
                    instagramData.id = result.id;
                    instagramData.username = result.username;
                    instagramData.profile_picture = result.profile_picture;
                    instagramData.full_name = result.full_name;
                    instagramData.bio = result.bio;
                    instagramData.website = result.website;
                    instagramData.is_business = result.is_business;
                    instagramData.counts = result.counts;
                }
                let myquery = {
                    _id: { $ne: new ObjectId(user_id) },
                    'instagram.id': result.id
                };
                userService.findData(myquery, (err, userInfo) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else {
                        if (userInfo) {
                            responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Please try with diffrent account, this account is already exists.');
                        } else {
                            // Saving data
                            let query = {
                                _id: new ObjectId(req.body.user_id),
                                'instagram.id': result.id
                            };
                            userService.findData(query, (error_, userInfo1) => {
                                if (error_) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                } else {
                                    if (userInfo1) {
                                        responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'You already have this account mentioned on your profile');

                                    } else {
                                        var query1 = {
                                            _id: new ObjectId(req.body.user_id),
                                        }
                                        var data = {
                                            $push: {
                                                instagram: instagramData
                                            }
                                        }
                                        userService.updateOne(query1, data, {
                                            new: true
                                        }, (error, updatedResp) => {
                                            if (error) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                            } else {
                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User linked in account added Successfully!', updatedResp);
                                            }
                                        });
                                    }

                                }
                            });
                        }
                    }
                });
            });
        }
    });
};

// Login with Instagram
const loginInstagram = function(req, res) {
    var api = require('instagram-node').instagram();
    api.use({
        client_id: process.env.INSTAGRAM_CLIENT_ID,
        client_secret: process.env.INSTAGRAM_CLIENT_SECRET_KEY
    });
    var redirect_uri = process.env.INSTAGRAM_LOGIN_REDIRECT_URI;
    var login_url = api.get_authorization_url(redirect_uri, { scope: ['likes'], state: 'a state' });
    //console.log(login_url);
    return res.status(200).send({ login_url });

    // res.redirect(api.get_authorization_url(redirect_uri, { scope: ['likes'], state: 'a state' }));
};

// Login Instagram Callback
const loginInstagramCallback = function(req, res) {
    let user_id = req.body.user_id;
    var instagramData = {};
    var api = require('instagram-node').instagram();
    api.use({
        client_id: process.env.INSTAGRAM_CLIENT_ID,
        client_secret: process.env.INSTAGRAM_CLIENT_SECRET_KEY
    });
    var redirect_uri = process.env.INSTAGRAM_LOGIN_REDIRECT_URI;
    console.log("in insta call back ");

    api.authorize_user(req.body.code, redirect_uri, function(err, result) {
        if (err) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
        } else {
            let log_access_token = result.access_token;
            console.log('Access token is ' + log_access_token);
            api.use({
                client_id: "0c8a1c22bd774109bb8fbab3100746dc",
                client_secret: "3a5d846ad57b4159986afaf6b9f7fff0",
                access_token: log_access_token
            });
            api.user(log_access_token.split('.')[0], function(error, result, remaining, limit) {
                if (error) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    console.log(result);
                    instagramData.id = result.id;
                    instagramData.username = result.username;
                    instagramData.profile_picture = result.profile_picture;
                    instagramData.full_name = result.full_name;
                    instagramData.bio = result.bio;
                    instagramData.website = result.website;
                    instagramData.is_business = result.is_business;
                    instagramData.counts = result.counts;
                }
                let myquery = {
                    'instagram.id': result.id
                };
                userService.findData(myquery, (err, userInfo) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else {
                        if (userInfo) {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'We could not find any profile associated with this Instagram account', userInfo);

                        } else {
                            responseHandle.sendResponseWithError(res, responseCode.NOT_FOUND, 'User Not Found!');

                        }
                    }
                });
            });
        }
    });
};
/*Export apis*/
module.exports = {
    authorize_user: authorize_user,
    handleauth: handleauth,
    loginInstagram: loginInstagram,
    loginInstagramCallback: loginInstagramCallback
}