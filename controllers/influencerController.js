// user model
const userModel = require('../models/userModel.js');
// genre model
const genreModel = require('../models/genreModel.js');
// user services
const userService = require('../services/userServices.js');
//Genre Services
const genreService = require('../services/genreService.js');
const adminService = require('../services/adminService.js');
const randomstring = require("randomstring");
//use helper
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
// Obj ID
const ObjectId = require('mongodb').ObjectID;
//S3 Upload
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
//social platforms
var platforms = require('../social_platforms');

/**
 * [Create the user data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const createInfluencer = (req, res) => {
    let check = helper.checkRequest(["first_name", "email_id", "email_valid"], req.body);
    if (check == true) {
        let newId = helper.generateRandomString();
        let userPassword = randomstring.generate(7);
        if (req.body.email_valid == "1") {
            let myquery = {
                email: req.body.email_id
            };
            userService.findData(myquery, (err, userInfo) => {
                if (err) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                } else {
                    if (userInfo) {
                        let myquery = {
                            _id: userInfo._id
                        };
                        let newvalues = {
                            $set: {
                                email: "",
                            }
                        }
                        userService.updateOne(myquery, newvalues, {
                            new: true
                        }, (error, userupdate) => {

                            let obj = new userModel({
                                influencer_id: newId,
                                first_name: req.body.first_name,
                                last_name: req.body.last_name,
                                gender: req.body.gender,
                                dob: req.body.dob,
                                country: req.body.country,
                                state: req.body.state,
                                city: req.body.city,
                                address: req.body.address,
                                email: req.body.email_id,
                                password: userPassword,
                                alt_email: req.body.alt_email,
                                isd_code: req.body.isd_code,
                                mobile: req.body.mobile,
                                alt_isd_code: req.body.alt_isd_code,
                                alt_mobile: req.body.alt_mobile,
                                isd_mobile: req.body.isd_code + req.body.mobile,
                                isd_alt_mobile: req.body.alt_isd_code + req.body.alt_mobile,
                                note: req.body.note,
                                twitter: req.body.twitter,
                                facebook: req.body.facebook,
                                youtube: req.body.youtube,
                                linkedin: req.body.linkedin,
                                instagram: req.body.instagram,
                                pinterest: req.body.pinterest,
                                blog: req.body.blog,
                                tiktok: req.body.tiktok,
                                registered: "1",
                                status: ""
                            })
                            obj.save((error, userData) => {
                                if (error) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                } else {
                                    if (req.body.profilePic) {
                                        buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                                        let number = Math.random() * (999999 - 10000) + 10000
                                        let resData = {
                                            Key: userData._id + number,
                                            Body: buf,
                                            ContentEncoding: 'base64',
                                            ContentType: 'image/jpeg',
                                            ACL: 'public-read'
                                        };
                                        s3Bucket.upload(resData, function(err, data) {
                                            if (err) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                            } else {
                                                let myquery = {
                                                    _id: userData._id
                                                };
                                                let imageValue = {
                                                    $set: {
                                                        profile_pic: data.Location,
                                                    }
                                                }
                                                userService.updateOne(myquery, imageValue, {
                                                    new: true
                                                }, (error_, userupdate) => {
                                                    if (error_) {
                                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                    } else {
                                                        var message = {
                                                            "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email_id + "<br/> Password:- " + userPassword + "</p>",
                                                            "subject": "Credentials",
                                                            "from_email": "team@eleve.co.in",
                                                            "from_name": "Eleve",
                                                            "to": [{
                                                                "email": req.body.email_id,
                                                                "type": "to"
                                                            }]
                                                        };
                                                        helper.sendEmail(message);
                                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer created Successfully.', userupdate);
                                                    }
                                                });
                                            }
                                        });

                                    } else {
                                        var message = {
                                            "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email_id + "<br/> Password:- " + userPassword + "</p>",
                                            "subject": "Credentials",
                                            "from_email": "team@eleve.co.in",
                                            "from_name": "Eleve",
                                            "to": [{
                                                "email": req.body.email_id,
                                                "type": "to"
                                            }]
                                        };
                                        helper.sendEmail(message);
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer created Successfully.', userData);
                                    }
                                }
                            });
                        });
                    } else {
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Email Id not exist already.Please check again.');
                    }
                }
            });
        } else {
            let obj = new userModel({
                influencer_id: newId,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                gender: req.body.gender,
                dob: req.body.dob,
                country: req.body.country,
                state: req.body.state,
                city: req.body.city,
                address: req.body.address,
                email: req.body.email_id,
                password: userPassword,
                alt_email: req.body.alt_email,
                isd_code: req.body.isd_code,
                mobile: req.body.mobile,
                alt_isd_code: req.body.alt_isd_code,
                alt_mobile: req.body.alt_mobile,
                isd_mobile: req.body.isd_code + req.body.mobile,
                isd_alt_mobile: req.body.alt_isd_code + req.body.alt_mobile,
                note: req.body.note,
                twitter: req.body.twitter,
                facebook: req.body.facebook,
                youtube: req.body.youtube,
                linkedin: req.body.linkedin,
                instagram: req.body.instagram,
                pinterest: req.body.pinterest,
                blog: req.body.blog,
                tiktok: req.body.tiktok,
                registered: "1",
                status: ""
            })
            obj.save((error, userData) => {
                if (error) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    if (req.body.profilePic) {
                        buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                        let number = Math.random() * (999999 - 10000) + 10000
                        let resData = {
                            Key: userData._id + number,
                            Body: buf,
                            ContentEncoding: 'base64',
                            ContentType: 'image/jpeg',
                            ACL: 'public-read'
                        };
                        s3Bucket.upload(resData, function(err, data) {
                            if (err) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                            } else {
                                let myquery = {
                                    _id: userData._id
                                };
                                let imageValue = {
                                    $set: {
                                        profile_pic: data.Location,
                                    }
                                }
                                userService.updateOne(myquery, imageValue, {
                                    new: true
                                }, (error_, userupdate) => {
                                    if (error_) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        var message = {
                                            "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email_id + "<br/> Password:- " + userPassword + "</p>",
                                            "subject": "Credentials",
                                            "from_email": "team@eleve.co.in",
                                            "from_name": "Eleve",
                                            "to": [{
                                                "email": req.body.email_id,
                                                "type": "to"
                                            }]
                                        };
                                        helper.sendEmail(message);
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer created Successfully.', userupdate);
                                    }

                                });
                            }
                        });

                    } else {
                        var message = {
                            "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email_id + "<br/> Password:- " + userPassword + "</p>",
                            "subject": "Credentials",
                            "from_email": "team@eleve.co.in",
                            "from_name": "Eleve",
                            "to": [{
                                "email": req.body.email_id,
                                "type": "to"
                            }]
                        };
                        helper.sendEmail(message);
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer created Successfully.', userData);
                    }
                }
            });
        }

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

}

/**
 * [Verify Email]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkEmailExist = (req, res) => {
    let check = helper.checkRequest(["email_id"], req.body);
    if (check == true) {
        // Check OTP is valid or not
        let myquery = {
            email: req.body.email_id,
            registered: '1'
        };
        userService.findData(myquery, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else if (!resp) {
                // responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Email Id not found.');
                let myquery1 = {
                    email: req.body.email_id
                };
                userService.findData(myquery1, (error_, resp_) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                    } else if (!resp_) {
                        responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Email Id not found.');
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.DUPLICATE_REGNO, 'Email Id already exist in unregistered influencer.');
                    }
                });
            } else {
                responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'Email Id already exist in registered influencer.');
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [Edit Basic detail for influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const editDetailInfluencer = (req, res) => {
    let check = helper.checkRequest(["influencer_id", "first_name", "email_id"], req.body);
    if (check == true) {
        // Check OTP is valid or not
        let myquery = {
            _id: new ObjectId(req.body.influencer_id),
        };
        userService.findData(myquery, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else if (!resp) {
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not found. ');
            } else {
                let data = {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    gender: req.body.gender,
                    dob: req.body.dob,
                    country: req.body.country,
                    state: req.body.state,
                    city: req.body.city,
                    address: req.body.address,
                    alt_email: req.body.alt_email,
                    isd_code: req.body.isd_code,
                    mobile: req.body.mobile,
                    alt_isd_code: req.body.alt_isd_code,
                    alt_mobile: req.body.alt_mobile,
                    isd_mobile: req.body.isd_code + req.body.mobile,
                    isd_alt_mobile: req.body.alt_isd_code + req.body.alt_mobile,
                    note: req.body.note,
                };
                let newvalues = {
                    $set: data
                };
                userService.arrayUpdate(myquery, newvalues, (error2, resp2) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                    } else {
                        if (req.body.profilePic) {
                            buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                            let number = Math.random() * (999999 - 10000) + 10000
                            let resData = {
                                Key: req.body.influencer_id + number,
                                Body: buf,
                                ContentEncoding: 'base64',
                                ContentType: 'image/jpeg',
                                ACL: 'public-read'
                            };
                            s3Bucket.upload(resData, function(err, data) {
                                if (err) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                } else {
                                    let imageValue = {
                                        $set: {
                                            profile_pic: data.Location,
                                        }
                                    }
                                    userService.updateOne(myquery, imageValue, {
                                        new: true
                                    }, (error_, userupdate) => {
                                        if (error_) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                        } else {
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Basic Detail updated successfully.', userupdate);
                                        }

                                    });
                                }
                            });
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Basic Deatil updated successfully.');

                        }
                    }
                });

            }

        });


        // Update the phone with is_verify field

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [Get Influencer detail by Id]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getInfluencerDetail = (req, res) => {
    let check = helper.checkRequest(["influencer_id"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.getData({
            _id: new ObjectId(req.body.influencer_id)
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer found!', userInfo);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Influencer not found!');
                }
            }
        });

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [Influencer status update ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const statusUpdate = (req, res) => {
    let check = helper.checkRequest(["influencer_id", "status"], req.body);
    if (check == true) {
        let obj = {
            status: req.body.status
        }
        let myquery = {
            _id: new ObjectId(req.body.influencer_id)
        };
        let newvalues = {
            $set: obj
        };
        userService.arrayUpdate(myquery, newvalues, (error, result_) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Status Updated Successfully', result_);
            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);

    }
}

/**
 * [Get Influencer List]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getInfluencerList = async(req, res) => {
    let check = helper.checkRequest([], req.body);
    if (check == true) {
        let search = req.body.keyword || "";
        let sortData = req.body.sort;
        //check status
        if (req.body.status == '1') {
            var status = ['1'];
        } else if (req.body.status == '0') {
            var status = ['', '0'];
        } else if (req.body.status == '2') {
            var status = ['2'];
        }
        // check gender
        if (req.body.gender && req.body.gender.length) {
            var gender =  {
                "$match": {
                    gender: {
                        "$in": req.body.gender
                    }
                }
            }
        }
        // check category
        if (req.body.category && req.body.category.length) {
            let category = req.body.category;
            var categoryData =  {
                    "$match": {
                        "$or": [
                            {
                                'facebook.category': {
                                    $in: category
                                }
                            },
                            {
                                'twitter.category': {
                                    $in: category
                                }
                            },
                            {
                                'linkedin.category': {
                                    $in: category
                                }
                            },
                            {
                                'instagram.category': {
                                    $in: category
                                }
                            },
                            {
                                'blog.category': {
                                    $in: category
                                }
                            },
                            {
                                'youtube.category': {
                                    $in: category
                                }
                            },
                            {
                                'tiktok.category': {
                                    $in: category
                                }
                            },
                            {
                                'snapchat.category': {
                                    $in: category
                                }
                            },
                            {
                                'instagram_business.category': {
                                    $in: category
                                }
                            },
                            {
                                'pinterest.category': {
                                    $in: category
                                }
                            },
                        ]
                    }
                }
        }
        // check social platform
        if (req.body.social && req.body.social.length) {
            var social = req.body.social;
            if (social.indexOf('facebook') != -1)
               var facebook =  {"facebook": { $exists: true, $ne: [] }}
            else
                var facebook = {};
            if (social.indexOf('twitter') != -1)
                var twitter =  {"twitter": { $exists: true, $ne: [] }}
            else
                var twitter = {};
            if (social.indexOf('instagram') != -1)
                var instagram =  {"instagram": { $exists: true, $ne: [] }}
            else
                var instagram = {};
            if (social.indexOf('blog') != -1)
                var blog =  {"blog": { $exists: true, $ne: [] }}
            else
                var blog ={};
            if (social.indexOf('snapchat') != -1)
                var snapchat =  {"snapchat": { $exists: true, $ne: [] }}
            else
                var snapchat = {};
            if (social.indexOf('youtube') != -1)
                var youtube =  {"youtube": { $exists: true, $ne: [] }}
            else
                var youtube = {};
            if (social.indexOf("tiktok") != -1)
                var tiktok =  {"tiktok": { $exists: true, $ne: [] }}
            else
                var tiktok = {};
            if (social.indexOf("linkedin") != -1)
                var linkedin =  {"linkedin": { $exists: true, $ne: [] }}
            else
                var linkedin = {};
            if (social.indexOf("instagram_business") != -1)
                var instagram_business =  {"instagram_business": { $exists: true, $ne: [] }}
            else
                var instagram_business = {};
            if (social.indexOf("pinterest") != -1)
                var pinterest =  {"pinterest": { $exists: true, $ne: [] }}
            else
                var pinterest = {};

            var socialPlatform =  {
                "$match": {
                    "$and": [
                        facebook,
                        twitter,
                        instagram,
                        linkedin,
                        blog,
                        youtube,
                        snapchat,
                        tiktok,
                        instagram_business,
                        pinterest
                    ]
                }
            }
        }
        // Check reach count
        if(req.body.max_reach){
            req.body.min_reach = req.body.min_reach || "0";
            let maxReach = parseInt(req.body.min_reach);
            let minReach = parseInt(req.body.max_reach);
            var reachData = {
                $match: {
                    $or: [{
                            'facebook_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'twitter_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'snapchat_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'instagram_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'tiktok_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'youtube_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'pinterest_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'linkedin_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'blog_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'instagram_business_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                    ]
                }
            }
        }
        //Check genres
        if (req.body.genres && req.body.genres.length > 0) {
            let genres = req.body.genres;
            var genresData =  {
                "$match": {
                    "$or": [{
                            'facebook.genres': {
                                $in: genres
                            }
                        },
                        {
                            'twitter.genres': {
                                $in: genres
                            }
                        },
                        {
                            'snapchat.genres': {
                                $in: genres
                            }
                        },
                        {
                            'instagram.genres': {
                                $in: genres
                            }
                        },
                        {
                            'tiktok.genres': {
                                $in: genres
                            }
                        },
                        {
                            'youtube.genres': {
                                $in: genres
                            }
                        },
                        {
                            'pinterest.genres': {
                                $in: genres
                            }
                        },
                        {
                            'linkedin.genres': {
                                $in: genres
                            }
                        },
                        {
                            'blog.genres': {
                                $in: genres
                            }
                        },
                        {
                            'instagram_business.genres': {
                                $in: genres
                            }
                        }
                    ]
                }
            }
        }
        var sort = { "_id": -1 };

        let bodyData = {};
        // Get excel download data
        if(req.body.excel_download && req.body.excel_download == 1){
            bodyData.excelDownload = 1;
        }
        // Get outdated profile data
        if(req.body.outdated_profiles && req.body.outdated_profiles == 1){
            bodyData.outdateProfiles = 1;
            bodyData.status = status;
        }else{
            bodyData = {
                ...bodyData,
                search: search || '',
                status: status,
                categoryData:categoryData || '',
                gender: gender || '',
                socialPlatform:socialPlatform || '',
                reachData:reachData || '',
                genresData:genresData || ''
             }
         }

        let options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,
                lean: true
            }
            // Check from DB Collection
        userService.getPaginateDataWithAggregate(bodyData, options, sort, (err, userInfo, pages=null,total=null) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    if(req.body.excel_download && req.body.excel_download == 1){
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencers found!', userInfo);
                    }else{
                        for (let key of userInfo) {

                            if (key.email_verification !== undefined && key.email_verification && key.email_verification == "1") var emailVer = 2;
                            else var emailVer = 0;

                            if (key.mobile !== undefined && key.mobile && key.mobile != "") var mobileExist = 2;
                            else var mobileExist = 0;

                            if (key.is_verified !== undefined && key.is_verified && key.is_verified == "1") var mobileVer = 2;
                            else var mobileVer = 0;

                            if (key.country !== undefined && key.country && key.state && key.address) var locationExist = 4;
                            else var locationExist = 0

                            if (key.gender !== undefined && key.gender) var genderExist = 1;
                            else var genderExist = 0;

                            if (key.dob !== undefined && key.dob) var dobExist = 1;
                            else var dobExist = 0;

                            if (key.bank_details && key.bank_details !== undefined && key.bank_details.length > 0) var bankDetailExist = 3;
                            else var bankDetailExist = 0;

                            if ((key.facebook && key.facebook !== undefined && key.facebook.length > 0) || (key.twitter && key.twitter !== undefined && key.twitter.length > 0) || (key.youtube && key.youtube !== undefined && key.youtube.length > 0) || (key.snapchat && key.snapchat !== undefined && key.snapchat.length > 0) || (key.instagram && key.instagram !== undefined && key.instagram.length > 0) || (key.tiktok && key.tiktok !== undefined && key.tiktok.length > 0) || (key.linkedin && key.linkedin !== undefined && key.linkedin.length > 0) || (key.blog && key.blog !== undefined && key.blog.length > 0) || (key.pinterest && key.pinterest !== undefined && key.pinterest.length > 0)) var socailExist = 5;
                            else var socailExist = 0;

                            if (emailVer > 0 && mobileExist > 0 && mobileVer > 0 && locationExist > 0 && genderExist > 0 && dobExist > 0 && bankDetailExist > 0 && socailExist > 0) var allDetailExit = 5;
                            else var allDetailExit = 0;

                            var countPercentage = ((emailVer + mobileExist + mobileVer + locationExist + genderExist + dobExist + bankDetailExist + socailExist + allDetailExit) / 25) * 100;
                            key.percentage = Math.trunc(countPercentage);

                        }
                        let data = {
                            docs: userInfo,
                            page: req.body.pageNumber || 1,
                            limit: req.body.limit || 10,
                            pages: pages,
                            total: total
                        }
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencers found!', data);
                    }

                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Influencers not found!');
                }
            }
        });

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [Get Max Influencer reach  count]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getMaxReachCountData = (req, res) => {
    let check = helper.checkRequest([], req.body);
    if (check == true) {
        let sessionData = req.body.decoded;
        let myquery = {
            'email': sessionData.email.toLowerCase()
        };
        adminService.findData(myquery, (error, resp) => {
            if (error) responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            else{
                if(resp){
                    let bodyData = {
                        status:'0'
                    }
                    userService.getMaxReachCountInfluencer(bodyData,(errIn, inactivecount) => {
                        if(errIn)  responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errIn);
                        else{
                            let [{_id, ...rest}] = inactivecount.length ? inactivecount : [{_id:''}];
                            let inactive_max_reach = 0;
                            if(_id)
                                inactive_max_reach = Math.max(...Object.values(rest));
                            let bodyData = {
                                status:'1'
                            }
                            userService.getMaxReachCountInfluencer(bodyData,(errAc, activecount) => {
                                if(errAc)  responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errAc);
                                else{
                                    let [{_id, ...rest}] = activecount.length ? activecount : [{_id:''}];
                                    let active_max_reach = 0;
                                    if(_id)
                                        active_max_reach = Math.max(...Object.values(rest));
                                    let bodyData = {
                                        status:'2'
                                    }
                                    userService.getMaxReachCountInfluencer(bodyData,(errBa, barredcount) => {
                                        if(errBa)  responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errBa);
                                        else{
                                            let [{_id, ...rest}] = barredcount.length ? barredcount : [{_id:''}];
                                            let barred_max_reach = 0;
                                            if(_id)
                                                barred_max_reach = Math.max(...Object.values(rest));
                                            let data = {
                                                active_max_reach : active_max_reach || 0,
                                                inactive_max_reach : inactive_max_reach || 0,
                                                barred_max_reach : barred_max_reach || 0
                                            }
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Max reach count from influecners', data);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }else{
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Request not found.');
                }
            }
        });
    }else{
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Edit social detail for influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const editSocialDetailInfluencer = (req, res) => {
    let check = helper.checkRequest(["influencer_id", "platform"], req.body);
    if (check == true) {
        // Check OTP is valid or not
        let myquery = {
            _id: new ObjectId(req.body.influencer_id),
        };
        userService.findData(myquery, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else if (!resp) {
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not found. ');
            } else {
                let platform = req.body.platform;
                let platform_name = `${platform}.name`;
                let input_array = {[platform]: req.body[platform]};
                let input_array_remove = { [platform] : { 'name': req.body.name }};
                let input_platform_user =
                  {
                    _id: new ObjectId(req.body.influencer_id),
                    [platform_name]: req.body.name
                  };
                if (req.body.name == "") {
                    let newvalues = {
                        $push: input_array
                    };
                    userService.arrayUpdate(myquery, newvalues, (error3, resp) => {
                        if (error3) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error3);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Social Detail added successfully.');
                        }
                    });
                } else {
                    userService.findData(input_platform_user, (error_, userInfo) => {
                        if (error_) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                        } else {
                            if (userInfo) {
                                let newvalues = {
                                    $pull: input_array_remove
                                };
                                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                                    if (error2) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                                    } else {
                                        let newvalues = {
                                            $push: input_array
                                        };
                                        userService.arrayUpdate(myquery, newvalues, (error2, resp2) => {
                                            if (error2) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                                            } else {
                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Social Detail updated successfully.');
                                            }
                                        });
                                    }
                                });
                            } else {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, "Social Account not found.");
                            }
                        }
                    });
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [delete social detail for influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const deleteSocialCard = (req, res) => {
    let check = helper.checkRequest(["influencer_id", "platform", "name"], req.body);
    if (check == true) {
        // Check OTP is valid or not
        let myquery = {
            _id: new ObjectId(req.body.influencer_id)
        };
        userService.findData(myquery, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else if (!resp) {
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not found. ');
            } else {
                let input_array = '';
                let input_platform_user = '';
                if (req.body.platform == 'twitter') {
                    input_array = {
                        'twitter': {
                            'name': req.body.name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'twitter.name': req.body.name
                    };
                }
                if (req.body.platform == 'facebook') {
                    input_array = {
                        'facebook': {
                            'name': req.body.name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'facebook.name': req.body.name
                    };
                }
                if (req.body.platform == 'instagram') {
                    input_array = {
                        'instagram': {
                            'name': req.body.name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'instagram.name': req.body.name
                    }

                }
                if (req.body.platform == 'youtube') {
                    input_array = {
                        'youtube': {
                            'name': req.body.name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'youtube.name': req.body.name
                    };

                }
                if (req.body.platform == 'blog') {

                    input_array = {
                        'blog': {
                            'name': req.body.screen_name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'blog.name': req.body.screen_name
                    };

                }
                if (req.body.platform == 'snapchat') {
                    input_array = {
                        'snapchat': {
                            'name': req.body.screen_name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'snapchat.name': req.body.screen_name
                    };

                }
                if (req.body.platform == 'linkedin') {
                    input_array = {
                        'linkedin': {
                            'name': req.body.name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'linkedin.name': req.body.name
                    };
                }
                if (req.body.platform == 'tiktok') {
                    input_array = {
                        'tiktok': {
                            'name': req.body.name
                        }
                    };
                    input_platform_user = {
                        _id: new ObjectId(req.body.influencer_id),
                        'tiktok.name': req.body.name
                    };
                }
                userService.findData(input_platform_user, (error_, userInfo) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                    } else {
                        if (userInfo) {
                            let myquery = { _id: new ObjectId(req.body.influencer_id) };
                            let newvalues = {
                                $pull: input_array
                            };
                            userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                                if (error2) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Social Details deleted successfully.');
                                }
                            });
                        } else {
                            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, "Social account not found");

                        }
                    }
                });
            }

        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [genre List for influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const genreList = (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        genreService.findData({status:true}, (error_, resp_) => {
            if (error_) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
            } else {
                if (resp_) {
                    let data = {
                        genre_list: resp_
                    }
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Genre list fetch successfully.', data);
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Genre list fetch successfully.', []);
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [Check Handle Url for social influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */


const checkHandleUrl = (req, res) => {
    let check = helper.checkRequest(["user_id", "platform"], req.body);
    if (check == true) {
        let myquery = { _id: new ObjectId(req.body.user_id)};
        adminService.findData(myquery, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else if (!resp) {
                if (this.adminData.isd) {
                    this.mobile_code = this.adminData.isd;
                } else {
                    this.mobile_code = '';
                }
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
            } else {
                let input_array = '';
                let input_platform_user = '';

                if (req.body.name) {
                    if (req.body.platform == 'twitter') {
                        input_platform_users = {
                            'registered': '1',
                            'twitter.name': req.body.name
                        };
                    }
                    if (req.body.platform == 'facebook') {
                        input_platform_users = {
                            'registered': '1',
                            'facebook.name': req.body.name
                        };
                    }
                    if (req.body.platform == 'instagram') {
                        input_platform_users = {
                            'registered': '1',
                            'instagram.name': req.body.name
                        }

                    }
                    if (req.body.platform == 'youtube') {
                        input_platform_users = {
                            'registered': '1',
                            'youtube.name': req.body.name
                        };
                    }
                    if (req.body.platform == 'blog') {
                        input_platform_users = {
                            'registered': '1',
                            'blog.name': req.body.name
                        };

                    }
                    if (req.body.platform == 'snapchat') {
                        input_platform_users = {
                            'registered': '1',
                            'snapchat.name': req.body.name
                        };

                    }
                    if (req.body.platform == 'linkedin') {
                        input_platform_users = {
                            'registered': '1',
                            'linkedin.name': req.body.name
                        };
                    }

                    if (req.body.platform == 'tiktok') {
                        input_platform_users = {
                            'registered': '1',
                            'tiktok.name': req.body.name
                        };
                    }
                }
                if (req.body.link) {
                    if (req.body.platform == 'twitter') {
                        input_platform_user = {
                            'registered': '1',
                            'twitter.link': req.body.link
                        };
                    }
                    if (req.body.platform == 'facebook') {
                        input_platform_user = {
                            'registered': '1',
                            'facebook.link': req.body.link
                        };
                    }
                    if (req.body.platform == 'instagram') {
                        input_platform_user = {
                            'registered': '1',
                            'instagram.link': req.body.link
                        }
                    }
                    if (req.body.platform == 'youtube') {
                        input_platform_user = {
                            'registered': '1',
                            'youtube.link': req.body.link
                        };
                    }
                    if (req.body.platform == 'blog') {
                        input_platform_user = {
                            'registered': '1',
                            'blog.link': req.body.link
                        };
                    }
                    if (req.body.platform == 'snapchat') {
                        input_platform_user = {
                            'registered': '1',
                            'snapchat.link': req.body.link
                        };
                    }
                    if (req.body.platform == 'linkedin') {
                        input_platform_user = {
                            'registered': '1',
                            'linkedin.link': req.body.link
                        };
                    }

                    if (req.body.platform == 'tiktok') {
                        input_platform_user = {
                            'registered': '1',
                            'tiktok.link': req.body.link
                        };
                    }
                }
                if (req.body.name || req.body.link) {
                    if (req.body.name) {
                        userService.findData(input_platform_users, (error_, userInfo) => {
                            if (error_) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                            } else {
                                if (userInfo) {
                                    responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This account is already connected by a different influencer. Please try a different account');
                                } else {
                                    if (req.body.link) {
                                        userService.findData(input_platform_user, (error_, userInfo) => {

                                            if (error_) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                            } else {
                                                if (userInfo) {
                                                    responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This account is already connected by a different influencer. Please try a different account');
                                                } else {
                                                    responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, "Pass. Social profile is unique.");
                                                }
                                            }
                                        });
                                    } else {
                                        responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, "Pass. Social profile is unique.");
                                    }

                                }
                            }
                        });
                    } else {
                        userService.findData(input_platform_user, (error_, userInfo) => {
                            if (error_) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                            } else {
                                if (userInfo) {
                                    responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This account is already connected by a different influencer. Please try a different account');
                                } else {
                                    if (req.body.name) {
                                        userService.findData(input_platform_users, (error_, userInfo) => {
                                            if (error_) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                            } else {
                                                if (userInfo) {
                                                    responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This account is already connected by a different influencer. Please try a different account');
                                                } else {
                                                    responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, "Pass. Social profile is unique.");
                                                }
                                            }
                                        });
                                    } else {
                                        responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, "Pass. Social profile is unique.");
                                    }

                                }
                            }
                        });
                    }

                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `Provide name or link`);
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};



/**
 * [Get Influencer List getInfluencerListDataForExcel]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getInfluencerListDataForExcel = async(req, res) => {
    let check = helper.checkRequest([], req.body);
    if (check == true) {
        let search = req.body.keyword || "";
        let sortData = req.body.sort;
        //check status
        if (req.body.status == '1') {
            var status = ['1'];
        } else if (req.body.status == '0') {
            var status = ['', '0'];
        } else if (req.body.status == '2') {
            var status = ['2'];
        }
        // check gender
        if (req.body.gender && req.body.gender.length) {
            var gender =  {
                "$match": {
                    gender: {
                        "$in": req.body.gender
                    }
                }
            }
        }
        // check category
        if (req.body.category && req.body.category.length) {
            let category = req.body.category;
            var categoryData =  {
                    "$match": {
                        "$or": [
                            {
                                'facebook.category': {
                                    $in: category
                                }
                            },
                            {
                                'twitter.category': {
                                    $in: category
                                }
                            },
                            {
                                'linkedin.category': {
                                    $in: category
                                }
                            },
                            {
                                'instagram.category': {
                                    $in: category
                                }
                            },
                            {
                                'blog.category': {
                                    $in: category
                                }
                            },
                            {
                                'youtube.category': {
                                    $in: category
                                }
                            },
                            {
                                'tiktok.category': {
                                    $in: category
                                }
                            },
                            {
                                'snapchat.category': {
                                    $in: category
                                }
                            },
                            {
                                'instagram_business.category': {
                                    $in: category
                                }
                            },
                            {
                                'pinterest.category': {
                                    $in: category
                                }
                            },
                        ]
                    }
                }
        }
        // check social platform
        if (req.body.social && req.body.social.length) {
            var social = req.body.social;
            if (social.indexOf('facebook') != -1)
               var facebook =  {"facebook": { $exists: true, $ne: [] }}
            else
                var facebook = {};
            if (social.indexOf('twitter') != -1)
                var twitter =  {"twitter": { $exists: true, $ne: [] }}
            else
                var twitter = {};
            if (social.indexOf('instagram') != -1)
                var instagram =  {"instagram": { $exists: true, $ne: [] }}
            else
                var instagram = {};
            if (social.indexOf('blog') != -1)
                var blog =  {"blog": { $exists: true, $ne: [] }}
            else
                var blog ={};
            if (social.indexOf('snapchat') != -1)
                var snapchat =  {"snapchat": { $exists: true, $ne: [] }}
            else
                var snapchat = {};
            if (social.indexOf('youtube') != -1)
                var youtube =  {"youtube": { $exists: true, $ne: [] }}
            else
                var youtube = {};
            if (social.indexOf("tiktok") != -1)
                var tiktok =  {"tiktok": { $exists: true, $ne: [] }}
            else
                var tiktok = {};
            if (social.indexOf("linkedin") != -1)
                var linkedin =  {"linkedin": { $exists: true, $ne: [] }}
            else
                var linkedin = {};
            if (social.indexOf("instagram_business") != -1)
                var instagram_business =  {"instagram_business": { $exists: true, $ne: [] }}
            else
                var instagram_business = {};
            if (social.indexOf("pinterest") != -1)
                var pinterest =  {"facebook": { $exists: true, $ne: [] }}
            else
                var pinterest = {};

               var socialPlatform =  {
                "$match": {
                    "$and": [
                        facebook,
                        twitter,
                        instagram,
                        linkedin,
                        blog,
                        youtube,
                        snapchat,
                        tiktok,
                        instagram_business,
                        pinterest
                    ]
                }
            }
        }
        // Check reach count
        if(req.body.max_reach){
            req.body.min_reach = req.body.min_reach || "0";
            let maxReach = parseInt(req.body.min_reach);
            let minReach = parseInt(req.body.max_reach);
            var reachData = {
                $match: {
                    $or: [{
                            'facebook_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'twitter_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'snapchat_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'instagram_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'tiktok_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'youtube_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'pinterest_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'linkedin_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'blog_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                        {
                            'instagram_business_count': {
                                $gte: maxReach,
                                $lte: minReach
                            }
                        },
                    ]
                }
            }
        }
        //Check genres
        if (req.body.genres.length > 0) {
            let genres = req.body.genres;
            var genresData =  {
                "$match": {
                    "$or": [{
                            'facebook.genres': {
                                $in: genres
                            }
                        },
                        {
                            'twitter.genres': {
                                $in: genres
                            }
                        },
                        {
                            'snapchat.genres': {
                                $in: genres
                            }
                        },
                        {
                            'instagram.genres': {
                                $in: genres
                            }
                        },
                        {
                            'tiktok.genres': {
                                $in: genres
                            }
                        },
                        {
                            'youtube.genres': {
                                $in: genres
                            }
                        },
                        {
                            'pinterest.genres': {
                                $in: genres
                            }
                        },
                        {
                            'linkedin.genres': {
                                $in: genres
                            }
                        },
                        {
                            'blog.genres': {
                                $in: genres
                            }
                        },
                        {
                            'instagram_business.genres': {
                                $in: genres
                            }
                        }
                    ]
                }
            }
        }

        if (sortData == '1') {
            var sort = { "first_name": -1 };
        } else if (sortData == '0') {
            var sort = { "first_name": 1 };
        } else {
            var sort = { "_id": -1 };
        }

        let bodyData = {};
        if(req.body.outdated_profiles && req.body.outdated_profiles == 1){
            bodyData.outdateProfiles = 1;
        }else{
            bodyData = {
                search: search,
                status: status,
                categoryData:categoryData,
                gender: gender,
                socialPlatform:socialPlatform,
                reachData:reachData,
                genresData:genresData,
                excelDownload: 1

            }
        }

            // Check from DB Collection
        userService.getPaginateDataWithAggregate(bodyData,{}, sort, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencers found!', userInfo);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Influencers not found!');
                }
            }
        });

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

// Geting all social platforms
const getAllPlatforms = (req, res) => {
    var influencerId = req.body.influencer_id;
    var platformData = {};
    let check = helper.checkRequest(['influencer_id'], req.body);
    if (check == true) {
        userModel.findOne({'influencer_id' : influencerId}).then((data) => {
        for(var key in data) {
            if(platforms.indexOf(key) !== -1){
                platformData[key] = data[key];
            }
        }
        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platforms Found', platformData)
        //res.send(platformData);
        }).catch((err) => {
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Platforms data not found!')
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}


/*Export apis*/
module.exports = {
    createInfluencer,
    checkEmailExist,
    editDetailInfluencer,
    getInfluencerDetail,
    statusUpdate,
    getInfluencerList,
    editSocialDetailInfluencer,
    deleteSocialCard,
    genreList,
    checkHandleUrl,
    getInfluencerListDataForExcel,
    getAllPlatforms,
    getMaxReachCountData
}