var randomstring = require("randomstring");
const advertiserService = require('../services/advertiserService.js');// user services
const organisationService = require('../services/organisationService.js');// organisation services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const ObjectId = require('mongodb').ObjectID;// Obj ID
var AWS = require('aws-sdk');//S3 Upload
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
const advertiserModel = require('./../models/advertiserModel');
const bcrypt = require('bcryptjs');

/**
 * [Advertiser List ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const advertiserList = (req, res) => {
    let search_keyword = req.body.keyword;
    let advertiser_filter = req.body.filter;
    let advertiser_sort = req.body.sort;
    if (advertiser_filter == "0") {var filter = ['0'];}
    else if (advertiser_filter == "1") { var filter = ['1'];}
    else {var filter = ["", "0", "1"];}
    let bodyData = {
        search: search_keyword,
        status: filter
    }
    if (advertiser_sort == "0") {var sort = { "name": -1 };}
    else if (advertiser_sort == "1") {var sort = { "name": 1 };}
    else {var sort = { "_id": -1 };}
    let options = {
        page: req.body.pageNumber || 1,
        limit: req.body.limit || 10,
        lean : true
    }
    advertiserService.getPaginateDataWithAggregate(bodyData, options, sort, (error, advertiserInfo, pages, total) => {
        if (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
        } else {
            for (let key of advertiserInfo) {
                if(key.status === ""){
                    key.status = "0";
                }
            }
            if (advertiserInfo) {
                let data = {
                    docs: advertiserInfo,
                    page: req.body.pageNumber || 1,
                    limit: req.body.limit || 10,
                    pages: pages,
                    total: total
                }
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser List!', data);
            } else {
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Data not found!');
            }
        }
    });
}

/**
 * [Get data by id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const advertiserData = (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        // Check from DB Collection
        advertiserService.getAdvertiserById({_id: new ObjectId(req.body.user_id)
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    if (userInfo[0].organisation_details[0].addresses.length) {
                        const address = userInfo[0].organisation_details[0].addresses;
                        for (let data of address) {
                            if(data.advertisers && data.advertisers.length > 0){
                                for (let data2 of data.advertisers) {
                                    if (data2 == req.body.user_id) {
                                        var address_id = data._id;
                                    }
                                }
                            }
                        }
                    }
                    var data = userInfo[0];
                    data.address_id = address_id;
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User found!', data);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Add Advertiser data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const createAdvertiser = (req, res) => {
    let check = helper.checkRequest(["first_name", "organisation_id", "email"], req.body);
    if (check == true) {
        let newId = helper.generateRandomString();
        if (req.body.profilePic !== undefined) {
            buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            let number = Math.random() * (999999 - 10000) + 10000
            let resData = {
                Key: number.toString(),
                Body: buf,
                ContentEncoding: 'base64',
                ContentType: 'image/jpeg',
                ACL: 'public-read'
            };
            s3Bucket.upload(resData, function(err, data) {
                if (err) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                } else {
                    let obj = {
                        "advertiser_id": newId,
                        "name": req.body.first_name,
                        "last_name": req.body.last_name,
                        "email": req.body.email.toLowerCase(),
                        "isd": req.body.isd,
                        "phone": req.body.mobile,
                        "isd": req.body.isd,
                        "organisation_id": req.body.organisation_id,
                        "manager": req.body.manager,
                        "address": req.body.address,
                        "country": req.body.country,
                        "state": req.body.state,
                        "city": req.body.city,
                        "associated_brands": req.body.associated_brands,
                        "profile_image": data.Location,
                        "designation": req.body.designation,
                        "notes": req.body.notes,
                        "password": randomstring.generate(6),
                        "status": '1'
                    };
                    advertiserService.findData({ 'email': req.body.email.toLowerCase() }, (error_, result1) => {
                        if (error_) {
                           return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                        } else if (result1) {
                            return responseHandle.sendResponsewithError(res, 402, "Email already exist.");
                        } else {
                            advertiserService.findData({ 'phone': req.body.mobile, 'status': '1' }, (error1_, result2) => {
                                if (error1_) {
                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1_);
                                } else if (result2) {
                                    return responseHandle.sendResponsewithError(res, 403, 'Mobile already exists');
                                } else {
                                    advertiserService.createData(obj, (error, result_) => {
                                        if (error) {
                                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                        } else {
                                            // Check address ID
                                            if (req.body.address_id) {
                                                // Save old address
                                                let myquery = {
                                                    _id: new ObjectId(req.body.organisation_id),
                                                    "addresses._id": new ObjectId(req.body.address_id)
                                                };
                                                let newvalues = {
                                                    $push: {
                                                        "addresses.$.advertisers": result_._id
                                                    }
                                                }
                                                organisationService.updateOne(myquery, newvalues, { new: true }, (error_org, addressupdate) => {
                                                    if (error_org) {
                                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org);
                                                    } else {
                                                        if (req.body.associated_brands.length) {
                                                            // Insert associated brands with advetiser id
                                                            let myquery1 = {
                                                                _id: new ObjectId(req.body.organisation_id)
                                                            };
                                                            let newvalues1 = {
                                                                $push: {
                                                                    associated_brands: {
                                                                        "advertiser_id": result_._id,
                                                                        "brands": req.body.associated_brands
                                                                    }
                                                                }
                                                            }
                                                            organisationService.updateOne(myquery1, newvalues1, { new: true }, (error_ass_brand, res_ass_brand) => {
                                                                if (error_ass_brand) {
                                                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand);
                                                                } else {
                                                                    var message = {
                                                                        "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                                        "subject": "Credentials",
                                                                        "from_email": "team@eleve.co.in",
                                                                        "from_name": "Eleve",
                                                                        "to": [{
                                                                            "email": req.body.email.toLowerCase(),
                                                                            "type": "to"
                                                                        }]
                                                                    };
                                                                    helper.sendEmail(message);
                                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                                }
                                                            });
                                                            // ends here
                                                        } else {
                                                            var message = {
                                                                "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                                "subject": "Credentials",
                                                                "from_email": "team@eleve.co.in",
                                                                "from_name": "Eleve",
                                                                "to": [{
                                                                    "email": req.body.email.toLowerCase(),
                                                                    "type": "to"
                                                                }]
                                                            };
                                                            helper.sendEmail(message);
                                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                        }
                                                    }
                                                });
                                            } else {
                                                // save new address
                                                let myquery = { _id: new ObjectId(req.body.organisation_id) };
                                                let newvalues = {
                                                    $push: {
                                                        addresses: {
                                                            advertisers: result_._id,
                                                            address: req.body.address,
                                                            country: req.body.country,
                                                            state: req.body.state,
                                                            city: req.body.city
                                                        }
                                                    }
                                                }
                                                organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_2, addressupdate1) => {
                                                    if (error_org_2) {
                                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_2);
                                                    } else {
                                                        if (req.body.associated_brands.length) {
                                                            // Insert associated brands with advetiser id
                                                            let myquery2 = {
                                                                _id: new ObjectId(req.body.organisation_id)
                                                            };
                                                            let newvalues2 = {
                                                                $push: {
                                                                    associated_brands: {
                                                                        "advertiser_id": result_._id,
                                                                        "brands": req.body.associated_brands
                                                                    }
                                                                }
                                                            }
                                                            organisationService.updateOne(myquery2, newvalues2, { new: true }, (error_ass_brand2, res_ass_brand2) => {
                                                                if (error_ass_brand2) {
                                                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand2);
                                                                } else {
                                                                    var message = {
                                                                        "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                                        "subject": "Credentials",
                                                                        "from_email": "team@eleve.co.in",
                                                                        "from_name": "Eleve",
                                                                        "to": [{
                                                                            "email": req.body.email.toLowerCase(),
                                                                            "type": "to"
                                                                        }]
                                                                    };
                                                                    helper.sendEmail(message);
                                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                                }

                                                            });
                                                        } else {
                                                            var message = {
                                                                "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                                "subject": "Credentials",
                                                                "from_email": "team@eleve.co.in",
                                                                "from_name": "Eleve",
                                                                "to": [{
                                                                    "email": req.body.email.toLowerCase(),
                                                                    "type": "to"
                                                                }]
                                                            };
                                                            helper.sendEmail(message);
                                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    })
                                }
                            });
                        }
                    })

                }
            })
        } else {
            let obj = {
                "advertiser_id": newId,
                "name": req.body.first_name,
                "last_name": req.body.last_name,
                "email": req.body.email.toLowerCase(),
                "isd": req.body.isd,
                "phone": req.body.mobile,
                "isd": req.body.isd,
                "organisation_id": req.body.organisation_id,
                "manager": req.body.manager,
                "address": req.body.address,
                "country": req.body.country,
                "state": req.body.state,
                "city": req.body.city,
                "designation": req.body.designation,
                "associated_brands": req.body.associated_brands,
                "notes": req.body.notes,
                "password": randomstring.generate(6),
                "status": '1'
            };
            advertiserService.findData({ 'email': req.body.email.toLowerCase() }, (error_, result1) => {
                if (error_) {
                   return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                } else if (result1) {
                    return responseHandle.sendResponsewithError(res, 402, "Email already exist.");
                } else {
                    advertiserService.findData({ 'phone': req.body.mobile, 'status': '1' }, (error1_, result2) => {
                        if (error1_) {
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1_);
                        } else if (result2) {
                            return responseHandle.sendResponsewithError(res, 403, 'Mobile already exists');
                        } else {
                            advertiserService.createData(obj, (error, result_) => {
                                if (error) {
                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                } else {
                                    // Check address ID
                                    if (req.body.address_id) {
                                        // Save old address
                                        let myquery = {
                                            _id: new ObjectId(req.body.organisation_id),
                                            "addresses._id": new ObjectId(req.body.address_id)
                                        };
                                        let newvalues = {
                                            $push: {
                                                "addresses.$.advertisers": result_._id
                                            }
                                        }
                                        organisationService.updateOne(myquery, newvalues, { new: true }, (error_org, addressupdate) => {
                                            if (error_org) {
                                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org);
                                            } else {
                                                if (req.body.associated_brands.length) {
                                                    // Insert associated brands with advetiser id
                                                    let myquery3 = {
                                                        _id: new ObjectId(req.body.organisation_id)
                                                    };
                                                    let newvalues3 = {
                                                        $push: {
                                                            associated_brands: {
                                                                "advertiser_id": result_._id,
                                                                "brands": req.body.associated_brands
                                                            }
                                                        }
                                                    }
                                                    organisationService.updateOne(myquery3, newvalues3, { new: true }, (error_ass_brand3, res_ass_brand3) => {
                                                        if (error_ass_brand3) {
                                                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand3);
                                                        } else {
                                                            var message = {
                                                                "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                                "subject": "Credentials",
                                                                "from_email": "team@eleve.co.in",
                                                                "from_name": "Eleve",
                                                                "to": [{
                                                                    "email": req.body.email.toLowerCase(),
                                                                    "type": "to"
                                                                }]
                                                            };
                                                            helper.sendEmail(message);
                                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                        }

                                                    });
                                                } else {
                                                    var message = {
                                                        "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                        "subject": "Credentials",
                                                        "from_email": "team@eleve.co.in",
                                                        "from_name": "Eleve",
                                                        "to": [{
                                                            "email": req.body.email.toLowerCase(),
                                                            "type": "to"
                                                        }]
                                                    };
                                                    helper.sendEmail(message);
                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                }
                                            }
                                        });
                                    } else {
                                        // save new address
                                        let myquery = { _id: new ObjectId(req.body.organisation_id) };
                                        let newvalues = {
                                            $push: {
                                                addresses: {
                                                    advertisers: result_._id,
                                                    address: req.body.address,
                                                    country: req.body.country,
                                                    state: req.body.state,
                                                    city: req.body.city,
                                                }
                                            }
                                        }
                                        organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_2, addressupdate1) => {
                                            if (error_org_2) {
                                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_2);
                                            } else {
                                                if (req.body.associated_brands.length) {
                                                    // Insert associated brands with advetiser id
                                                    let myquery4 = {
                                                        _id: new ObjectId(req.body.organisation_id)
                                                    };
                                                    let newvalues4 = {
                                                        $push: {
                                                            associated_brands: {
                                                                "advertiser_id": result_._id,
                                                                "brands": req.body.associated_brands
                                                            }
                                                        }
                                                    }
                                                    organisationService.updateOne(myquery4, newvalues4, { new: true }, (error_ass_brand4, res_ass_brand4) => {
                                                        if (error_ass_brand4) {
                                                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand4);
                                                        } else {
                                                            var message = {
                                                                "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                                "subject": "Credentials",
                                                                "from_email": "team@eleve.co.in",
                                                                "from_name": "Eleve",
                                                                "to": [{
                                                                    "email": req.body.email.toLowerCase(),
                                                                    "type": "to"
                                                                }]
                                                            };
                                                            helper.sendEmail(message);
                                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                        }

                                                    });
                                                } else {
                                                    //console.log(res_ass_brand4);
                                                    var message = {
                                                        "html": "<p>Hi ,Login credentials <br> Email:- " + req.body.email.toLowerCase() + "<br/> Password:- " + obj.password + "</p>",
                                                        "subject": "Credentials",
                                                        "from_email": "team@eleve.co.in",
                                                        "from_name": "Eleve",
                                                        "to": [{
                                                            "email": req.body.email.toLowerCase(),
                                                            "type": "to"
                                                        }]
                                                    };
                                                    helper.sendEmail(message);
                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Created Successfully', result_);
                                                }
                                            }
                                        });
                                    }
                                }
                            })
                        }
                    });
                }
            })
        }
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const advertiserImageUpload = (req, res) => {
    let check = helper.checkRequest(["advertiser_id", "profilePic"], req.body);
    if (check == true) {
        buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        let number = Math.random() * (999999 - 10000) + 10000
        let resData = {
            Key: number.toString(),
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg',
            ACL: 'public-read'
        };
        s3Bucket.upload(resData, function(err, data) {
            if (err) {
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                let myquery = {
                    _id: new ObjectId(req.body.advertiser_id)
                };
                let imageValue = {
                    $set: {
                        profile_image: data.Location,
                    }
                }
                advertiserService.updateOne(myquery, imageValue, {
                    new: true
                }, (error_, userupdate) => {
                    if (error_) {
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User images uploaded successfully!', userupdate);
                    }
                });
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Edit Advertiser]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const editAdvertiser = (req, res) => {
    let check = helper.checkRequest(["first_name", "organisation_id", "email"], req.body);
    if (check == true) {
        if (req.body.profilePic !== undefined) {
            buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            let number = Math.random() * (999999 - 10000) + 10000
            let resData = {
                Key: number.toString(),
                Body: buf,
                ContentEncoding: 'base64',
                ContentType: 'image/jpeg',
                ACL: 'public-read'
            };
            s3Bucket.upload(resData, function(err, data) {
                if (err) {
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    let obj = {
                        "name": req.body.first_name,
                        "last_name": req.body.last_name,
                        "email": req.body.email,
                        "isd": req.body.isd,
                        "phone": req.body.mobile,
                        "isd": req.body.isd,
                        "status": req.body.status,
                        "organisation_id": req.body.organisation_id,
                        "manager": req.body.manager,
                        "address": req.body.address,
                        "country": req.body.country,
                        "state": req.body.state,
                        "city": req.body.city,
                        "designation": req.body.designation,
                        "notes": req.body.notes,
                        "associated_brands": req.body.associated_brands,
                        "profile_image": data.Location,
                        "updated_at": Date.now()
                    };
                    let myquery = {
                        _id: new ObjectId(req.body.user_id)
                    };
                    let newvalues = {
                        $set: obj
                    };
                    advertiserService.arrayUpdate(myquery, newvalues, { new: true }, (error, result_) => {
                        if (error) {
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            // Check address ID
                            if (req.body.address_id) {
                                // Save old address
                                let myquery = {
                                    _id: new ObjectId(req.body.organisation_id),
                                    "addresses.advertisers": new ObjectId(req.body.user_id)
                                };
                                let newvalues = {
                                    $pull: {
                                        "addresses.$.advertisers": new ObjectId(req.body.user_id)
                                    }
                                }
                                organisationService.updateOne(myquery, newvalues, { safe: true }, (error_org_edit, addressupdate_edit) => {
                                    if (error_org_edit) {
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_edit);
                                    } else {
                                        // Save old address
                                        let myquery = {
                                            _id: new ObjectId(req.body.organisation_id),
                                            "addresses._id": new ObjectId(req.body.address_id)
                                        };
                                        let newvalues = {
                                            $push: {
                                                "addresses.$.advertisers": new ObjectId(req.body.user_id)
                                            }
                                        }
                                        organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_edit2, addressupdate_edit2) => {
                                            if (error_org_edit2) {
                                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_edit2);
                                            } else {
                                                let myquery = {
                                                    _id: new ObjectId(req.body.organisation_id),
                                                    "addresses._id": new ObjectId(req.body.address_id)
                                                };
                                                organisationService.findDataBy(myquery, (_error1_, _result2) => {
                                                    if (_error1_) {
                                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, _error1_);
                                                    } else {
                                                        let obj = {
                                                            "address": _result2.addresses[0].address,
                                                            "country": _result2.addresses[0].country,
                                                            "state": _result2.addresses[0].state,
                                                            "city": _result2.addresses[0].city,
                                                            "updated_at": Date.now()
                                                        };
                                                        let myquery = {
                                                            _id: new ObjectId(req.body.user_id)
                                                        };
                                                        let newvalues = {
                                                            $set: obj
                                                        };
                                                        advertiserService.arrayUpdate(myquery, newvalues, { new: true }, (_error, result_) => {
                                                            if (_error) {
                                                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, _error);
                                                            } else {
                                                                //
                                                                if (req.body.associated_brands) {
                                                                    // Insert associated brands with advetiser id
                                                                    let myquery1 = {
                                                                        _id: new ObjectId(req.body.organisation_id),
                                                                        "associated_brands.advertiser_id": req.body.user_id
                                                                    };
                                                                    let newvalues1 = {
                                                                        $set: {
                                                                            "associated_brands.$.brands": req.body.associated_brands
                                                                        }
                                                                    }
                                                                    organisationService.updateOne(myquery1, newvalues1, { new: true }, (error_ass_brand, res_ass_brand) => {
                                                                        if (error_ass_brand) {
                                                                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand);
                                                                        } else {
                                                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                                                        }

                                                                    });
                                                                    // ends here
                                                                } else {
                                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                            } else {
                                // Save new address
                                let myquery = {
                                    _id: new ObjectId(req.body.organisation_id),
                                    "addresses._id": new ObjectId(req.body.old_address_id)
                                };
                                let newvalues = {
                                    $pull: {
                                        "addresses.$.advertisers": new ObjectId(req.body.user_id)

                                    }
                                }
                                organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_edit_old, addressupdate_edit2_old) => {
                                    if (error_org_edit_old) {
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_edit_old);
                                    } else {
                                        // save new address
                                        let myquery = { _id: new ObjectId(req.body.organisation_id) };
                                        let newvalues = {
                                            $push: {
                                                addresses: {
                                                    advertisers: new ObjectId(req.body.user_id),
                                                    address: req.body.address,
                                                    country: req.body.country,
                                                    state: req.body.state,
                                                    city: req.body.city,
                                                }
                                            }
                                        }
                                        organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_2_old, addressupdate1_old) => {
                                            if (error_org_2_old) {
                                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_2_old);
                                            } else {
                                                if (req.body.associated_brands) {
                                                    // Insert associated brands with advetiser id
                                                    let myquery1 = {
                                                        _id: new ObjectId(req.body.organisation_id),
                                                        "associated_brands.advertiser_id": req.body.user_id
                                                    };
                                                    let newvalues1 = {
                                                        $set: {
                                                            "associated_brands.$.brands": req.body.associated_brands
                                                        }
                                                    }
                                                    organisationService.updateOne(myquery1, newvalues1, { new: true }, (error_ass_brand, res_ass_brand) => {
                                                        if (error_ass_brand) {
                                                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand);
                                                        } else {
                                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                                        }

                                                    });
                                                    // ends here
                                                } else {
                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    })
                }
            })
        } else {
            let obj = {
                "name": req.body.first_name,
                "last_name": req.body.last_name,
                "email": req.body.email,
                "isd": req.body.isd,
                "phone": req.body.mobile,
                "isd": req.body.isd,
                "status": req.body.status,
                "organisation_id": req.body.organisation_id,
                "manager": req.body.manager,
                "designation": req.body.designation,
                "notes": req.body.notes,
                "address": req.body.address,
                "country": req.body.country,
                "state": req.body.state,
                "city": req.body.city,
                "associated_brands": req.body.associated_brands,
                "updated_at": Date.now()
            };
            let myquery = {
                _id: new ObjectId(req.body.user_id)
            };
            let newvalues = {
                $set: obj
            };
            advertiserService.arrayUpdate(myquery, newvalues, { new: true }, (error, result_) => {
                if (error) {
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    // Check address ID
                    if (req.body.address_id) {
                        // Save old address
                        let myquery = {
                            _id: new ObjectId(req.body.organisation_id),
                            "addresses.advertisers": new ObjectId(req.body.user_id)
                        };
                        let newvalues = {
                            $pull: {
                                "addresses.$.advertisers": new ObjectId(req.body.user_id)
                            }
                        }
                        organisationService.updateOne(myquery, newvalues, { safe: true }, (error_org_edit, addressupdate_edit) => {
                            if (error_org_edit) {
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_edit);
                            } else {
                                // Save old address
                                let myquery = {
                                    _id: new ObjectId(req.body.organisation_id),
                                    "addresses._id": new ObjectId(req.body.address_id)
                                };
                                let newvalues = {
                                    $push: {
                                        "addresses.$.advertisers": new ObjectId(req.body.user_id)
                                    }
                                }
                                organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_edit2, addressupdate_edit2) => {
                                    if (error_org_edit2) {
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_edit2);
                                    } else {
                                        // check if has brands
                                        if (req.body.associated_brands) {
                                            // Insert associated brands with advetiser id
                                            let myquery1 = {
                                                _id: new ObjectId(req.body.organisation_id),
                                                "associated_brands.advertiser_id": req.body.user_id
                                            };
                                            let newvalues1 = {
                                                $set: {
                                                    "associated_brands.$.brands": req.body.associated_brands
                                                }
                                            }
                                            organisationService.updateOne(myquery1, newvalues1, { new: true }, (error_ass_brand, res_ass_brand) => {
                                                if (error_ass_brand) {
                                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand);
                                                } else {
                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                                }
                                            });
                                            // ends here
                                        } else {
                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                        }
                                    }
                                });
                            }
                        });

                    } else {
                        // Save new address
                        let myquery = {
                            _id: new ObjectId(req.body.organisation_id),
                            "addresses._id": new ObjectId(req.body.old_address_id)
                        };
                        let newvalues = {
                            $pull: {
                                "addresses.$.advertisers": new ObjectId(req.body.user_id)
                            }
                        }
                        organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_edit_old, addressupdate_edit2_old) => {
                            if (error_org_edit_old) {
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_edit_old);
                            } else {
                                // save new address
                                let myquery = { _id: new ObjectId(req.body.organisation_id) };
                                let newvalues = {
                                    $push: {
                                        addresses: {
                                            advertisers: new ObjectId(req.body.user_id),
                                            address: req.body.address,
                                            country: req.body.country,
                                            state: req.body.state,
                                            city: req.body.city,
                                        }
                                    }
                                }
                                organisationService.updateOne(myquery, newvalues, { new: true }, (error_org_2_old, addressupdate1_old) => {
                                    if (error_org_2_old) {
                                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_org_2_old);
                                    } else {
                                        // check if has brands
                                        if (req.body.associated_brands) {
                                            // Insert associated brands with advetiser id
                                            let myquery1 = {
                                                _id: new ObjectId(req.body.organisation_id),
                                                "associated_brands.advertiser_id": req.body.user_id
                                            };
                                            let newvalues1 = {
                                                $set: {
                                                    "associated_brands.$.brands": req.body.associated_brands
                                                }
                                            }
                                            organisationService.updateOne(myquery1, newvalues1, { safe: true }, (error_ass_brand, res_ass_brand) => {
                                                if (error_ass_brand) {
                                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_ass_brand);
                                                } else {
                                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                                }
                                            });
                                            // ends here
                                        } else {
                                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Updated Successfully', result_);
                                        }
                                    }
                                });

                            }
                        });
                    }
                }
            })
        }
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [status Update of Advertiser]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const statusUpdate = (req, res) => {
    let check = helper.checkRequest(["user_id", "status"], req.body);
    if (check == true) {
        let obj = {
            status: req.body.status
        }
        let myquery = {
            _id: new ObjectId(req.body.user_id)
        };
        let newvalues = {
            $set: obj
        };
        advertiserService.arrayUpdate(myquery, newvalues, { new: true }, (error, result_) => {
            if (error) {
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Status Updated Successfully', result_);
            }
        })
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

// Test api to hash password
const hashAdverPassword = async (req, res) => {
    var result;
    var password;
        let query = {
            $where: 'this.password.length != 60'
        }
        try {
            result = await advertiserModel.find(query);
            console.log(result.length,'----');
            result.forEach(async(advertiser) => {
                password = advertiser.password;
                console.log(password,'--old ');
                var salt = bcrypt.genSaltSync(10);
                var hash = bcrypt.hashSync(password, salt);
                password =  hash;
                console.log(password);
                try {
                    await advertiserModel.findByIdAndUpdate({_id : advertiser._id}, {$set : {'password' : password}});
                } catch (error) {
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error)
                }
            })
            console.log('password hashing completed!');
        } catch (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error)
    }
}

/* Export apis */
module.exports = {
    createAdvertiser,
    advertiserData,
    advertiserImageUpload,
    advertiserList,
    editAdvertiser,
    statusUpdate,
    hashAdverPassword
}