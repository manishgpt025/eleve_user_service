const request = require('request-promise');
const qs = require('qs');
// use Instagram-node
var api = require('instagram-node').instagram();
//use Twitter
var tw = require('node-twitter-api');
// user services
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;
//use jwt config
const config = require('../config.js');
const secret = `${global.gConfig.secret}`;
//use Instagram tokens
const insta_client_secret = `${global.gConfig.instagram_client_secret}`;
const insta_client_id = `${global.gConfig.instagram_client_id}`;
const insta_redirect_uri = `${global.gConfig.instagram_redirect_uri}`;
// use UserModel
const userModel = require('../models/userModel.js');
//use helper
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
let jwt = require('jsonwebtoken');
//
api.use({
    client_id: insta_client_id,
    client_secret: insta_client_secret
});
var redirect_uri = insta_redirect_uri;

//insta login
const authorize_user = function(req, res) {
    res.redirect(api.get_authorization_url(redirect_uri, { scope: ['likes'], state: 'a state' }));
};

// insta login callback
const handleauth = function(req, res) {
    // console.log(req.body);
    // return false;
    api.authorize_user(req.body.code, redirect_uri, function(err, result) {
        if (err) {
            console.log(err.body);
            res.send("Didn't work");
        } else {
            let log_access_token = result.access_token;
            console.log('Yay! Access token is ' + log_access_token);
            // res.send('You made it!!');
            api.use({
                client_id: "ee1e1d852296409993e4aca937083273",
                client_secret: "e2c456bc3259451d859f8ad07749317d",
                access_token: log_access_token
            });
            api.user(log_access_token.split('.')[0], function(error, result, remaining, limit) {
                if (error) res.json(error);
                let username = result.username;
                console.log(username);
                userService.getData({ "instagram.username": username }, (err, userInfo) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else {
                        if (userInfo) {
                            console.log(userInfo);
                            let token = jwt.sign({ email: username },
                                secret, {
                                    expiresIn: '24h' // expires in 24 hours
                                }
                            );
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User found!', userInfo, token);
                        } else {
                            console.log("user not found");
                            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                        }
                    }
                });
            });
        }
    });
};

// add manual platforms
const addConnection = function(req, res) {
    let user_email = req.body.decoded.email.toLowerCase();
    let platform = req.body.platform;
    let platform_array = {};
    let input_array = {};
    let input_platform = {};
    let input_platform_user = {};
    if(platform){
        let platform_social = `${platform}.Elv_social`;
        platform_array = {
            'name': req.body.handle_name,
            'Elv_social': req.body.handle_name,
            'followers_count': req.body.reach,
            'link': req.body.link,
            'connection_type': '',
            'category': '',
            'genres': [],
            'bio': '',
            'followers_count':'',
            'profile_image_url' :''
        }
        input_array = {
            [platform]: platform_array
        };
        input_platform_user = {
            email: user_email,
            [platform_social]: req.body.handle_name
        }
        input_platform = {
            [platform_social]: req.body.handle_name
        }
        userService.findData({ input_platform_user }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
        } else {
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'You already have this account mentioned on your profile');
            } else {
                userService.findData(input_platform, (error2, userInfo1) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                    } else {
                        if (userInfo1) {
                            responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This account is already connected by a different influencer. Please try a different account');
                        } else {
                            let myquery = { email: user_email }
                            let newvalues = {
                                $push: input_array
                            };
                            userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                                if (error2) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Social connection added');
                                }
                            });
                        }
                    }
                });

            }
        }
        });
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Platform not found');
    }

};

// add multiple twitter login
const twitterlogin = function(req, res) {
    let screen_name = req.body.screen_name;
    userService.findData({ 'twitter.screen_name': req.body.twitter_array['screen_name'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            // console.log(userInfo);
            // return false;
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'Screen name Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'twitter': req.body.twitter_array
                    }
                };
                console.log(myquery);
                console.log(newvalues);
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    // console.log(userupdate);
                    // return false;
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Twitter Social connection Added');
                    }
                });
            }
        }
    });
};

// add multiple facebook login
const facebooklogin = function(req, res) {
    // console.log(req.body.facebook_array['id']);
    // return false;
    userService.findData({ 'facebook.id': req.body.facebook_array['id'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            // console.log(userInfo);
            // return false;
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Facebook account Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'facebook': req.body.facebook_array
                    }
                };
                console.log(myquery);
                console.log(newvalues);
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    // console.log(userupdate);
                    // return false;
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Facebook Social connection Added');
                    }
                });
            }
        }
    });
};

// add multiple youtube login
const youtubelogin = function(req, res) {
    // console.log(req.body.facebook_array['id']);
    // return false;
    userService.findData({ 'youtube.id': req.body.youtube_array['id'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            // console.log(userInfo);
            // return false;
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Youtube account Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'youtube': req.body.youtube_array
                    }
                };
                console.log(myquery);
                console.log(newvalues);
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    // console.log(userupdate);
                    // return false;
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Youtube Social connection Added');
                    }
                });
            }
        }
    });
};


// add multiple linkdien login
const linkedinlogin = function(req, res) {
    // console.log(req.body.facebook_array['id']);
    // return false;
    userService.findData({ 'linkdien.id': req.body.linkdien_array['id'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            // console.log(userInfo);
            // return false;
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'linkdien account Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'linkdien': req.body.linkdien_array
                    }
                };
                console.log(myquery);
                console.log(newvalues);
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    // console.log(userupdate);
                    // return false;
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Linkdien Social connection Added');
                    }
                });
            }
        }
    });
};

// add multiple pinterest login
const pinterestlogin = function(req, res) {
    // console.log(req.body.facebook_array['id']);
    // return false;
    userService.findData({ 'pinterest.id': req.body.pinterest_array['id'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            // console.log(userInfo);
            // return false;
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Pinterest account Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'pinterest': req.body.pinterest_array
                    }
                };
                console.log(myquery);
                console.log(newvalues);
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    // console.log(userupdate);
                    // return false;
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Pinterest Social connection Added');
                    }
                });
            }
        }
    });
};

// add multiple blog login
const bloglogin = function(req, res) {
    userService.findData({ 'blog.id': req.body.blog_array['id'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Blog account Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'blog': req.body.blog_array
                    }
                };
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Youtube Social connection Added');
                    }
                });
            }
        }
    });
};

// add multiple instagram login
const instagramlogin = function(req, res) {

    userService.findData({ 'instagram.username': req.body.instagram_array['username'] }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, err);
        } else {
            // console.log(userInfo);
            // return false;
            if (userInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Instagram account Exists');
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $push: {
                        'instagram': req.body.instagram_array
                    }
                };
                console.log(myquery);
                console.log(newvalues);
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    // console.log(userupdate);
                    // return false;
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Youtube Social connection Added');
                    }
                });
            }
        }
    });
};

// delete Manual Connection account
const deleteConnection = function(req, res) {
    let user_email = req.body.decoded.email.toLowerCase();
    let platform = req.body.platform;
    let input_array = '';
    let input_platform_user = '';
    if (platform) {
        let platform_social = `${platform}.Elv_social`;
        let platform_connection_type = `${platform}.connection_type`;
        input_array = {
            [platform]: {
                'name': req.body.screen_name
            }
        };
        input_platform_user = {
            email: user_email,
            [platform_social]: req.body.screen_name,
            [platform_connection_type]: ""
        };
    }
    userService.findData(input_platform_user, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, error_);
        } else {
            if (userInfo) {
                let myquery = { email: user_email };
                let newvalues = {
                    $pull: input_array
                };
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Twitter Account Deleted');
                    }
                });
            }
        }
    });
}

// Delete Auto Social connections
const deleteSocials = function(req, res) {
    let input_array = '';
    let input_platform_user = '';
    let platform = req.body.platform;
    let user_email = req.body.decoded.email.toLowerCase();
    if(platform){
        let platform_social = `${platform}.Elv_social`;
        let platform_connection_type = `${platform}.connection_type`;
        input_array = {
            [platform]: {
                'Elv_social': req.body.social_name
            }
        };
        input_platform_user = {
            email: user_email,
            [platform_social]: req.body.social_name,
            [platform_connection_type]:'auto'
        };
    }else{
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide platform');
    }
    userService.findData(input_platform_user, (error_, userSocailInfo) => {
        if (error_) {
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
        } else {
            if (userSocailInfo) {
                let myquery = { email: user_email };
                let newvalues = {
                    $pull: input_array
                };
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, req.body.platform + ' account removed');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Social account not found')
            }
        }
    });
}

/**
 * [Blog Add]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addBlog = (req, res) => {
    let check = helper.checkRequest(["platform"], req.body);
    let user_email = req.body.decoded.email.toLowerCase();
    if (check == true) {
        let input_array = '';
        let check_platform = '';
        if (req.body.platform == 'blog') {
            let platform_array = {
                'name': req.body.blogTitle,
                'Elv_social': req.body.blogTitle,
                'link': req.body.blogUrl,
                'category': '',
                'followers_count': '',
                'connection_type': "auto",
                'bio':'',
                'genres': [],
            }
            input_array = {
                blog: platform_array
            }
            check_platform = {
                'blog.Elv_social': req.body.blogTitle
            }
        }else{
            return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide platform');
        }
        userService.findData(check_platform, (error2, ExistsAccount) => {
            if (error2) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
            } else {
                if(ExistsAccount){
                    return responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Already exists.Please try different blog');
                }else{
                    let myquery = {
                        email: user_email
                    };
                    let value = {
                        $push: input_array
                    }
                    userService.updateOne(myquery, value, {
                        new: true
                    }, (error_, userupdate) => {
                        if (error_) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Blog Added successfully!');
                        }
                    });
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}


/**
 * [Blog Delete]
 * @param {[type]} req [object received from the application.]
 * @param {[type]} res [object to be sent as response from the api.]
 * @return {[type]} [function call to return the appropriate response]
 */
const deleteBlog = (req, res) => {
    let input_array = '';
    input_array = {
        'blog': {
            'Elv_social': req.body.title
        }
    };

    let myquery = {
        _id: new ObjectId(req.body.user_id)
    };
    let newvalues = {
        $pull: input_array
    };
    userService.updateOne(myquery, newvalues, {
        new: true
    }, (error_, userupdate) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
        } else {
            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Blog Deleted successfully!', userupdate);
        }

    });
}

/*Export apis*/
module.exports = {
    authorize_user,
    handleauth,
    addConnection,
    twitterlogin,
    facebooklogin,
    youtubelogin,
    linkedinlogin,
    pinterestlogin,
    bloglogin,
    instagramlogin,
    deleteConnection,
    deleteBlog,
    deleteSocials,
    addBlog
}