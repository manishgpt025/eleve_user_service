const helper = require('../globalFunctions/function.js');//use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const userService = require('../services/userServices.js');// user services
const ObjectId = require('mongodb').ObjectID;

/**
 * [Notifications ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const settings = (req, res) => {
    let campaign_notify = {
        'email': req.body.campaign_email,
        'sms': req.body.campaign_sms,
        'app_push': req.body.campaign_app_push
    };
    let account_notify = {
        'email': req.body.account_email,
        'sms': req.body.account_sms,
        'app_push': req.body.account_app_push
    };
    let payment_notify = {
        'email': req.body.payment_email,
        'sms': req.body.payment_sms,
        'app_push': req.body.payment_app_push
    };
    let new_feature_notify = {
        'email': req.body.new_feature_email,
        'sms': req.body.new_feature_sms,
        'app_push': req.body.new_feature_app_push
    };
    userService.findData({ _id: new ObjectId(req.body.user_id) }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
        } else {
            if (userInfo) {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $set: {
                        campaign_notification: campaign_notify,
                        payment_notification: payment_notify,
                        new_feature_notification: new_feature_notify,
                        account_notification: account_notify,
                    }
                };
                userService.updateOne(myquery, newvalues, { new: true }, (err, userupdate) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, 'settings Updated Successfully');
                    }
                });
            } else {
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User Not Found');
            }
        }
    });
}

/*Export apis*/
module.exports = {
    settings,
}