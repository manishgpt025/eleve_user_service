const helper = require('../globalFunctions/function.js'); //use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
// user services
const userService = require('../services/userServices.js');

/*create apis*/
/**
 * [Reset Password -  function resetPassword for setting the new password for the user
]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const resetPassword = (req, res) => {
    let check = helper.checkRequest(["email", "newpassword", "confirmnewpassword"], req.body);
    if (check == true) {
        if (req.body.newpassword != req.body.confirmnewpassword) {
            responseHandle.sendResponsewithError(res, 230, "Both Passwords don't match");
        }
        userService.findData({ email: req.body.email }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    let query = { email: req.body.email };
                    let newvalues = { $set: { password: req.body.newpassword } };
                    userService.updateOne(query, newvalues, { new: true }, (err, result) => {
                        if (err) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Your password has been updated.', result);
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, 'Emailid ' + req.body.email + ' is not registered with us.');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Forgot Password -  function forgotPassword for sending reset link in mail to the user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const forgotPassword = (req, res) => {
    let check = helper.checkRequest(["email", "url"], req.body);
    var randomstring = require("randomstring");
    if (check == true) {
       userService.findData({ email: req.body.email }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    let query = { email: req.body.email };
                    let resetPasswordToken = randomstring.generate(7);
                    let resetPasswordExpires = Date.now() + 3600000; // 1 hour
                    let newvalues = { $set: { resetPasswordToken: resetPasswordToken, resetPasswordExpires: resetPasswordExpires } };
                    userService.updateOne(query, newvalues, { new: true }, (err, result) => {
                        if (err) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                        } else {
                            var message = {
                                "html": "<p>Hi , Please find below the link for reset password <br />- " + req.body.url + "/reset-password?email=" + req.body.email + "&token=" + resetPasswordToken + "</p>",
                                "subject": "Reset password Link",
                                "from_email": "team@eleve.co.in",
                                "from_name": "Eleve",
                                "to": [{
                                    "email": req.body.email,
                                    "name": userInfo.name,
                                    "type": "to"
                                }]
                            };
                            helper.sendEmail(message);
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Check the email we have sent you on the submitted email ID.', result);
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, 'Emailid ' + req.body.email + ' is not registered with us.');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [check token validity -  function checkTokenValidity for checking the expiry of the link.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkTokenValidity = (req, res) => {
    let check = helper.checkRequest(["email", "token"], req.body);
    if (check == true) {
        // Check from DB Collection
       userService.findData({ email: req.body.email, resetPasswordToken: req.body.token }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    let timenow = Date.now();
                    let dateexpiry = new Date(userInfo.resetPasswordExpires);
                    var milliseconds = dateexpiry.getTime();
                    if (timenow > milliseconds) {
                        responseHandle.sendResponseWithData(res, responseCode.WENT_WRONG, 'Expired');
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Valid');
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/*Export apis*/
module.exports = {
    resetPassword,
    forgotPassword,
    checkTokenValidity
}