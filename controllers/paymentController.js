const helper = require('../globalFunctions/function.js');// Helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const userService = require('../services/userServices.js');// user services
const countryService = require('../services/countryService.js');// Country services
const cityService = require('../services/cityService.js');// cityService
const currencyService = require('../services/currencyService.js');//currency service
const ObjectId = require('mongodb').ObjectID;// Object ID

/**
 * [Add payment -  Function not currently in use]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const paymentAdd = (req, res) => {
    let country = req.body.country;
    if (country == 'India') {
        var gst_details = {
            'gst_no': req.body.gst_no,
            'sac_code': req.body.sac_code,
            'state': req.body.gst_state,
            'code': req.body.code,
            'nature_of_service': req.body.nature_of_service,
            'pan_image': req.body.upload_pan,
            'gst_image': req.body.upload_gst
        };
    } else if (country == 'Indonesia') {
        var gst_details = {
            'ektp': req.body.ektp,
            'npwp': req.body.npwp,
            'ektp_image': req.body.upload_ektp,
            'npwp_image': req.body.upload_npwp
        };
    } else {
        var gst_details = {
            'tax_id': req.body.tax_id,
            'relation': req.body.relation
        };
    }
    userService.findData({ _id: new ObjectId(req.body.user_id) }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, 500, error_);
        } else {
            let myquery = { _id: new ObjectId(req.body.user_id) };
            let newvalues = {
                $set: {
                    gst_details: gst_details
                }
            };
            userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                if (error2) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'GST Updated');
                }
            });
        }
    });
}

/**
 * [Get GST data -  function getGstData to get the payment Details for the user.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getGstData = (req, res) => {
    userService.getData({ _id: req.body.user_id }, { 'gst_details': 1 }, (err, userInfo) => {
        if (err) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
        } else {
            if(userInfo)
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Record found!', userInfo);
            else
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Record not found!');
        }
    });
}

/**
 * [Get State List - function stateList to get the state List based on country.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const stateList = (req, res) => {
    let country = req.body.country;
    cityService.findData({ "name": country }, (error, countryInfo) => {
        if (error) {
            responseHandle.sendResponsewithError(res, 500, error);
        } else {
            if (countryInfo) {
                let country_test_id = countryInfo.id;
                let myquery = { "country_id": country_test_id };
                cityService.getData(myquery, (error_, stateInfo) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, 500, error_);
                    } else {
                        if (stateInfo) {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'State found!', stateInfo);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'State found!', []);
                        }
                    }
                });
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'State found!', [])
            }

        }
    });
}

/**
 * [Get Country List - function countryList  to get the country List.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const countryList = (req, res) => {
    cityService.getData({ "state_id": "0", "city_id": "0" }, (err1, countryInfo) => {
        if (err1) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err1);
        } else {
            if (countryInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Countries found!', countryInfo);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'No Countries', []);

            }
        }
    });
}

/**
 * [Get Country ISD - function countryISD  to get the country ISD list.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const countryISD = (req, res) => {
    countryService.getData({}, (err, countryInfo) => {
        if (err) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
        } else {
            if (countryInfo) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Country ISD Code found', countryInfo);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Country ISD Code found', []);
            }
        }
    });
}

/**
 * [Get city List - function cityList to get the city List based on state.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const cityList = (req, res) => {
    let state = req.body.state;
    cityService.findData({ "name": state }, (err1, stateInfo) => {
        if (err1) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err1);
        } else {
            if (stateInfo) {
                let state_code_id = stateInfo.state_id;
                let myquery = { "city_id": state_code_id };
                cityService.getData(myquery, (err, cityInfo) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, 500, err);
                    } else {
                        if (cityInfo) {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'City found!', cityInfo);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'City found!', []);
                        }
                    }
                });
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'No State!', [])
            }
        }
    });
}

/**
 * [Get Currency List - function]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const currencyList = (req, res) => {
    currencyService.getData({}, (error, result) => {
        if (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
        } else {
            if (result) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Currency List found!', result);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'No Currency List', []);

            }
        }
    });
}

/**
 * [Add Payment Post Form Data- function addPaymentPostData to Add payemnet details with multipart form.]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addPaymentPostData = (req, res) => {
    let userQuery = { _id: req.body.user_id};
    userService.findData(userQuery, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
        } else {
            // Prepare Data Set here
            let country  = req.body.country_name;
            if (country == 'India') {
                gst_details = {
                    'pan_number': req.body.pan_number,
                    'gst_number': req.body.gst_number,
                    'sac_code': req.body.sac_code,
                    'gst_state': req.body.gst,
                    'code': req.body.code,
                    'nature_of_service': req.body.nature_service,
                    'state_code': req.body.state_code,
                    'pan_image': req.body.pan_image != '' ? req.body.pan_image : userInfo.payment_details && userInfo.payment_details[0].pan_image ? userInfo.payment_details[0].pan_image:'',
                    'gst_image': req.body.gst_image !='' ? req.body.gst_image : userInfo.payment_details && userInfo.payment_details[0].gst_image ? userInfo.payment_details[0].gst_image:''
                };
            } else if (country == 'Indonesia') {
                gst_details = {
                    'ektp': req.body.ktp_number,
                    'npwp': req.body.npwp,
                    'ektp_image': req.body.ktp_image != "" ? req.body.ktp_image : userInfo.payment_details && userInfo.payment_details[0].ktp_image ? userInfo.payment_details[0].ktp_image:'',
                    'npwp_image': req.body.npwp_image != "" ? req.body.npwp_image : userInfo.payment_details && userInfo.payment_details[0].npwp_image ? userInfo.payment_details[0].npwp_image:'',
                };
            } else {
                gst_details = {
                    'tax_id': req.body.tax_id,
                    'business_relation': req.body.business_relation,
                    'tax_image': req.body.tax_image != "" ? req.body.tax_image : userInfo.payment_details && userInfo.payment_details[0].tax_image ? userInfo.payment_details[0].tax_image:'',
                };
            }
            // Check request and database values
            if (req.body.country_name != userInfo.country) {

                let userQuery = {
                    name: req.body.country_name
                };
                countryService.findData(userQuery, (err, countryISD) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, 500, err);
                    } else {
                        if (countryISD) {
                            // Make Query
                            let myquery = { _id: req.body.user_id };
                            let newvalues = {
                                $set: {
                                    payment_details: gst_details,
                                    country: req.body.country_name,
                                    state: req.body.state_name,
                                    city: req.body.city_name,
                                    address:req.body.billing_address,
                                    isd_code: countryISD.dial_code,
                                    isd_mobile: countryISD.dial_code + userInfo.mobile,
                                    is_verified: 0
                                }
                            };
                            userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                                if (error2) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Payment Updated', userupdate);
                                }
                            });
                        }else{
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong.Please again.');
                        }
                    }
                });
            } else {
                // Make Query
                let myquery = { _id: req.body.user_id };
                let newvalues = {
                    $set: {
                        payment_details: gst_details,
                        state: req.body.state_name,
                        city: req.body.city_name,
                        address:req.body.billing_address
                    }
                };
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Payment Updated', userupdate);
                    }
                });
            }
        }
    });
}

/*Export apis*/
module.exports = {
    paymentAdd,
    getGstData,
    stateList,
    countryList,
    cityList,
    currencyList,
    addPaymentPostData,
    countryISD
}