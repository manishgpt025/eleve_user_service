const request = require('request-promise');
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;
const config = require('../config.js');
const secret = `${global.gConfig.secret}`;
const userModel = require('../models/userModel.js');
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
let jwt = require('jsonwebtoken');
var FB = require('fb');

/**
 * [Instagram Business Connect]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const instagramBusinessConnect = function(req, res) {
    var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name', 'gender', 'picture', 'friends', 'accounts'];
    var accessTokenUrl = 'https://graph.facebook.com/v2.8/oauth/access_token';
    var graphApiUrl = 'https://graph.facebook.com/v2.8/me?fields=' + fields.join(',');
   // var pageApiUrl = 'https://graph.facebook.com/v2.8//me/accounts';
    var params = {
        code: req.body.code,
        client_id: process.env.FBCLIENT_ID,
        client_secret: process.env.FBCLIENT_SECRET,
        redirect_uri: req.body.redirectUri
    };
    request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
        console.log(response);
        if (response.statusCode !== 200) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Access token error');
        }
        // Step 2. Retrieve profile information about the current user.
        request.get({ url: graphApiUrl, qs: accessToken, json: true }, function(err, response, profile) {
            if (response.statusCode !== 200) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Profile error');
            }
           // responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'profile!', profile);
           
          // return;
            for(var i = 0; i < profile.accounts.data.length;i++){
                var businessData = {};
                        businessData.pageName = profile.accounts.data[i].name;
                        businessData.accessToken = accessToken;
                        businessData.id = profile.id;
                        businessData.email = profile.email;
                        businessData.name = profile.name;
                        businessData.profileurl = profile.picture.data.url;
                let pageApiUrl="https://graph.facebook.com/v3.2/"+ profile.accounts.data[i].id+ "?fields=instagram_business_account";
            // Step 3. Retrieve page id  information about the current user.
                request.get({ url: pageApiUrl, qs: accessToken, json: true }, function(err, response, pageinfo) {
                    if (response.statusCode !== 200) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    }
                    if(pageinfo.instagram_business_account){
                       
                        businessData.instagram_business_id = pageinfo.instagram_business_account.id;
                        businessData.pageid = pageinfo.id;
                        businessData.pagename = pageinfo.name;
                        
                        console.log(businessData);
                        let myquery = {
                            _id: { $ne: new ObjectId(req.body.user_id) },
                            'instagram_business.instagram_business_id': pageinfo.instagram_business_account.id
                        };
                        userService.findData(myquery, (err, userInfo) => {
                            if (err) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                            } else {
                                if (userInfo) {
                                    responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Please try with diffrent account, this instagram business account  already exists.');
                                } else {
                                    let myquery = {
                                        _id: new ObjectId(req.body.user_id),
                                        'instagram_business.instagram_business_id': pageinfo.instagram_business_account.id
                                    };
                                    userService.findData( myquery, (error_, userInfo1) => {
                                        if (error_) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                        } else {
                                            if (userInfo1) {
                                                responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'You already have this account mentioned on your profile');
        
                                            } else{
                                                 // Saving data
                                                let query = {
                                                    _id: new ObjectId(req.body.user_id)
                                                };
                                                let data = {
                                                    $set: {
                                                        instagram_business: businessData
                                                    }
                                                }
                                                userService.updateOne(query, data, {
                                                    new: true
                                                }, (error, updatedResp) => {
                                                    if (error) {
                                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                    } else {
                                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Updated Successfully!', updatedResp);
                                                    /// continue;
                                                    }
                                                });
                                            }
                                            
                                        }
                                    });
                                }
                            }
                        });
                    }
                    
                });
            }
            // responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Updated Successfully!', profile);
        });
    });
};

/**
 * [Instagram Business Auth API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const instagramBusinessCodeURI = function(req, res) {
    
    var codeUrl = 'https://www.facebook.com/dialog/oauth';
    let code_url = codeUrl + '?client_id=' + process.env.FBCLIENT_ID + '&redirect_uri=' + 'https://dev.eleveglobal.com/instagram/business/callback' + '&scope=manage_pages&auth_type=rerequest';
    return res.status(200).send({ code_url });
};

/*Export apis*/
module.exports = {
    instagramBusinessConnect: instagramBusinessConnect,
    instagramBusinessCodeURI: instagramBusinessCodeURI,
}