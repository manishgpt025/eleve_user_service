const userService = require('../services/userServices.js');// user services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
let otpGenerator = require('otp-generator');
const ObjectId = require('mongodb').ObjectID;// Obj ID
var AWS = require('aws-sdk');//S3 Upload
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });

/*create apis*/
/**
 * [Get user data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userData = (req, res) => {
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.getData({
            _id: req.body.user_id
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User found!', userInfo);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Update the user data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userDataUpdate = (req, res) => {
    let check = helper.checkRequest(["user_id", "first_name", "mobile"], req.body);
    if (check == true) {
        let myquery = { _id: new ObjectId(req.body.user_id)};
        let newvalues = {
            $set: {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                gender: req.body.gender ? req.body.gender : "",
                dob: req.body.dob  ? req.body.dob : "",
                country: req.body.country ? req.body.country : "",
                state: req.body.state ? req.body.state : "",
                city: req.body.city ? req.body.city : "",
                address: req.body.address ? req.body.address : "",
                alt_email: req.body.alt_email ? req.body.alt_email : "",
                mobile: req.body.mobile ? req.body.mobile : "",
                alt_mobile: req.body.alt_mobile ? req.body.alt_mobile : "",
                isd_code: req.body.isd_code ? req.body.isd_code : "",
                alt_isd_code: req.body.alt_isd_code ? req.body.alt_isd_code : "",
                isd_mobile: req.body.isd_code + req.body.mobile,
                isd_alt_mobile: req.body.alt_isd_code + (req.body.alt_mobile) ? req.body.alt_mobile : "",
                is_verified: req.body.is_verified
            }
        }
        userService.updateOne(myquery, newvalues, {
            new: true
        }, (error, userupdate) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (req.body.profilePic) {
                    buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                    let number = Math.random() * (999999 - 10000) + 10000
                    let resData = {
                        Key: req.body.user_id + number,
                        Body: buf,
                        ContentEncoding: 'base64',
                        ContentType: 'image/jpeg',
                        ACL: 'public-read'
                    };
                    s3Bucket.upload(resData, function(err, data) {
                        if (err) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            let imageValue = {
                                $set: { profile_pic: data.Location}
                            }
                            userService.updateOne(myquery, imageValue, {
                                new: true
                            }, (error_, userupdate) => {
                                if (error_) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Updated Successfully!', userupdate);
                                }

                            });
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Updated Successfully!', userupdate);
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

}

/**
 * [Check phone number]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const phoneCheck = (req, res) => {
    let check = helper.checkRequest(["mobile", "user_id"], req.body);
    if (check == true) {
        let myquery = {
            _id: { $ne: new ObjectId(req.body.user_id) },
            isd_mobile: req.body.mobile
        };
        userService.findData(myquery, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Mobile already exists');
                } else {
                    let myquery2 = {
                        _id: new ObjectId(req.body.user_id),
                        isd_mobile: req.body.mobile
                    };
                    userService.findData(myquery2, (error2, resp2) => {
                        if (error2) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                        } else {
                            if (resp2) {
                                responseHandle.sendResponseWithData(res, responseCode.DUPLICATE_REGNO, 'User has this mobile', resp2);
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Mobile not registered');
                            }
                        }
                    });
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Send OTP]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const sendOTP = (req, res) => {
    let check = helper.checkRequest(["isd_code", "mobile", "user_id"], req.body);
    if (check == true) {
        let ISD = req.body.isd_code;
        let mobile = req.body.mobile;
        let isd_mobile = ISD + mobile;
        let otp = otpGenerator.generate(6, { digits: true, alphabets: false, upperCase: false, specialChars: false });
        let data = {
            mobile: isd_mobile,
            otp: otp
        }
        // Send OTP
        helper.sendSMS(data);
        // Save OTP in DB
        let otp_data = {
            "otp": otp
        };
        let myquery = {
            _id: new ObjectId(req.body.user_id)
        };
        let newvalues = {
            $set: otp_data
        };
        userService.arrayUpdate(myquery, newvalues, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'OTP sent successfully');
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }

};

/**
 * [Verify OTP]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const verifyOTP = (req, res) => {
    let check = helper.checkRequest(["otp", "user_id", "isd_code", "mobile"], req.body);
    if (check == true) {
        // Check OTP is valid or not
        let myquery = {
            _id: new ObjectId(req.body.user_id),
            otp: req.body.otp
        };
        userService.findData(myquery, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else if (!resp) {
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'OTP invalid');
            } else {
                let ISD = req.body.isd_code;
                let mobile = req.body.mobile;
                let isd_mobile = ISD + mobile;
                let data = {
                    "isd_mobile": isd_mobile,
                    "otp": req.body.otp,
                    "isd_code": ISD,
                    "mobile": mobile,
                    "is_verified": 1
                };
                let newvalues = {
                    $set: data
                };
                userService.arrayUpdate(myquery, newvalues, (error2, resp2) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error2);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'OTP verified');
                    }
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userImageUpload = (req, res) => {
    let check = helper.checkRequest(["user_id", "profilePic"], req.body);
    if (check == true) {
        buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        let number = Math.random() * (999999 - 10000) + 10000
        let resData = {
            Key: req.body.user_id + number,
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg',
            ACL: 'public-read'
        };
        s3Bucket.upload(resData, function(err, data) {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                let myquery = { _id: new ObjectId(req.body.user_id)};
                let imageValue = { $set: {profile_pic: data.Location}}
                userService.updateOne(myquery, imageValue, {
                    new: true
                }, (error_, userupdate) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User images uploaded successfully!', userupdate);
                    }
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Get user skill defalt data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const skillSettings = (req, res) => {
    let check = helper.checkRequest(["decoded"], req.body);
    if (check == true) {
        let adminEmail = req.body.decoded.email.toLowerCase();
        userService.getData({
            email: adminEmail
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    let interested_topics =[
                        "Advertising","Beauty","Business","Celebrity","DIY/Crafts","Entertainment","Family","Fashion","Food","General","Health",
                        "Lifestyle","Music","News","Pop Culture","Social Media","Sports","Technology","Travel","Video Games"
                    ]
                    let life_stags =[
                        "Single Adult","Married Adult","Kids in High School","Kids in High College","Kids in Elementary"
                    ]
                    let educations =[
                        "High School","Senior Secondary","Graduate Degree","Post Graduate Degree","Higher Education"
                    ]
                    let languages =[
                        "Hindi","English","Punjabi","Tamil","Marathi","German","Haryanwi","Gujrati","Rajasthani","Urdu","Bengali",
                        "Spanish","Russian","French","Japanese","Telugu","Arabic"
                    ]
                    let incomes =[
                        "Under US$ 300","US$ 300 - 700","US$ 700 - 1,500","Above US$1,500","Above US$ 7,000"
                    ]
                    let data = {
                        interested_topics,
                        life_stags,
                        educations,
                        languages,
                        incomes
                    }
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Skill default setting fetch successfully', data);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/* Export apis */
module.exports = {
    userData,
    userDataUpdate,
    phoneCheck,
    sendOTP,
    verifyOTP,
    userImageUpload,
    skillSettings
}