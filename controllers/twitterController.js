const request = require('request-promise');
const qs = require('qs');
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;
const userModel = require('../models/userModel.js');
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
let jwt = require('jsonwebtoken');

/**
 * [Twitter login]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const twitterConnection = function(req, res) {
    var twitterData = {};
    var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
    var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    var oauthAuthenticate = 'https://api.twitter.com/oauth/authenticate?oauth_token=';
    var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';

    if ((!req.body.oauth_token || !req.body.oauth_verifier) && (!req.query.oauth_token || !req.query.oauth_verifier)) {
        var requestTokenOauth = {
            consumer_key: process.env.TWITTER_KEY,
            consumer_secret: process.env.TWITTER_SECRET,
            callback: process.env.redirectUri
        };
        if (!requestTokenOauth.callback) {
            requestTokenOauth.callback = req.protocol + 's://' + req.get('host') + req.originalUrl;
        }
        request.post({ url: requestTokenUrl, oauth: requestTokenOauth }, (err, response, body) => {
            var oauthToken = qs.parse(body);
            let code_url = oauthAuthenticate + oauthToken.oauth_token;
            return res.status(200).send({ code_url });
        });
    } else {
        let oauth_token = req.body.oauth_token || req.query.oauth_token;
        let oauth_verifier = req.body.oauth_verifier || req.query.oauth_verifier;
        var accessTokenOauth = {
            consumer_key: process.env.TWITTER_KEY,
            consumer_secret: process.env.TWITTER_SECRET,
            token: oauth_token,
            verifier: oauth_verifier
        };
        // Step 3. Exchange oauth token and oauth verifier for access token.
        request.post({ url: accessTokenUrl, oauth: accessTokenOauth }, function(err, response, accessToken) {
            accessToken = qs.parse(accessToken);
            twitterData.token = accessToken.oauth_token;
            twitterData.tokenSecret = accessToken.oauth_token_secret;
            var profileOauth = {
                consumer_key: process.env.TWITTER_KEY,
                consumer_secret: process.env.TWITTER_SECRET,
                oauth_token: accessToken.oauth_token
            };
            request.get({
                url: profileUrl + accessToken.screen_name,
                oauth: profileOauth,
                json: true
            }, function(err, response, profile) {
                twitterData.id = profile.id;
                twitterData.name = profile.name;
                twitterData.Elv_social = profile.screen_name;
                twitterData.link = process.env.TWITTER_HOME + profile.screen_name;
                twitterData.screen_name = profile.screen_name;
                twitterData.profile_location = profile.profile_location;
                twitterData.bio = profile.description;
                twitterData.followers_count = profile.followers_count;
                twitterData.friends_count = profile.friends_count;
                twitterData.profile_image_url = profile.profile_image_url_https;
                twitterData.connection_type = "auto";
                twitterData.category = "";
                twitterData.genres = [];
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User Updated Successfully!', twitterData);
            });
        });
    }
}

/**
 * [Twitter callback]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getData = function(req, res) {
    var twitterData = {};
    var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';
    let user_email = req.body.decoded.email.toLowerCase();
    let oauth_token = req.body.oauth_token;
    let oauth_verifier = req.body.oauth_verifier;
    var accessTokenOauth = {
        consumer_key: process.env.TWITTER_KEY,
        consumer_secret: process.env.TWITTER_SECRET,
        token: oauth_token,
        verifier: oauth_verifier
    };
    request.post({ url: accessTokenUrl, oauth: accessTokenOauth }, function(err, response, accessToken) {
        accessToken = qs.parse(accessToken);
        twitterData.oauth_token = accessToken.oauth_token;
        twitterData.oauth_token_secret = accessToken.oauth_token_secret;
        var profileOauth = {
            consumer_key: process.env.TWITTER_KEY,
            consumer_secret: process.env.TWITTER_SECRET,
            oauth_token: accessToken.oauth_token
        };
        request.get({
            url: profileUrl + accessToken.screen_name,
            oauth: profileOauth,
            json: true
        }, function(err, response, profile) {
                twitterData.oauth_token = req.body.oauth_token;
                twitterData.oauth_token_secret = req.body.oauth_verifier;
                twitterData.id = profile.id;
                twitterData.name = profile.name;
                twitterData.Elv_social = profile.screen_name;
                twitterData.link = process.env.TWITTER_HOME + profile.screen_name;
                twitterData.screen_name = profile.screen_name;
                twitterData.profile_location = profile.profile_location;
                twitterData.bio = profile.description;
                twitterData.followers_count = profile.followers_count;
                twitterData.friends_count = profile.friends_count;
                twitterData.profile_image_url = profile.profile_image_url_https;
                twitterData.connection_type = "auto";
                twitterData.category = "";
                twitterData.genres = [];
            let myquery = {
                email: { $ne: user_email },
                'twitter.Elv_social': profile.screen_name
            };
            userService.findData(myquery, (err, userInfo) => {
                if (err) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                } else {
                    if (userInfo) {
                        responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Please try with diffrent account, this account is already exists.');
                    } else {
                        // Saving data
                        let query = {
                            email: user_email,
                            'twitter.Elv_social': profile.screen_name
                        };

                        userService.findData(query, (error_, userInfo1) => {
                            if (error_) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                if (userInfo1) {
                                    responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'You already have this account mentioned on your profile');

                                } else {
                                    var query1 = {
                                        email: user_email
                                    }
                                    var data = {
                                        $push: {
                                            twitter: twitterData
                                        }
                                    }
                                    userService.updateOne(query1, data, {
                                        new: true
                                    }, (error, updatedResp) => {
                                        if (error) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                        } else {
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User linked in account added Successfully!', updatedResp);

                                        }
                                    });
                                }

                            }
                        });

                    }
                }
            });

        });
    });

}

/*Export apis*/
module.exports = {
    twitterConnection,
    getData
}