
var randomstring = require("randomstring");
const adminService = require('../services/adminService.js');// user services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const ObjectId = require('mongodb').ObjectID;// Obj ID
let date = require('date-and-time');// Date and time
var AWS = require('aws-sdk');//S3 Upload
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
const adminModel = require('./../models/adminModel');
 //bcrypt to hash password
 var bcrypt = require('bcryptjs');
/**
 * [Get Admin users list]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const usersList = (req, res) => {
    let search_keyword = req.body.keyword;
    let admin_filter = req.body.filter;
    let location_filter = req.body.loc;
    let decoded_data = req.body.decoded;
    // check access
    let myQuery = {'email': decoded_data.email.toLowerCase()}
    // status
    if (admin_filter == "0") {
        var filter = {'$match': { status: { $in: ['0'] } }};
    }
    else if (admin_filter == "1") {
        var filter = {'$match': { status: { $in: ['1'] } }};
    }
    else {
        var filter = {'$match': {} };
    }
    // location
    if (location_filter != "") {
        var location_final = {'$match': { location: { $in: [location_filter] } }};
    } else {
        var location_final = {'$match': {} };
    }
    // Role
    var role =  {$match: { role: { $in: ['admin', 'superadmin'] } }};
    // sort
    var sort = { $sort: { "role": -1, "status" : -1, "name"  : 1 } };
    adminService.findData(myQuery, (error_,adminData) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
        }else if(adminData){
            let admin_credential = {'$match': {_id: { $ne: adminData._id}}};
            let bodyData = {
                search: search_keyword,
                status: filter,
                role,
                location: location_final,
                admin_credential
            }
            let options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 5,
            }
            adminService.getPaginateDataWithAggregate(bodyData, options, sort, (error, adminInfo, pages, total) => {
                if (error) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    if (adminInfo) {
                        let data = {
                            docs: adminInfo,
                            page: req.body.pageNumber || 1,
                            limit: req.body.limit || 5,
                            pages: pages,
                            total: total
                        }
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin List!', data);
                    } else {
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Data not found!');
                    }
                }
            });
        }else{
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Access denied.');
        }
    });

}
/**
 * [Admin Login]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminLogin = (req, res) => {
        let check = helper.checkRequest(["password", "email"], req.body);
        if (check == true) {
            adminService.findData({ 'email': req.body.email.toLowerCase() }, (error, result) => {
                if (error) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else if (result) {
                    bcrypt.compare(req.body.password, result.password, (err, result1) => {
                        if (err) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                        } else if (result1) {
                            if (result.status == '1') {
                                let now = new Date();
                                let myquery = {
                                    _id: new ObjectId(result._id)
                                };
                                let obj = {
                                    logins: {
                                        'login': date.format(now, 'YYYY-MM-DD HH:mm:ss'),
                                        'last_login_from': req.body.login_ip
                                    }
                                }
                                let newvalues = {
                                    $push: obj
                                };
                                adminService.updateDetails(myquery, newvalues, { new: true }, (error, result_) => {
                                    if (error) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        adminService.findDataById(myquery,(err, resData_) => {
                                            if(err)
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                            else
                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'login Updated Successfully', resData_);
                                        });
                                    }
                                });
                            } else {
                                responseHandle.sendResponsewithError(res, 505, 'User is Inactive', error);
                            }
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Credentials do not matched')
                        }

                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_MODIFIED, 'This is not a registered email ID');
                }
            });

        } else {
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
        }
    }
/**
 * [Create Admin]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const createAdmin = (req, res) => {
        let check = helper.checkRequest(["team", "email"], req.body);
        if (check == true) {
            adminService.findData({ 'email': req.body.email }, (err, result) => {
                if (err) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);

                } else if (result) {
                    responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'There already is an admin access associated with this email', result);

                } else {
                    let randomNumber = helper.generateRandomString();
                    if ((req.body.isd == "") || (req.body.mobile == "") || (req.body.isd == null) || (req.body.mobile == null)) {
                        let now = new Date();
                        let obj = {
                            "admin_id": randomNumber,
                            'name': req.body.name,
                            'last_name': req.body.last_name,
                            'email': req.body.email.toLowerCase(),
                            'date': date.format(now, 'YYYY-MM-DD HH:mm:ss'),
                            'status': '1',
                            'role': 'admin',
                            'password': randomstring.generate(8),
                            'mobile': req.body.mobile,
                            'isd': req.body.isd,
                            'team': req.body.team,
                            'first_login': "1"
                        }
                        adminService.createData(obj, (error, result_) => {
                            if (error) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                var message = {
                                    "html": "<p>Hi " + req.body.name +
                                    ",</p><p>Please find below the link <br>" + req.body.email.toLowerCase() + "<br/> " + obj.password + "</p>",
                                    "subject": "Admin Login Credentials",
                                    "from_email": "team@eleve.co.in",
                                    "from_name": "Eleve",
                                    "to": [{
                                        "email": req.body.email,
                                        "type": "to"
                                    }]
                                };
                                let sendmail = helper.sendEmail(message);
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin has been created & invited', result_);
                            }
                        });
                    } else {
                        adminService.findData({ 'isd': req.body.isd, 'mobile': req.body.mobile }, (err1, result1) => {
                            if (err1) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err1);

                            } else if (result1) {
                                responseHandle.sendResponseWithData(res, 403, 'There already is an admin access associated with this mobile number', result1);

                            } else {
                                let now = new Date();
                                let obj = {
                                    "admin_id": randomNumber,
                                    'name': req.body.name,
                                    'last_name': req.body.last_name,
                                    'email': req.body.email.toLowerCase(),
                                    'date': date.format(now, 'YYYY-MM-DD HH:mm:ss'),
                                    'status': '1',
                                    'role': 'admin',
                                    'password': randomstring.generate(8),
                                    'mobile': req.body.mobile,
                                    'isd': req.body.isd,
                                    'team': req.body.team,
                                    'first_login': "1"
                                }
                                adminService.createData(obj, (error, result_) => {
                                    if (error) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        var message = {
                                            "html": "<p>Hi " + req.body.name +
                                            ",</p><p>Please find below the link <br>" + req.body.email.toLowerCase() + "<br/> " + obj.password + "</p>",
                                            "subject": "Admin Login Credentials",
                                            "from_email": "team@eleve.co.in",
                                            "from_name": "Eleve",
                                            "to": [{
                                                "email": req.body.email,
                                                "type": "to"
                                            }]
                                        };
                                        let sendmail = helper.sendEmail(message);
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin has been created & invited', result_);
                                    }
                                });
                            }
                        });
                    }
                }
            });

        } else {
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
        }
    }
/**
 * [Update Status on Admin]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateStatus = (req, res) => {
        let check = helper.checkRequest(["user_id", "status"], req.body);
        if (check == true) {
            let myquery = {
                admin_id: req.body.user_id
            };
            let newvalues = {
                $set: {
                    'status': req.body.status,
                }
            }
            adminService.updateOne(myquery, newvalues, {
                new: true
            }, (error_, userupdate) => {
                if (error_) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Status Updated successfully!', userupdate);
                }
            });
        } else {
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
        }
    }
/**
 * [Update Admin]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminUpdate = (req, res) => {
    let check = helper.checkRequest(["id"], req.body);
    if (check == true) {
        let obj = {
            'name': req.body.name ? req.body.name :"",
            'last_name': req.body.last_name ? req.body.last_name :"",
            'status': req.body.status ? req.body.status :"",
            'mobile': req.body.mobile ? req.body.mobile :"",
            'isd': req.body.isd ? req.body.isd :"",
            'location': req.body.location ? req.body.location :"",
            'dob': req.body.dob ? req.body.dob :"",
            'extension': req.body.extension ? req.body.extension :"",
            'team': req.body.team ? req.body.team :"",
            'role': req.body.role ? req.body.role :"admin"
        }
        let myquery = {
            admin_id: req.body.id
        };
        let newvalues = {
            $set: obj
        }
        adminService.updateOne(myquery, newvalues, (error_, userupdate) => {
            if (error_) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
            } else {
                let newQuery = {
                    _id: new ObjectId(userupdate._id)
                }
                adminService.findDataById(newQuery,(err, resData_) => {
                    if(err)
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    else
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin Updated successfully!', resData_);
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}
/**
 * [Update Password of Admin Users]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminPasswordUpdate = (req, res) => {
    let check = helper.checkRequest(["id", 'password'], req.body);
    let password;
    if (check == true) {
        adminService.findData({'_id': new ObjectId(req.body.id) }, (error1, result1) => {
            password = bcrypt.compareSync(req.body.password, result1.password);
            if (error1) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);

            } else if (result1 && password) {
                responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'You cannot use the existing password', result1);
            } else {
                let obj = {
                    'password': req.body.password,
                    'first_login': '0'
                }
                let myquery = {
                    _id: new ObjectId(req.body.id)
                };
                let newvalues = {
                    $set: obj
                }
                adminService.updateDetails(myquery, newvalues, {
                    new: true
                }, (error_, userupdate) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Password Updated successfully!', userupdate);
                    }
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}
/**
 * [get User Data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getUserData = (req, res) => {
    let check = helper.checkRequest(["decoded"], req.body);
    if (check == true) {
        let admin_email = req.body.decoded.email.toLowerCase();
        adminService.findData({ 'email': admin_email }, (error1, userInfo) => {
            if (error1) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);
            } else {
                if(userInfo){
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin detail fetch successfully', userInfo);
                }else{
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Admin not found');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}
/**
 * [User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminUserImageUpload = (req, res) => {
        let check = helper.checkRequest(["id", "profilePic"], req.body);
        if (check == true) {
            buf = Buffer.from(req.body.profilePic.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            let number = Math.random() * (999999 - 10000) + 10000
            let resData = {
                Key: req.body.id + number,
                Body: buf,
                ContentEncoding: 'base64',
                ContentType: 'image/jpeg',
                ACL: 'public-read'
            };
            s3Bucket.upload(resData, function(err, data) {
                if (err) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    let myquery = {
                        admin_id: req.body.id
                    };
                    let imageValue = {
                        $set: {
                            profile_image: data.Location,
                        }
                    }
                    adminService.updateOne(myquery, imageValue, {
                        new: true
                    }, (error_, userupdate) => {
                        if (error_) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            adminService.findDataById(myquery,(err, resData_) => {
                                if(err)
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                else
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User image uploaded successfully', resData_);
                            });
                        }
                    });
                }
            });
        } else {
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
        }

    }
/**
 * [Update Password of Admin Users]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminChangePassword = (req, res) => {
    let check = helper.checkRequest(["decoded", 'current_password', 'password'], req.body);
    let password;
    let admin_email = req.body.decoded.email.toLowerCase();
    if (check == true) {
        adminService.findData({ 'email': admin_email }, (error1, result1) => {
            if (error1) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);

            } else if (result1){
                password = bcrypt.compareSync(req.body.current_password, result1.password);
            }
            if (password) {
                let obj = {
                    'password': req.body.password,
                }
                let myquery = {
                    _id: new ObjectId(req.body.id)
                };
                let newvalues = {
                    $set: obj
                }
                adminService.updateDetails(myquery, newvalues, {
                    new: true
                }, (error_, userupdate) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Password Updated successfully!', userupdate);
                    }
                });
            } else {
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Current password does not match', result1);
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}
/**
 * [Admin list]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminList = (req, res) => {
        adminService.getData({ 'role': { '$in': ['admin', 'superadmin'] } }, (error, result) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (result) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin List!', result);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Data not found!');
                }
            }
        });
    }


// Test api to hash password
const hashAdminPassword = async (req, res) => {
    var result;
    var password;
        let query = {
            $where: 'this.password.length != 60'
        }
        let i =0;
        try {
            result = await adminModel.find(query);
            console.log(result.length,'total users')
            result.forEach(async (admin) => {
                password = admin.password;
                var salt = bcrypt.genSaltSync(10);
                var hash = bcrypt.hashSync(password, salt);
                password =  hash;

                try {
                    adminModel.findByIdAndUpdate({_id : admin._id}, {$set : {'password' : password}}, (err, result) => {
                        console.log(i++);
                        console.log(err, "error");
                    });
                } catch (error) {
                    console.log("Errrorrr");
                    console.log(error);
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error)
                }
             })
        } catch (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error)
    }
}

// Test Api to create super admin
const superAdmin = (req, res) => {
    let check = helper.checkRequest(["email", "password", "name", "last_name"], req.body);
    let randomNumber = helper.generateRandomString();
    if(check) {
        let obj = {
            "admin_id": randomNumber,
            "email" : req.body.email,
            "password" : req.body.password,
            "name" : req.body.name,
            "last_name" : req.body.last_name,
            "role" : "superadmin",
            "status" : 1
        }
        adminService.findData({'email' : req.body.email}, (err, data) => {
            if(err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if (data){
                responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Record Already Exists', err);
            } else {
                adminService.createData(obj, (err, result) => {
                    if(err) {
                       return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else {
                        if(result) {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, "Super Admin created successfully!", result)
                        }
                    }
                })
            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

//Api for super admin to change password of admin
const adminResetPassword = (req, res) => {
    let check = helper.checkRequest(["admin_id", "new_password", "re_password"], req.body);
    if(check) {
    let admin_id = req.body.admin_id;
    let new_password = req.body.new_password;
    let confirm_password = req.body.re_password;
    let user = req.body.decoded;
        if(new_password !== confirm_password) {
            return responseHandle.sendResponsewithError(res, responseCode.NOT_MODIFIED, 'Both password must be same');
        }
        adminService.findData({'email' : user.email.toLowerCase()},(err, result) => {
            if(err) {
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if(result){
                adminService.findData({'admin_id' : admin_id},(errData, resultData) => {
                    if(errData) return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errData);
                    else if(resultData){
                        bcrypt.compare(new_password, resultData.password, (errPass, resultPass) => {
                            if(errPass) return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errPass);
                            else if(resultPass) return responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'New password can not be same as old password');
                            else{
                                if(result.role === 'superadmin' || (result.role_permission && result.role_permission === 'it_admin')) {
                                    let query = {
                                        'admin_id' : admin_id
                                    }
                                    let newvalues = {
                                        $set : {'password' : new_password}
                                    }
                                    adminService.updateOne(query,newvalues, (err, result_) => {
                                        if(err) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                        } else if(result_){
                                            var message = {
                                                "html": "<p>Hi " + result_.name +
                                                ",</p><p>Your password has been changed by " + user.email.toLowerCase() + "<br/></p>",
                                                "subject": "Reset Password",
                                                "from_email": "team@eleve.co.in",
                                                "from_name": "Eleve",
                                                "to": [{
                                                    "email": result_.email,
                                                    "type": "to"
                                                }]
                                            };
                                            helper.sendEmail(message);
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Password reset successfully!');
                                        }
                                    })
                                } else {
                                    responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, 'You are not authorized to change this password', err);
                                }
                            }
                        });
                    }else{
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                    }
                });
            } else {
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!', err);
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/* Export apis */
module.exports = {
    usersList,
    adminLogin,
    createAdmin,
    updateStatus,
    adminUpdate,
    adminPasswordUpdate,
    getUserData,
    adminUserImageUpload,
    adminChangePassword,
    adminList,
    hashAdminPassword,
    superAdmin,
    adminResetPassword
}