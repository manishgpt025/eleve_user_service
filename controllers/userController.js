const userModel = require('../models/userModel.js');// user model
const userService = require('../services/userServices.js');// user services
const helper = require('../globalFunctions/function.js');//use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
// Obj ID
const ObjectId = require('mongodb').ObjectID;
var randomstring = require("randomstring");
const webPush = require('web-push');
//bcrypt
const bcrypt = require('bcryptjs');


const publicVapidKey = process.env.PUBLIC_VAPID_KEY;
const privateVapidKey = process.env.PRIVATE_VAPID_KEY;

webPush.setVapidDetails('mailto:mohan@eleve.co.in', publicVapidKey, privateVapidKey);

/**
 * [Login API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const login = (req, res) => {
    let check = helper.checkRequest(["email", "password"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.findData({ email: req.body.email}, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if (userInfo) {
                bcrypt.compare(req.body.password, userInfo.password, (err, result1) => {
                    if (result1) {
                        if(userInfo.status && userInfo.status == '2'){
                            responseHandle.sendResponsewithError(res, responseCode.FORBIDDEN, 'User is barred.');
                        }else{
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User found!', userInfo);
                        }
                    } else {
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                    }
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Logout API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const logout = (req, res) => {
    console.log(req.body);
    let check = helper.checkRequest(["user_id"], req.body);
    if (check == true) {
        // Check from DB Collection
        let myquery = {
            _id: new ObjectId(req.body.user_id)
        };
        userService.findData(myquery, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    // Remove JWT token here

                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User logout!');
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Sign-up API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const postUserdata = (req, res) => {
    // check for empty data
    let check = helper.checkRequest(["first_name", "email", "mobile", "password"], req.body);

    // let checkMobile = helper.checkData("mobile", req.body.mobile);


    if (check == true) {
        userModel.findOne({ email: req.body.email }, (err, emailData) => {
            if (err) {
                responseHandle.sendResponsewithError(res, 500, err);
            } else if (emailData) {
                responseHandle.sendResponsewithError(res, 500, "Email already exist.", err);
            } else {
                userModel.findOne({ mobile: req.body.mobile }, (err, mobileData) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, 500, err);
                    } else if (mobileData) {
                        responseHandle.sendResponsewithError(res, 500, "Mobile number already exist.", err);
                    } else {
                        let newId = helper.generateRandomString();
                        let obj = new userModel({
                                "influencer_id": newId,
                                "first_name": req.body.first_name,
                                "last_name": req.body.last_name,
                                "email": req.body.email,
                                "mobile": req.body.mobile,
                                "password": req.body.password,
                                "registered": "1",
                                "status": ""
                            })
                        let token = '';
                        obj.save((error_, result_) => {
                            if (error_) {
                                responseHandle.sendResponsewithError(res, 500, error_);
                            } else {

                                responseHandle.sendResponseWithData(res, 200, 'Register Successfully!', result_, token);
                            }
                        })
                    }
                });
            }
        });

    } else {
        responseHandle.sendResponsewithError(res, 404, '${check} key is missing.');
    }
}


/**
 * [Sign-up API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const postSubscribeData = (req, res) => {
    console.log(req.body);

    const subscription = req.body

      res.status(201).json({});

      // create payload
      const payload = JSON.stringify({
        title: 'Invitation pending.',
        body: 'New campaign invitation pending',
      });

      webPush.sendNotification(subscription, payload)
        .catch(error => console.error(error));

    // return responseHandle.sendResponseWithData(res, 200, 'Register Successfully!');
    //     userModel.findOne({ email: req.body.email }, (err, emailData) => {
    //         if (err) {
    //             responseHandle.sendResponsewithError(res, 500, err);
    //         } else if (emailData) {
    //             responseHandle.sendResponsewithError(res, 500, "Email already exist.", err);
    //         } else {
    //             userModel.findOne({ mobile: req.body.mobile }, (err, mobileData) => {
    //                 if (err) {
    //                     responseHandle.sendResponsewithError(res, 500, err);
    //                 } else if (mobileData) {
    //                     responseHandle.sendResponsewithError(res, 500, "Mobile number already exist.", err);
    //                 } else {
    //                     let newId = helper.generateRandomString();
    //                     let obj = new userModel({
    //                             "influencer_id": newId,
    //                             "first_name": req.body.first_name,
    //                             "last_name": req.body.last_name,
    //                             "email": req.body.email,
    //                             "mobile": req.body.mobile,
    //                             "password": req.body.password,
    //                             "registered": "1",
    //                             "status": ""
    //                         })
    //                     let token = '';
    //                     obj.save((error_, result_) => {
    //                         if (error_) {
    //                             responseHandle.sendResponsewithError(res, 500, error_);
    //                         } else {

    //                             responseHandle.sendResponseWithData(res, 200, 'Register Successfully!', result_, token);
    //                         }
    //                     })
    //                 }
    //             });
    //         }
    //     });
}

// Test api to hash password
const hashInfluencerPassword = async (req, res) => {
    var result;
    var password;
        let query = {
            'password' : {$exists : true},
            'registered' : '1',
            $where: 'this.password.length != 60'
        }
        let i = 0;
        try {
            result = await userModel.find(query);
            console.log(result.length);
            result.forEach(async(influencer) => {
                password = influencer.password;
                console.log(i++)
                console.log(influencer.email)
                console.log(password), 'old--';
                var salt = bcrypt.genSaltSync(10);
                var hash = bcrypt.hashSync(password, salt);
                password =  hash;
                console.log(password);
                try {
                    await userModel.findByIdAndUpdate({_id : influencer._id}, {$set : {'password' : password}});
                } catch (error) {
                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error)
                }
            })
            console.log('password hashing completed!');
        } catch (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error)
    }
}

/*Export apis*/
module.exports = {
    postUserdata: postUserdata,
    postSubscribeData : postSubscribeData,
    login: login,
    logout: logout,
    hashInfluencerPassword
}