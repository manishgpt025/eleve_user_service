//use helper
const helper = require('../globalFunctions/function.js');
const request = require('request-promise');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
var { google } = require('googleapis');
var OAuth2 = google.auth.OAuth2;
// user services
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;

const oauth2Client = new OAuth2(
    process.env.OAUTH2_CLIENT_ID,
    process.env.OAUTH2_CLIENT_SECRET,
    "https://dev.eleveglobal.com/youtube/callback"
);

/*create apis*/
/**
 * [Youtube authenciate api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const youtubeLogin = function(req, res) {
    var htmlString = "<html><head><script language='javascript'>function redirect(){window.opener.location.reload();self.close();}</script></head><body><script language='javascript'>redirect();</script></body></html>";
    if (!req.query.code && !req.body.code) {
        var scopes = ['https://www.googleapis.com/auth/youtube.readonly', 'https://www.googleapis.com/auth/youtube.force-ssl', 'https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/userinfo.profile'];
        let loginOptions = {
            access_type: 'offline',
            scope: scopes,
            prompt: 'consent'
        };
        let google_login_url = oauth2Client.generateAuthUrl(loginOptions);
        return res.status(200).send({ google_login_url });
    } else if (req.query.error) {
        res.status(200);
        res.send(htmlString);
        return;
    }

}
/**
 * [Youtube callback api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const YoutubeCallBack = function(req, res) {
    let code = req.body.code;
    let user_email = req.body.decoded.email.toLowerCase();
    var params = {
        code: req.body.code,
        client_id: process.env.OAUTH2_CLIENT_ID,
        client_secret: process.env.OAUTH2_CLIENT_SECRET,
        grant_type: 'authorization_code'
    };
    let userInfoUrl = 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&oauth_token=';
    var youtubeData = {};
    oauth2Client.getToken(code)
        .then(function(response, err) {
            const { tokens } = response.tokens;
            let google_tokens = response.tokens;
            oauth2Client.setCredentials(google_tokens);
            var params = {
                mine: 'true',
                part: 'snippet,contentDetails,statistics',
            };
            var youtube = google.youtube({
                version: 'v3',
                auth: oauth2Client
            });
            youtube.channels.list(params, (err, response) => {
                // if (err) {
                //     return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                // }
                userInfoUrl = userInfoUrl + google_tokens.access_token;
                request.get(userInfoUrl, (err, result, profile) => {
                    profile = JSON.parse(profile);
                    //profile.youtube = response.data.items;
                    youtubeData.youtube = profile.youtube
                    youtubeData.oauth_token = google_tokens;
                    youtubeData.id = profile.id;
                    youtubeData.name = profile.name;
                    youtubeData.Elv_social = profile.name;
                    youtubeData.given_name = profile.given_name;
                    youtubeData.family_name = profile.family_name;
                    youtubeData.profile_image_url = profile.picture;
                    youtubeData.connection_type = "auto";
                    youtubeData.category = "";
                    youtubeData.genres = [];

                    let myquery = {
                        email: { $ne:user_email},
                        'youtube.id': profile.id
                    };
                    userService.findData(myquery, (err_, userInfo) => {
                        if (err_) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err_);
                        } else {
                            if (userInfo) {
                                responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Please try with diffrent account, this account is already exists.');
                            } else {
                                // Saving data
                                let query = {
                                    email: user_email,
                                    'youtube.id': profile.id
                                };
                                userService.findData(query, (error_, userInfo1) => {
                                    if (error_) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        if (userInfo1) {
                                            responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'You already have this account mentioned on your profile');
                                        } else {
                                            var query1 = {
                                                email: user_email
                                            }
                                            var data = {
                                                $push: {
                                                    youtube: youtubeData
                                                }
                                            }
                                            userService.updateOne(query1, data, {
                                                new: true
                                            }, (error, updatedResp) => {
                                                if (error) {
                                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                } else {
                                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User linked in account added Successfully!', updatedResp);

                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                });
            });
        });
}

/*Export apis*/
module.exports = {
    youtubeLogin,
    YoutubeCallBack

}