const helper = require('../globalFunctions/function.js');//use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
//bcrypt
const bcrypt = require('bcryptjs');
// user services
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;

// check current password
const checkcurrentpassword = (req, res) => {
    userService.findData({ _id: new ObjectId(req.body.user_id) /* password: req.body.password*/ }, (error_, userInfo) => {
        if (error_) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
        } else if (userInfo){
            bcrypt.compare(req.body.password, userInfo.password, (err, result) => {
                if (result) {
                    responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, 'Password Match');
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.EVERYTHING_IS_OK, 'Password  not Match');
                }
            })
        }
    });
}

/**
 * [Change Password]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const changePassword = (req, res) => {
    let check = helper.checkRequest(["user_id", "old_password", "new_password"], req.body);
    let password;
    if (check == true) {
        // Check from DB Collection
        userService.findData({
            _id: new ObjectId(req.body.user_id)
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    password = bcrypt.compareSync(req.body.old_password, userInfo.password);
                    if (!password) {
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Current password entered is wrong.');
                    } else if (req.body.old_password == req.body.new_password) {
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'New password must be different from the old password');
                    } else {
                        let myquery = { _id: new ObjectId(req.body.user_id) };
                        let newvalues = { $set: { password: req.body.new_password } }
                        userService.updateOne(myquery, newvalues, { new: true }, (error, userupdate) => {
                            if (error) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Your password has been changed', userupdate);
                            }
                        });
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/*Export apis*/
module.exports = {
    checkcurrentpassword,
    changePassword
}