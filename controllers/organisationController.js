const organisationService = require('../services/organisationService.js');// organisation services
const advertiserService = require('../services/advertiserService.js');// advertiser Services
const helper = require('../globalFunctions/function.js');//use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const ObjectId = require('mongodb').ObjectID;// Obj ID

/**
 * [Organisation List ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const organisationList = (req, res) => {
    let search_keyword = req.body.keyword;
    let org_filter = req.body.filter;
    let org_sort = req.body.sort;
    if (org_filter == "0") {
        var filter = ['brand'];
    } else if (org_filter == "1") {
        var filter = ['agency'];
    } else {
        var filter = ["", "brand", "agency"];
    }
    let bodyData = {
        search: search_keyword,
        status: filter
    }
    if (org_sort == "0") {
        var sort = { "organisation_name": -1 };
    } else if (org_sort == "1") {
        var sort = { "organisation_name": 1 };
    } else {
        var sort = { "_id": -1 };
    }
    let options = {
        page: req.body.pageNumber || 1,
        limit: req.body.limit || 10,
    }
    organisationService.getPaginateDataWithAggregate(bodyData, options, sort, (error, organisationInfo, pages, total) => {
        if (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
        } else {
            if (organisationInfo) {
                let data = {
                    docs: organisationInfo,
                    page: req.body.pageNumber || 1,
                    limit: req.body.limit || 10,
                    pages: pages,
                    total: total
                }
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Organisation List!', data);
            } else {
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Data not found!');
            }
        }
    });
}

/**
 * [Create Organisation API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const postOrganisationdata = (req, res) => {
    // check for empty data
    let check = helper.checkRequest(["organisation_name", "type", "social_media", "is_asscoiated"], req.body);
    if (check == true) {
        organisationService.findData({ organisation_name: req.body.organisation_name.toLowerCase() }, (err, organisationData) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if (organisationData) {
                responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'This name already exist in the database as an ' + organisationData.type, organisationData);
            } else {
                if (req.body.associated_brands == undefined) {
                    req.body.associated_brands = [];
                }
                if (req.body.addresses == undefined) {
                    req.body.addresses = [];
                }
                let newId = helper.generateRandomString();
                let obj = {
                    "organisation_id":newId,
                    "organisation_name": req.body.organisation_name.toLowerCase(),
                    "type": req.body.type,
                    "social_media": req.body.social_media,
                    "associated_brands": req.body.associated_brands,
                    "is_asscoiated": req.body.is_asscoiated,
                    "addresses": req.body.addresses
                }
                organisationService.createData(obj, (error_, result) => {
                    if (error_) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Organisation created Successfully!', result);
                    }
                })
            }
        });
    } else
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
}

/**
 * [update Organisation Location]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateOrganisationAddresses = (req, res) => {
    let check = helper.checkRequest(["id"], req.body);
    if (check == true) {
        // Check from DB Collection
        organisationService.findData({
            _id: req.body.id
        }, (err, organisationInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (organisationInfo) {
                    let myquery = { _id: new ObjectId(req.body.id) };
                    let newvalues = { $push: { addresses: req.body.addresses } }
                    organisationService.updateOne(myquery, newvalues, { new: true }, (error, addressupdate) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Address is added in the Organisation', addressupdate);
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Organisation not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [update Organisation]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateOrganisation = (req, res) => {
    let check = helper.checkRequest(["id"], req.body);
    if (check == true) {
        // Check from DB Collection
        organisationService.findData({
            _id: req.body.id
        }, (err, organisationData) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (organisationData) {
                    if (req.body.type == undefined) {
                        req.body.type = organisationData.type;
                    }
                    if (req.body.social_media == undefined) {
                        req.body.social_media = organisationData.social_media;
                    }
                    if (req.body.associated_brands == undefined) {
                        req.body.associated_brands = organisationData.associated_brands;
                    }
                    if (req.body.addresses == undefined) {
                        req.body.addresses = organisationData.addresses;
                    }
                    if (req.body.is_asscoiated == undefined) {
                        req.body.is_asscoiated = organisationData.is_asscoiated;
                    }
                    let myquery = { _id: organisationData._id };
                    let newvalues = {
                        $set: {
                            organisation_name: req.body.organisation_name.toLowerCase(),
                            type: req.body.type,
                            social_media: req.body.social_media,
                            associated_brands: req.body.associated_brands,
                            is_asscoiated: req.body.is_asscoiated,
                            addresses: req.body.addresses
                        }
                    };
                    organisationService.updateOne(myquery, newvalues, { new: true }, (error, organisationDataupdate) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Organisation updated', organisationDataupdate);
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Organisation not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Get Organisation Data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const details = (req, res) => {
    let check = helper.checkRequest(["id"], req.body);
    if (check == true) {
        // Check from DB Collection
        organisationService.findData({ _id: new ObjectId(req.body.id) }, (err, result_) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (result_) {
                    advertiserService.getData({ organisation_id: req.body.id }, (err1, result1) => {
                        if (err1) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err1);
                        } else if (result1) {
                            let resultData = {
                                "organisationInfo": result_,
                                "advertiserInfo": result1,
                            }
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Organisation and advertiser found', resultData);

                        } else {
                            let resultData = {
                                "organisationInfo": result_
                            }
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Organisation results found', resultData);
                        }
                    })
                }
            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Get Organisation Data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getOrganisationdata = (req, res) => {
    // check for empty data
    let check = helper.checkRequest(["organisation_name"], req.body);
    if (check == true) {
        organisationService.findData({ organisation_name: req.body.organisation_name.toLowerCase() }, (err, organisationData) => {
            if (err) {
                responseHandle.sendResponsewithError(res, 500, err);
            } else {
                if (organisationData) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Organisation fetch successfully', organisationData);
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `Organisation not found`);
                }
            }
        });
    } else
       responseHandle.sendResponsewithError(res, 404, `${check} key is missing.`);
}


/*Export apis*/
module.exports = {
    organisationList,
    postOrganisationdata,
    updateOrganisationAddresses,
    updateOrganisation,
    details,
    getOrganisationdata

}