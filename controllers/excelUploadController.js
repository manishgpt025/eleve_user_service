const userService = require('../services/userServices.js');// user services
const helper = require('../globalFunctions/function.js');//use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');

/**
 * [Upload excel ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const uploadExcel = (req, res) => {
        let check = helper.checkRequest(["data"], req.body);
        if (check == true) {
            let data = req.body.data;
            // loop here
            data.map(obj => {
                let facebook = [];
                if (obj.facebook_1_name) {
                    facebook.push({
                        'name': obj.facebook_1_name,
                        'category': obj.facebook_1_category,
                        'followers_count': obj.facebook_1_followers_count,
                        'link': obj.facebook_1_link,
                        'genres': obj.facebook_1_genre.split(','),
                        'bio': obj.facebook_1_bio,
                        'type': obj.facebook_1_type,
                        'connection_type': obj.facebook_1_connection_type,
                        'currency': obj.facebook_1_currency,
                        'cost': obj.facebook_1_cost,
                        'text': obj.facebook_1_text,
                        'image': obj.facebook_1_image,
                        'video': obj.facebook_1_video,
                        'story': obj.facebook_1_story,
                        'Elv_social': obj.facebook_1_name,
                        'Elv_name': obj.facebook_1_name
                    })
                }
                if (obj.facebook_2_name) {
                    facebook.push({
                        'name': obj.facebook_2_name,
                        'category': obj.facebook_2_category,
                        'followers_count': obj.facebook_2_followers_count,
                        'link': obj.facebook_2_link,
                        'genres': obj.facebook_2_genre.split(','),
                        'bio': obj.facebook_2_bio,
                        'type': obj.facebook_2_type,
                        'connection_type': obj.facebook_2_connection_type,
                        'currency': obj.facebook_2_currency,
                        'cost': obj.facebook_2_cost,
                        'text': obj.facebook_2_text,
                        'image': obj.facebook_2_image,
                        'video': obj.facebook_2_video,
                        'story': obj.facebook_2_story,
                        'Elv_social': obj.facebook_2_name,
                        'Elv_name': obj.facebook_2_name
                    })
                }
                let twitter = [];
                if (obj.twitter_1_name) {
                    twitter.push({
                        'name': obj.twitter_1_name,
                        'category': obj.twitter_1_category,
                        'followers_count': obj.twitter_1_followers_count,
                        'link': obj.twitter_1_link,
                        'genres': obj.twitter_1_genre.split(','),
                        'bio': obj.twitter_1_bio,
                        'connection_type': obj.twitter_1_connection_type,
                        'currency': obj.twitter_1_currency,
                        'cost': obj.twitter_1_cost,
                        'text': obj.twitter_1_text,
                        'image': obj.twitter_1_image,
                        'video': obj.twitter_1_video,
                        'Elv_social': obj.twitter_1_name,
                        'Elv_name': obj.twitter_1_name
                    })
                }
                if (obj.twitter_2_name) {
                    twitter.push({
                        'name': obj.twitter_2_name,
                        'category': obj.twitter_2_category,
                        'followers_count': obj.twitter_2_followers_count,
                        'link': obj.twitter_2_link,
                        'genres': obj.twitter_2_genre.split(','),
                        'bio': obj.twitter_2_bio,
                        'connection_type': obj.twitter_2_connection_type,
                        'currency': obj.twitter_2_currency,
                        'cost': obj.twitter_2_cost,
                        'text': obj.twitter_2_text,
                        'image': obj.twitter_2_image,
                        'video': obj.twitter_2_video,
                        'Elv_social': obj.twitter_2_name,
                        'Elv_name': obj.twitter_2_name
                    })
                }
                let instagram = [];
                if (obj.instagram_1_name) {
                    instagram.push({
                        'name': obj.instagram_1_name,
                        'category': obj.instagram_1_category,
                        'followers_count': obj.instagram_1_followers_count,
                        'link': obj.instagram_1_link,
                        'genres': obj.instagram_1_genre.split(','),
                        'bio': obj.instagram_1_bio,
                        'connection_type': obj.instagram_1_connection_type,
                        'cost': obj.instagram_1_cost,
                        'currency': obj.instagram_1_currency,
                        'igtv': obj.instagram_1_igtv,
                        'image': obj.instagram_1_image,
                        'video': obj.instagram_1_video,
                        'story': obj.instagram_1_story,
                        'Elv_social': obj.instagram_1_name,
                        'Elv_name': obj.instagram_1_name
                    })
                }
                if (obj.instagram_2_name) {
                    instagram.push({
                        'name': obj.instagram_2_name,
                        'category': obj.instagram_2_category,
                        'followers_count': obj.instagram_2_followers_count,
                        'link': obj.instagram_2_link,
                        'genres': obj.instagram_2_genre.split(','),
                        'bio': obj.instagram_2_bio,
                        'connection_type': obj.instagram_2_connection_type,
                        'currency': obj.instagram_2_currency,
                        'cost': obj.instagram_2_cost,
                        'igtv': obj.instagram_2_igtv,
                        'image': obj.instagram_2_image,
                        'video': obj.instagram_2_video,
                        'story': obj.instagram_2_story,
                        'Elv_social': obj.instagram_2_name,
                        'Elv_name': obj.instagram_2_name
                    })
                }
                let youtube = [];
                if (obj.youtube_1_name) {
                    youtube.push({
                        'name': obj.youtube_1_name,
                        'category': obj.youtube_1_category,
                        'followers_count': obj.youtube_1_followers_count,
                        'link': obj.youtube_1_link,
                        'genres': obj.youtube_1_genre.split(','),
                        'bio': obj.youtube_1_bio,
                        'connection_type': obj.youtube_1_connection_type,
                        'currency': obj.youtube_1_currency,
                        'cost': obj.youtube_1_cost,
                        'Elv_social': obj.youtube_1_name,
                        'Elv_name': obj.youtube_1_name
                    })
                }
                if (obj.youtube_2_name) {
                    youtube.push({
                        'name': obj.youtube_2_name,
                        'category': obj.youtube_2_category,
                        'followers_count': obj.youtube_2_followers_count,
                        'link': obj.youtube_2_link,
                        'genres': obj.youtube_2_genre.split(','),
                        'bio': obj.youtube_2_bio,
                        'connection_type': obj.youtube_2_connection_type,
                        'currency': obj.youtube_2_currency,
                        'cost': obj.youtube_2_cost,
                        'Elv_social': obj.youtube_2_name,
                        'Elv_name': obj.youtube_2_name
                    })
                }
                let blog = [];
                if (obj.blog_1_name) {
                    blog.push({
                        'name': obj.blog_1_name,
                        'category': obj.blog_1_category,
                        'followers_count': obj.blog_1_followers_count,
                        'link': obj.blog_1_link,
                        'genres': obj.blog_1_genre.split(','),
                        'bio': obj.blog_1_bio,
                        'connection_type': obj.blog_1_connection_type,
                        'currency': obj.blog_1_currency,
                        'cost': obj.blog_1_cost,
                        'Elv_social': obj.blog_1_name,
                        'Elv_name': obj.blog_1_name
                    })
                }
                if (obj.blog_2_name) {
                    blog.push({
                        'name': obj.blog_2_name,
                        'category': obj.blog_2_category,
                        'followers_count': obj.blog_2_followers_count,
                        'link': obj.blog_2_link,
                        'genres': obj.blog_2_genre.split(','),
                        'bio': obj.blog_2_bio,
                        'connection_type': obj.blog_2_connection_type,
                        'currency': obj.blog_2_currency,
                        'cost': obj.blog_2_cost,
                        'Elv_social': obj.blog_2_name,
                        'Elv_name': obj.blog_2_name
                    })
                }
                let snapchat = [];
                if (obj.snapchat_1_name) {
                    snapchat.push({
                        'name': obj.snapchat_1_name,
                        'category': obj.snapchat_1_category,
                        'followers_count': obj.snapchat_1_followers_count,
                        'link': obj.snapchat_1_link,
                        'genres': obj.snapchat_1_genre.split(','),
                        'bio': obj.snapchat_1_bio,
                        'connection_type': obj.snapchat_1_connection_type,
                        'currency': obj.snapchat_1_currency,
                        'cost': obj.snapchat_1_cost,
                        'Elv_social': obj.snapchat_1_name,
                        'Elv_name': obj.snapchat_1_name
                    })
                }
                if (obj.snapchat_2_name) {
                    snapchat.push({
                        'name': obj.snapchat_2_name,
                        'category': obj.snapchat_2_category,
                        'followers_count': obj.snapchat_2_followers_count,
                        'link': obj.snapchat_2_link,
                        'genres': obj.snapchat_2_genre.split(','),
                        'bio': obj.snapchat_2_bio,
                        'connection_type': obj.snapchat_2_connection_type,
                        'currency': obj.snapchat_2_currency,
                        'cost': obj.snapchat_2_cost,
                        'Elv_social': obj.snapchat_2_name,
                        'Elv_name': obj.snapchat_2_name
                    })
                }
                let tiktok = [];
                if (obj.tiktok_1_name) {
                    tiktok.push({
                        'name': obj.tiktok_1_name,
                        'category': obj.tiktok_1_category,
                        'followers_count': obj.tiktok_1_followers_count,
                        'link': obj.tiktok_1_link,
                        'genres': obj.tiktok_1_genre.split(','),
                        'bio': obj.tiktok_1_bio,
                        'connection_type': obj.tiktok_1_connection_type,
                        'currency': obj.tiktok_1_currency,
                        'cost': obj.tiktok_1_cost,

                        'Elv_social': obj.tiktok_1_name,
                        'Elv_name': obj.tiktok_1_name
                    })
                }
                if (obj.tiktok_2_name) {
                    tiktok.push({
                        'name': obj.tiktok_2_name,
                        'category': obj.tiktok_2_category,
                        'followers_count': obj.tiktok_2_followers_count,
                        'link': obj.tiktok_2_link,
                        'genres': obj.tiktok_2_genre.split(','),
                        'bio': obj.tiktok_2_bio,
                        'connection_type': obj.tiktok_2_connection_type,
                        'currency': obj.tiktok_2_currency,
                        'cost': obj.tiktok_2_cost,

                        'Elv_social': obj.tiktok_2_name,
                        'Elv_name': obj.tiktok_2_name
                    })
                }
                let linkedin = [];
                let pinterest = [];
                let instagram_business = [];
                let newId = helper.generateRandomString();
                let final_json = {
                        'influencer_id': newId,
                        'first_name': obj.first_name,
                        'last_name': obj.last_name,
                        'gender': obj.gender,
                        'email': obj.email_id,
                        'country': obj.country,
                        'state': obj.state,
                        'city': obj.city,
                        'address': obj.address,
                        'isd_code': obj.isd_code,
                        'mobile': obj.mobile,
                        "email_valid": "0",
                        "alt_isd_code": "",
                        "alt_mobile": "",
                        "profile_pic": "",
                        "alt_email": "",
                        "note": "",
                        "facebook": facebook,
                        "blog": blog,
                        "youtube": youtube,
                        "instagram": instagram,
                        "twitter": twitter,
                        "snapchat": snapchat,
                        "tiktok": tiktok,
                        "linkedin": linkedin,
                        "pinterest": pinterest,
                        "instagram_business": instagram_business,
                        "language_1": obj.language_1,
                        "language_2": obj.language_2,
                        "language_3": obj.language_3,
                        "language_4": obj.language_4,
                        'registered': "1",
                        "status": "1",
                        "excel_upload": "1"
                    }
                userService.createData(final_json, function(error, result) {
                    if (error) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);

                    } else {
                        console.log('save---', result.email);
                    }
                });
            });
        } else {
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
        }
 };
    /*Export apis*/
module.exports = {
    uploadExcel
}