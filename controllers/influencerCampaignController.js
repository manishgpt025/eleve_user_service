const userService = require('../services/userServices.js'); // user services
const adminService = require('../services/adminService.js');
const campaignInfluencerService = require('../services/campaignInfluencerServices.js');
var Twitter = require('twitter'); // Packages
const helper = require('../globalFunctions/function.js'); //use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const ObjectId = require('mongodb').ObjectID; // Obj ID
const image2base64 = require('image-to-base64');

/**
 * [Get Influencer Campaign New/Accepted List ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getInfluencerCampaignList = (req, res) => {
    let check = helper.checkRequest(['decoded'], req.body);
    if (check == true) {
        // Check user from influencer collection
        user_email = req.body.decoded.email.toLowerCase();
        userService.getData({
            email: user_email
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    //Campaign Status
                    if(req.body.status)
                        var camapign_status = { "$match": { 'status': '6' } }
                    else
                        var camapign_status = { "$match": { 'status': '3' } }
                    let bodyData = {
                        influencer_id: userInfo._id,
                        camapign_status,
                        completeCamapign:req.body.status
                    }
                    let options = {
                        page: req.body.pageNumber || 1,
                        limit: req.body.limit || 6,
                    }
                    let sort = { "$sort": { "_id": -1 } };
                    campaignInfluencerService.getInfluencerCampaignList(bodyData, options, sort, (error, campaignInfo,pages=null,total=null) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            if (campaignInfo) {
                                if(req.body.status){
                                    let data = {
                                        docs: campaignInfo,
                                        page: req.body.pageNumber || 1,
                                        limit: req.body.limit || 6,
                                        pages: pages,
                                        total: total
                                    }
                                   return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List!', data);
                                }else{
                                    let data = {
                                        docs: campaignInfo,
                                        total: campaignInfo.length
                                    }
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List!', data);
                                }
                            } else {
                                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Campaign not found!');
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [Get Influencer Campaign Details ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignDetail = (req, res) => {
    let check = helper.checkRequest(['user_id', 'campaign_id'], req.body);
    if (check == true) {
        let user_id = req.body.user_id;
        let campaign_id = req.body.campaign_id;
        // Set data for the query
        let bodyData = {
            influencer_id: new ObjectId(user_id),
            campaign_id: new ObjectId(campaign_id),
            campaignStatus: '3'
        }
        campaignInfluencerService.getCampaignNewAcceptedData(bodyData, (error, campaignInfo) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (campaignInfo.length > 0) {
                    if (campaignInfo[0].created_id) {
                        adminService.getData({ '_id': { '$in': [campaignInfo[0].created_id, '5783610d8ead0ef2622d4ac1'] } }, { 'name': 1, 'email': 1, 'mobile': 1, 'isd': 1, 'profile_image': 1 }, (err, userInfo) => {
                            if (err) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                            } else {
                                campaignInfo[0]['contact'] = userInfo;
                                let data = {
                                    campaignInfo :  campaignInfo[0]
                                }
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List!', data);
                            }
                        })
                    } else {
                        campaignInfo[0]['contact'] = [];
                        let data = {
                            campaignInfo :  campaignInfo[0]
                        }
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List!', data);
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Campaign not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Request not complete.');
    }
};

/**
 * [Get Influencer Campaign Accepted List ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getCampaignCompletedList = (req, res) => {
    let check = helper.checkRequest(['user_id'], req.body);
    if (check == true) {
        let search_keyword = req.body.keyword || "";
        var sort = {};
        // Set data for the query
        let bodyData = {
            influencerId: { "$match": { "influencer_id": new ObjectId(req.body.user_id) } },
            search: search_keyword,
            camapign_status: { "$match": { "campaign_details.status": { $in: ['4', '6'] } } }
        }
        var sort = { "$sort": { "_id": -1 } };
        let options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 6,
            }
            // Check user from influencer collection
        userService.getData({
            _id: req.body.user_id
        }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    campaignInfluencerService.getPaginateCompletedDataWithAggregate(bodyData, options, sort, (error, campaignInfo, pages, total) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            if (campaignInfo) {
                                let data = {
                                    docs: campaignInfo,
                                    page: req.body.pageNumber || 1,
                                    limit: req.body.limit || 6,
                                    pages: pages,
                                    total: total
                                }
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List!', data);
                            } else {
                                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Campaign not found!');
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Influencer Campaign Accepted ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const actionInfluencerCampaign = (req, res) => {

    let check = helper.checkRequest(['user_id','campaign_influencer_ids'], req.body);
    if (check == true) {
        let campaign_influencer_ids = req.body.campaign_influencer_ids;
        let query = {
            'campaign_id': new ObjectId(req.body.campaign_id),
            'campaign_influencer_id': { $in: campaign_influencer_ids }
        }

        let bodyData = { $set: { 'action': req.body.action } }
        campaignInfluencerService.updateDetails(query, bodyData, { safe: true }, (error, result) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer campaign action updated!');
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Influencer Campaign Twitter Post ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignTwitterPost = (req, res) => {
    let check = helper.checkRequest(['oauth_token', 'oauth_token_secret', 'text', 'campaign_influencer_id'], req.body);
    if (check == true) {
        // Lets tweet it
        if (req.body.file) {
            image2base64(req.body.file).then((response) => {
                var params = { media_data: response };
                var client = new Twitter({
                    consumer_key: process.env.TWITTER_KEY,
                    consumer_secret: process.env.TWITTER_SECRET,
                    access_token_key: req.body.oauth_token,
                    access_token_secret: req.body.oauth_token_secret
                });
                client.post('media/upload', params, function(error, media, response) {
                    if (!error) {
                        // If successful, a media object will be returned.
                        var status = {
                            status: req.body.text,
                            media_ids: media.media_id_string
                        }
                        client.post('statuses/update', status, function(error, body, response) {
                            if (body) {
                                let query = {
                                    'campaign_influencer_id': req.body.campaign_influencer_id
                                }
                                let new_values = {
                                    '$inc': {
                                        'post_done': 1
                                    }
                                }
                                campaignInfluencerService.updateDetails(query, new_values, { safe: true }, (err1, result1) => {
                                    if (err1) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err1);
                                    } else {
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, `Tweet Done Succesfully`);
                                    }
                                })
                            }
                        });
                    }
                });
            });
        } else {
            var status = { status: req.body.text }
            var client = new Twitter({
                consumer_key: process.env.TWITTER_KEY,
                consumer_secret: process.env.TWITTER_SECRET,
                access_token_key: req.body.oauth_token,
                access_token_secret: req.body.oauth_token_secret
            });
            client.post('statuses/update', status, function(error, body, response) {
                if (body) {
                    let query = {
                        'campaign_influencer_id': req.body.campaign_influencer_id
                    }
                    let new_values = {
                        '$inc': {
                            'post_done': 1
                        }
                    }
                    campaignInfluencerService.updateDetails(query, new_values, { safe: true }, (err1, result1) => {
                        if (err1) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err1);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, `Tweet Done Succesfully`);
                        }
                    })
                }
            });
        }
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Influencer check Access token of handle ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkUserAuthentication = (req, res) => {
    let check = helper.checkRequest(['user_id', 'handle'], req.body);
    if (check == true) {
        userService.findUserData({
            '_id': ObjectId(req.body.user_id),
            'twitter.Elv_social': req.body.handle
        }, { 'twitter': 1 }, (error, result) => {
            if (error) {
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (result && result.twitter.length > 0) {
                    result.twitter.map(user => {
                        if (req.body.handle == user.Elv_social) {
                            if (user.oauth_token == "" || user.oauth_token_secret == "" || user.oauth_token == undefined || user.oauth_token_secret == undefined) {
                                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Access token not Found');
                            } else {
                                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Access token found', user);
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponsewithError(res, 403, 'Authentication error.Please connect your profile.');
                }
            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Influencer check Access token of handle ]
 * @param  {[type]} req [object received from the parameter.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
// update count of retweet done successfully
const retweetCountUpdate = (req, res) => {
    let check = helper.checkRequest(['campaign_influencer_id', 'url'], req.body);
    if (check == true) {
        let query = { 'campaign_influencer_id': req.body.campaign_influencer_id }
        let new_values = {
            $push: {
                'retweet_done': {
                    'url': req.body.url,
                    'status': "1"
                }
            }
        }
        campaignInfluencerService.updateDetails(query, new_values, { safe: true }, (err, result1) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, `Update Post Count Succesfully`);
            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/*Export apis*/
module.exports = {
    getCampaignNewList : getInfluencerCampaignList,
    campaignDetail,
    getCampaignCompletedList,
    actionInfluencerCampaign,
    campaignTwitterPost,
    checkUserAuthentication,
    retweetCountUpdate
}