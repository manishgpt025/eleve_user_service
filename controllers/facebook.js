const request = require('request-promise');
const userService = require('../services/userServices.js');
const ObjectId = require('mongodb').ObjectID;
const config = require('../config.js');
const secret = `${global.gConfig.secret}`;
const userModel = require('../models/userModel.js');
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
let jwt = require('jsonwebtoken');
var FB = require('fb');

/**
 * [Facebook Connect]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const facebookConnect = function(req, res) {
    let user_email = req.body.decoded.email.toLowerCase();
    var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name', 'gender', 'picture', 'friends', 'accounts'];
    var accessTokenUrl = 'https://graph.facebook.com/v2.8/oauth/access_token';
    var graphApiUrl = 'https://graph.facebook.com/v2.8/me?fields=' + fields.join(',');
    var params = {
        code: req.body.code,
        client_id: process.env.FBCLIENT_ID,
        client_secret: process.env.FBCLIENT_SECRET,
        redirect_uri: req.body.redirectUri
    };
    request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
        if (response.statusCode !== 200) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Access token error');
        }
        // Step 2. Retrieve profile information about the current user.
        request.get({ url: graphApiUrl, qs: accessToken, json: true }, function(err, response, profile) {
            if (response.statusCode !== 200) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Profile error');
            }
            profile.accessToken = accessToken.access_token;
            let myquery = {
                email: { $ne: user_email },
                'facebook.id': profile.id
            };
            userService.findData(myquery, (err, userInfo) => {
                if (err) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                } else {
                    if (userInfo) {
                        responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'Please try with diffrent account, this account is already exists.');
                    } else {
                        // Saving data
                        let query = {
                            email: user_email,
                            'facebook.id': profile.id
                        };

                        userService.findData(query, (error_, userInfo1) => {
                            if (error_) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                if (userInfo1) {
                                    responseHandle.sendResponsewithError(res, responseCode.ALREADY_EXIST, 'You already have this account mentioned on your profile');
                                } else {
                                    var query1 = {
                                        email: user_email,
                                    }
                                    profile.Elv_social = profile.name;
                                    let insertData = {
                                        "id" : profile.id,
                                        "oauth_token" : accessToken.access_token,
                                        "first_name": profile.first_name,
                                        "last_name": profile.last_name,
                                        "name": profile.name,
                                        "Elv_social": profile.name,
                                        "link" : profile.link,
                                        "followers_count" : profile.friends,
                                        "friends_count" :profile.friends,
                                        "profile_image_url" : profile.picture? profile.picture.data.url:'',
                                        "connection_type" : "auto",
                                        "category" : "",
                                        "genres" : []

                                    }
                                    var data = {
                                        $push: {
                                            facebook: insertData
                                        }
                                    }
                                    userService.updateOne(query1, data, {
                                        new: true
                                    }, (error, updatedResp) => {
                                        if (error) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                        } else {
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User linked in account added Successfully!', updatedResp);
                                        }
                                    });
                                }

                            }
                        });

                    }
                }
            });
        });
    });
}

/**
 * [Facebook outh API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const facebookCodeURI = function(req, res) {
    var codeUrl = 'https://www.facebook.com/dialog/oauth';
    let code_url = codeUrl + '?client_id=' + process.env.FBCLIENT_ID + '&redirect_uri=' + process.env.FB_REDIRECT_URI;
    return res.status(200).send({ code_url });
}

/*Export apis*/
module.exports = {
    facebookConnect,
    facebookCodeURI,
}