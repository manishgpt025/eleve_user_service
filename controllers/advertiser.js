const organisationService = require('../services/organisationService.js');// advertiser services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');

/**
 * [Search Organisation]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchOrganisation = (req, res) => {
    let check = helper.checkRequest(["user_id", "type"], req.body);
    if (check == true) {
        // Check from DB Collection
        if (req.body.type == '1') {
            var searchData = {
                "is_asscoiated": "0",
            }
        }
        if (req.body.type == '2') {
            var searchData = {
                "is_asscoiated": "1",
                "type": "Brand",
            }
        }
        if (req.body.type == '3') {
            var searchData = {
                "type": "Brand",
            }
        }
        organisationService.getAllData(searchData, (err, organosationInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (organosationInfo) {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Data found successfully.', organosationInfo);
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Data found successfully.', []);
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/* Export apis */
module.exports = {
    searchOrganisation
}