const helper = require('../globalFunctions/function.js');//use helper
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const userService = require('../services/userServices.js');// user services
const ObjectId = require('mongodb').ObjectID;

/**
 * [check for particular emailid]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkemail = (req, res) => {
        let check = helper.checkRequest(['email'], req.body);
        if (check == true) {
            userService.findData({ _id: new ObjectId(req.body.user_id), 'email_verification': '1' }, (error_, emailverify) => {
                if (error_) {
                    responseHandle.sendResponsewithError(res, 500, err);
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Email is verified', result);
                }
            });
        }
    }

/**
 * [email send link ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const email_send_link = (req, res) => {
        var randomstring = require('randomstring');
        let check = helper.checkRequest(['email'], req.body);
        if (check == true) {
            userService.findData({ email: req.body.email }, (err, emailData) => {
                if (err) {
                    responseHandle.sendResponsewithError(res, 500, err);
                } else if (emailData.email_verification == "1") {
                    responseHandle.sendResponseWithData(res, responseCode.ALREADY_EXIST, 'Your email is already verified', emailData);
                } else {
                    let query = { email: req.body.email };
                    let emailverificationToken = randomstring.generate(7);
                    let emailverificationExpires = Date.now() + 900; // 15 min
                    let newvalues = { $set: { emailverificationToken: emailverificationToken, emailverificationExpires: emailverificationExpires } };
                    userService.updateOne(query, newvalues, function(error, result) {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                        } else {
                            var message = {
                                "html": "<p>Hi , Please find below the link for email verification <br />-" + req.body.url + "?email=" + req.body.email + "&token=" + emailverificationToken + "</p>",
                                "subject": "Email Verification Link",
                                "from_email": "team@eleve.co.in",
                                "from_name": "Eleve",
                                "to": [{
                                    "email": req.body.email,
                                    "type": "to"
                                }]
                            };
                            let sendmail = helper.sendEmail(message);
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Check the email we have sent you on the submitted email ID.', result);
                        }
                    });
                }
            });

        } else
            responseHandle.sendResponsewithError(res, 404, '${check} key is missing.');
    };

/**
 * [check email verification]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const email_verify = (req, res) => {
    let check = helper.checkRequest(["email", "token"], req.body);
    if (check == true) {
        // Check from DB Collection
        userService.findData({ email: req.body.email, emailverificationToken: req.body.token }, (err, userInfo) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                if (userInfo) {
                    let timenow = Date.now();
                    let dateexpiry = new Date(userInfo.emailverificationExpires);
                    var milliseconds = dateexpiry.getTime();
                    if (timenow > milliseconds) {
                        responseHandle.sendResponseWithData(res, responseCode.WENT_WRONG, 'Expired');
                    } else {
                        let myquery = { email: req.body.email, emailverificationToken: req.body.token };
                        let newvalues = {$set: { email_verification: "1"}};
                        userService.updateOne(myquery, newvalues, function(err, userupdate) {
                            if (err) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Valid');
                            }
                        });
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, 'User not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [save user skills]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userskills = (req, res) => {
    let interests = req.body.interests;
    let lifestage = req.body.lifestage;
    let education = req.body.education;
    let income = req.body.income;
    let language = req.body.language;
    userService.findData({ _id: new ObjectId(req.body.user_id) }, (error1, userInfo) => {
        if (error1) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);
        } else {
            if (userInfo) {
                let myquery = { _id: new ObjectId(req.body.user_id) };
                let newvalues = {
                    $set: {
                        interests: interests,
                        lifestage: lifestage,
                        education: education,
                        language: language,
                        income: income,
                    }
                };
                userService.updateOne(myquery, newvalues, { new: true }, (error2, userupdate) => {
                    if (error2) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Skills Updated');
                    }
                });
            } else {
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Not found');
            }
        }
    });
}

/*Export apis*/
module.exports = {
    email_verify,
    email_send_link,
    checkemail,
    userskills
}